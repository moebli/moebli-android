package com.moebli.android.backend.contact;

import com.moebli.android.backend.billing.balance.ContactBalanceService;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class ContactStatusService {
    private static String C = ContactStatusService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static ContactStatusService mContactStatusService;
    private static HttpClientCallback mChangeContactStatusCallback;

    public void changeContactStatus(HttpClientCallback callback, String contactUuid, String
            status) {
        ChangeContactStatusRequestDTO requestDTO = new ChangeContactStatusRequestDTO();
        requestDTO.setUuid(contactUuid);
        requestDTO.setStatus(status);
        mChangeContactStatusCallback = callback;
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                        "/api/changeContactStatus",
                requestDTO, new ChangeContactStatusBackendCallback(), ChangeContactStatusResponseDTO
                        .class);
    }

    class ChangeContactStatusBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if(o instanceof Throwable) {
                mChangeContactStatusCallback.handleResponse(o);
                return;
            }
            ChangeContactStatusResponseDTO responseDTO = (ChangeContactStatusResponseDTO) o;
            ContactBalanceService.getInstance().updateContactStatus(responseDTO.getChangedContact());
            mChangeContactStatusCallback.handleResponse(responseDTO);
        }
    }

    public static ContactStatusService getInstance() {
        if (mContactStatusService == null)
            mContactStatusService = new ContactStatusService();
        return mContactStatusService;
    }

    private ContactStatusService() {
    }
}
