package com.moebli.android.backend.session;

import com.moebli.android.backend.util.ErrorDTO;

import java.util.ArrayList;
import java.util.List;


public class LoginResponseDTO {

    private String countryCode;

    private String primaryLocale;

    private String currencyCode;
    
    private String currencySymbol;

    private String javaDateFormat;

    private String statusCode;

    private String success;

    private List<ErrorDTO> errorList = new ArrayList<ErrorDTO>();

    // for FB login only
    public enum FBLoginStatusCode {
        Success, DisconnectedAccountExists, EmailRequired
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getJavaDateFormat() {
        return javaDateFormat;
    }

    public void setJavaDateFormat(String javaDateFormat) {
        this.javaDateFormat = javaDateFormat;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public FBLoginStatusCode getStatusCodeEnum() {
        return FBLoginStatusCode.valueOf(statusCode);
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<ErrorDTO> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<ErrorDTO> errorList) {
        this.errorList = errorList;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPrimaryLocale() {
        return primaryLocale;
    }

    public void setPrimaryLocale(String primaryLocale) {
        this.primaryLocale = primaryLocale;
    }
}
