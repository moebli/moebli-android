package com.moebli.android.backend.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public enum CountryCode {
    // country less than population of 20K excluded
    AF(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "412", TimeZoneEnum.utc, "Afghanistan"), // to be configured
    AL(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "276", TimeZoneEnum.utc, "Albania"), // to be configured
    DZ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "631", TimeZoneEnum.utc, "Algeria"), // to be configured
    AO(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "631", TimeZoneEnum.utc, "Angola"), // to be configured
    AG(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Antigua and Barbuda"), // to be configured
    AR(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "722", TimeZoneEnum.utc, "Argentina"), // to be configured
    AM(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "283", TimeZoneEnum.utc, "Armenia"), // to be configured
    AW(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Aruba"), // to be configured
    AU(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "505", TimeZoneEnum.utc, "Australia"), // to be configured
    AT(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "232", TimeZoneEnum.utc, "Austria"), // to be configured
    AZ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "400", TimeZoneEnum.utc, "Azerbaijan"), // to be configured
    BS(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Bahamas"), // to be configured
    BH(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "426", TimeZoneEnum.utc, "Bahrain"), // to be configured
    BD(LocaleEnum.en_US, Currency.BDT, DateFormat.DDhMMhYY, "470", TimeZoneEnum.utc, "Bangladesh"), // TZ
    BB(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Barbados"), // to be configured
    BY(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "257", TimeZoneEnum.utc, "Belarus"), // to be configured
    BE(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "206", TimeZoneEnum.utc, "Belgium"), // to be configured
    BZ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Belize"), // to be configured
    BJ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "616", TimeZoneEnum.utc, "Benin"), // to be configured
    BM(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Bermuda"), // to be configured
    BT(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Bhutan"), // to be configured
    BO(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "736", TimeZoneEnum.utc, "Bolivia"), // to be configured
    BA(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "218", TimeZoneEnum.utc, "Bosnia and Herzegovina"), // to be configured
    BW(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "652", TimeZoneEnum.utc, "Botswana"), // to be configured
    BR(LocaleEnum.en_US, Currency.BRL, DateFormat.DDsMMsYY, "724", TimeZoneEnum.utc, "Brazil"), // TZ
    BN(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Brunei Darussalam"), // to be configured
    BG(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "284", TimeZoneEnum.utc, "Bulgaria"), // to be configured
    BF(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "613", TimeZoneEnum.utc, "Burkina Faso"), // to be configured
    BI(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "642", TimeZoneEnum.utc, "Burundi"), // to be configured
    KH(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "456", TimeZoneEnum.utc, "Cambodia"), // to be configured
    CM(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "624", TimeZoneEnum.utc, "Cameroon"), // to be configured
    CA(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "302", TimeZoneEnum.utc, "Canada"), // to be configured
    CV(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Cape Verde"), // to be configured
    KY(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Cayman Islands"), // to be configured
    CF(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Central African Republic"), // to be configured
    TD(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Chad"), // to be configured
    CL(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "730", TimeZoneEnum.utc, "Chile"), // to be configured
    CN(LocaleEnum.en_US, Currency.CNY, DateFormat.YYdMMdDD, "460", TimeZoneEnum.utc, "China"), // TZ
    CO(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "732", TimeZoneEnum.utc, "Colombia"), // to be configured
    KM(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Comoros"), // to be configured
    CG(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "629", TimeZoneEnum.utc, "Congo"), // to be configured
    CD(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "630", TimeZoneEnum.utc, "Congo, Democratic Republic"), // to be configured
    CR(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "712", TimeZoneEnum.utc, "Costa Rica"), // to be configured
    HR(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "219", TimeZoneEnum.utc, "Croatia"), // to be configured
    CU(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Cuba"), // to be configured
    CW(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Curaçao"), // to be configured
    CY(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "280", TimeZoneEnum.utc, "Cyprus"), // to be configured
    CZ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "230", TimeZoneEnum.utc, "Czech Republic"), // to be configured
    DK(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "238", TimeZoneEnum.utc, "Denmark"), // to be configured
    DJ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Djibouti"), // to be configured
    DM(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Dominica"), // to be configured
    DO(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "370", TimeZoneEnum.utc, "Dominican Republic"), // to be configured
    EC(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "740", TimeZoneEnum.utc, "Ecuador"), // to be configured
    EG(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "602", TimeZoneEnum.utc, "Egypt"), // to be configured
    EU(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Europe"), // to be configured
    SV(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "706", TimeZoneEnum.utc, "El Salvador"), // to be configured
    GQ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Equatorial Guinea"), // to be configured
    ER(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Eritrea"), // to be configured
    EE(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "214", TimeZoneEnum.utc, "Estonia"), // to be configured
    ET(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "636", TimeZoneEnum.utc, "Ethiopia"), // to be configured
    FO(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Faroe Islands"), // to be configured
    FJ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "542", TimeZoneEnum.utc, "Fiji"), // to be configured
    FI(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "244", TimeZoneEnum.utc, "Finland"), // to be configured
    FR(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "208", TimeZoneEnum.utc, "France"), // to be configured
    GF(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "French Guiana"), // to be configured
    PF(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "French Polynesia"), // to be configured
    GA(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "628", TimeZoneEnum.utc, "Gabon"), // to be configured
    GM(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Gambia"), // to be configured
    GE(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "282", TimeZoneEnum.utc, "Georgia"), // to be configured
    DE(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "262", TimeZoneEnum.utc, "Germany"), // to be configured
    GH(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "620", TimeZoneEnum.utc, "Ghana"), // to be configured
    GI(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Gibraltar"), // to be configured
    GR(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "202", TimeZoneEnum.utc, "Greece"), // to be configured
    GL(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Greenland"), // to be configured
    GD(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Grenada"), // to be configured
    GP(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Guadeloupe"), // to be configured
    GU(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Guam"), // to be configured
    GT(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "704", TimeZoneEnum.utc, "Guatemala"), // to be configured
    GG(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Guernsey"), // to be configured
    GN(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "611", TimeZoneEnum.utc, "Guinea"), // to be configured
    GW(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Guinea-Bissau"), // to be configured
    GY(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Guyana"), // to be configured
    HT(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Haiti"), // to be configured
    HN(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "708", TimeZoneEnum.utc, "Honduras"), // to be configured
    HK(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "454", TimeZoneEnum.utc, "Hong Kong"), // to be configured
    HU(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "216", TimeZoneEnum.utc, "Hungary"), // to be configured
    IS(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "274", TimeZoneEnum.utc, "Iceland"), // to be configured
    IN(LocaleEnum.en_US, Currency.INR, DateFormat.DDhMMhYY, "404", TimeZoneEnum.india, "India"), //
    ID(LocaleEnum.en_US, Currency.IDR, DateFormat.MMsDDsYY, "510", TimeZoneEnum.utc, "Indonesia"), // to be configured ---- update date format
    IR(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "432", TimeZoneEnum.utc, "Iran"), // to be configured
    IQ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "418", TimeZoneEnum.utc, "Iraq"), // to be configured
    IE(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "272", TimeZoneEnum.utc, "Ireland"), // to be configured
    IM(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Isle of Man"), // to be configured
    IL(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "425", TimeZoneEnum.utc, "Israel"), // to be configured
    CI(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "612", TimeZoneEnum.utc, "Ivory Coast"), // to be configured
    IT(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "222", TimeZoneEnum.utc, "Italy"), // to be configured
    JM(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Jamaica"), // to be configured
    JP(LocaleEnum.en_US, Currency.JPY, DateFormat.YYdMMdDD, "440", TimeZoneEnum.utc, "Japan"), // to be configured
    JE(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Jersey"), // to be configured
    JO(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "416", TimeZoneEnum.utc, "Jordan"), // to be configured
    KZ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "401", TimeZoneEnum.utc, "Kazakhstan"), // to be configured
    KE(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "639", TimeZoneEnum.utc, "Kenya"), // to be configured
    KI(LocaleEnum.en_US, Currency.AUD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Kiribati"), // to be configured
    XK(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Kosovo"), // to be configured
    KW(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "419", TimeZoneEnum.utc, "Kuwait"), // to be configured
    KG(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "437", TimeZoneEnum.utc, "Kyrgyzstan"), // to be configured
    LA(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "457", TimeZoneEnum.utc, "Laos"), // to be configured
    LV(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Latvia"), // to be configured
    LB(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "415", TimeZoneEnum.utc, "Lebanon"), // to be configured
    LS(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "651", TimeZoneEnum.utc, "Lesotho"), // to be configured
    LR(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "618", TimeZoneEnum.utc, "Liberia"), // to be configured
    LY(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "606", TimeZoneEnum.utc, "Libya"), // to be configured
    LI(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Liechtenstein"), // to be configured
    LT(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "246", TimeZoneEnum.utc, "Lithuania"), // to be configured
    LU(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Luxembourg"), // to be configured
    MO(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Macao"), // to be configured
    MK(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "294", TimeZoneEnum.utc, "Macedonia"), // to be configured
    MG(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "646", TimeZoneEnum.utc, "Madagascar"), // to be configured
    MW(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "650", TimeZoneEnum.utc, "Malawi"), // to be configured
    MY(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "502", TimeZoneEnum.utc, "Malaysia"), // to be configured
    MV(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Maldives"), // to be configured
    ML(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Mali"), // to be configured
    MT(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "278", TimeZoneEnum.utc, "Malta"), // to be configured
    MH(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Marshall Islands"), // to be configured
    MQ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Martinique"), // to be configured
    MR(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Mauritania"), // to be configured
    MU(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "617", TimeZoneEnum.utc, "Mauritius"), // to be configured
    YT(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Mayotte"), // to be configured
    MX(LocaleEnum.en_US, Currency.MXN, DateFormat.MMsDDsYY, "334", TimeZoneEnum.utc, "Mexico"), // to be configured ---- update date format
    FM(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Micronesia"), // to be configured
    MD(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "259", TimeZoneEnum.utc, "Moldova"), // to be configured
    MC(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Monaco"), // to be configured
    MN(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Mongolia"), // to be configured
    ME(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "297", TimeZoneEnum.utc, "Montenegro"), // to be configured
    MA(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "604", TimeZoneEnum.utc, "Morocco"), // to be configured
    MZ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "643", TimeZoneEnum.utc, "Mozambique"), // to be configured
    MM(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "414", TimeZoneEnum.utc, "Myanmar"), // to be configured
    NA(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "649", TimeZoneEnum.utc, "Namibia"), // to be configured
    NP(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "429", TimeZoneEnum.utc, "Nepal"), // to be configured
    NL(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "204", TimeZoneEnum.utc, "Netherlands"), // to be configured
    AN(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Netherlands Antilles"), // to be configured
    NC(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "New Caledonia"), // to be configured
    NZ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "530", TimeZoneEnum.utc, "New Zealand"), // to be configured
    NI(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Nicaragua"), // to be configured
    NE(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "614", TimeZoneEnum.utc, "Niger"), // to be configured
    NG(LocaleEnum.en_US, Currency.NGN, DateFormat.DDsMMsYY, "621", TimeZoneEnum.utc, "Nigeria"), // TZ
    MP(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Northern Mariana Islands"), // to be configured
    KP(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "North Korea"), // to be configured
    NO(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "242", TimeZoneEnum.utc, "Norway"), // to be configured
    OM(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "422", TimeZoneEnum.utc, "Oman"), // to be configured
    PK(LocaleEnum.en_US, Currency.PKR, DateFormat.DDsMMsYY, "410", TimeZoneEnum.utc, "Pakistan"), // TZ
    PW(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Palau"), // to be configured
    PS(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Palestinian Territory"), // to be configured
    PA(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "714", TimeZoneEnum.utc, "Panama"), // to be configured
    PG(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Papua New Guinea"), // to be configured
    PY(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "744", TimeZoneEnum.utc, "Paraguay"), // to be configured
    PE(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "716", TimeZoneEnum.utc, "Peru"), // to be configured
    PH(LocaleEnum.en_US, Currency.PHP, DateFormat.MMsDDsYY, "515", TimeZoneEnum.utc, "Philippines"), // to be configured ---- update date format
    PL(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "260", TimeZoneEnum.utc, "Poland"), // to be configured
    PT(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "268", TimeZoneEnum.utc, "Portugal"), // to be configured
    PR(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Puerto Rico"), // to be configured
    QA(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "427", TimeZoneEnum.utc, "Qatar"), // to be configured
    RO(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "226", TimeZoneEnum.utc, "Romania"), // to be configured
    RU(LocaleEnum.en_US, Currency.RUB, DateFormat.DDdMMdYY, "250", TimeZoneEnum.utc, "Russia"), // TZ
    RW(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "635", TimeZoneEnum.utc, "Rwanda"), // to be configured
    RE(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Réunion"), // to be configured
    KN(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Saint Kitts And Nevis"), // to be configured
    LC(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Saint Lucia"), // to be configured
    MF(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Saint Martin"), // to be configured
    VC(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Saint Vincent and the Grenadines"), // to be configured
    WS(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Samoa"), // to be configured
    SM(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "San Marino"), // to be configured
    ST(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Sao Tome and Principe"), // to be configured
    SA(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "420", TimeZoneEnum.utc, "Saudi Arabia"), // to be configured
    SN(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "608", TimeZoneEnum.utc, "Senegal"), // to be configured
    RS(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "220", TimeZoneEnum.utc, "Serbia"), // to be configured
    SC(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "633", TimeZoneEnum.utc, "Seychelles"), // to be configured
    SL(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "619", TimeZoneEnum.utc, "Sierra Leone"), // to be configured
    SG(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "525", TimeZoneEnum.utc, "Singapore"), // to be configured
    SX(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Sint Maarten (Dutch part)"), // to be configured
    SK(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "231", TimeZoneEnum.utc, "Slovakia"), // to be configured
    SI(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "293", TimeZoneEnum.utc, "Slovenia"), // to be configured
    SB(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Solomon Islands"), // to be configured
    SO(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Somalia"), // to be configured
    ZA(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "655", TimeZoneEnum.utc, "South Africa"), // to be configured
    KR(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "450", TimeZoneEnum.utc, "South Korea"), // to be configured
    SS(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "South Sudan"), // to be configured
    ES(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "214", TimeZoneEnum.utc, "Spain"), // to be configured
    LK(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "413", TimeZoneEnum.utc, "Sri Lanka"), // to be configured
    SD(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "634", TimeZoneEnum.utc, "Sudan"), // to be configured
    SR(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Suriname"), // to be configured
    SZ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "653", TimeZoneEnum.utc, "Swaziland"), // to be configured
    SE(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "240", TimeZoneEnum.utc, "Sweden"), // to be configured
    CH(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "228", TimeZoneEnum.utc, "Switzerland"), // to be configured
    SY(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Syrian"), // to be configured
    TW(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "466", TimeZoneEnum.utc, "Taiwan"), // to be configured
    TJ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "436", TimeZoneEnum.utc, "Tajikistan"), // to be configured
    TZ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "640", TimeZoneEnum.utc, "Tanzania"), // to be configured
    TH(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "520", TimeZoneEnum.utc, "Thailand"), // to be configured
    TL(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Timor-Leste"), // to be configured
    TG(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "615", TimeZoneEnum.utc, "Togo"), // to be configured
    TO(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Tonga"), // to be configured
    TT(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Trinidad and Tobago"), // to be configured
    TN(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Tunisia"), // to be configured
    TR(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "286", TimeZoneEnum.utc, "Turkey"), // to be configured
    TM(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Turkmenistan"), // to be configured
    TC(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Turks and Caicos Islands"), // to be configured
    UG(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "641", TimeZoneEnum.utc, "Uganda"), // to be configured
    UA(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "255", TimeZoneEnum.utc, "Ukraine"), // to be configured
    AE(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "424", TimeZoneEnum.utc, "United Arab Emirates"), // to be configured
    GB(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "234", TimeZoneEnum.utc, "United Kingdom"), // to be configured
    US(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "311", TimeZoneEnum.pacific, "United States"), //
    UY(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "748", TimeZoneEnum.utc, "Uruguay"), // to be configured
    UZ(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "434", TimeZoneEnum.utc, "Uzbekistan"), // to be configured
    VU(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Vanuatu"), // to be configured
    VE(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "734", TimeZoneEnum.utc, "Venezuela"), // to be configured
    VN(LocaleEnum.en_US, Currency.VND, DateFormat.MMsDDsYY, "452", TimeZoneEnum.utc, "Vietnam"), // to be configured ---- update date format
    VG(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Virgin Islands, British"), // to be configured
    VI(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Virgin Islands, U.S."), // to be configured
    EH(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, null, TimeZoneEnum.utc, "Western Sahara"), // to be configured
    YE(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "421", TimeZoneEnum.utc, "Yemen"), // to be configured
    ZM(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "645", TimeZoneEnum.utc, "Zambia"), // to be configured
    ZW(LocaleEnum.en_US, Currency.USD, DateFormat.MMsDDsYY, "648", TimeZoneEnum.utc, "Zimbabwe"), // to be configured
    ;

    private String primaryLocale;
    private String currencyCode;
    private String javaDateFormat;
    private String javaScriptDateFormat;
    private String dateMonth;
    private String timeZone;
    private String name;
    private String mcc;

    CountryCode(LocaleEnum locale, Currency currencyCode, DateFormat dateFormat, String mcc, TimeZoneEnum timeZone,
                String name) {
        this.primaryLocale = locale.name();
        this.currencyCode = currencyCode.name();
        this.javaDateFormat = dateFormat.getJavaDateFormat();
        this.javaScriptDateFormat = dateFormat.getJavaScriptDateFormat();
        this.dateMonth = dateFormat.getDateMonth();
        this.timeZone = timeZone.name();
        this.name = name;
        this.mcc = mcc;
    }

    public String getName() {
        return name;
    }

    public String getPrimaryLocale() {
        return primaryLocale;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getJavaDateFormat() {
        return javaDateFormat;
    }

    public String getJavaScriptDateFormat() {
        return javaScriptDateFormat;
    }

    public String getDateMonth() {
        return dateMonth;
    }

    public String getTimeZone() {
        return timeZone;
    }

    private static final Map<String, String> countryCodeMap = new LinkedHashMap<>();
    private static final Map<String, CountryCode> countryCodeEnumMap = new LinkedHashMap<>();
    private static final Map<String, CountryCode> mccCountryCodeEnumMap = new LinkedHashMap<>();
    private static final List<CountryCode> countryCodeEnumList = new ArrayList<>();

    static {
        for (CountryCode en : values()) {
            countryCodeMap.put(en.name, en.name());
            countryCodeEnumMap.put(en.name(), en);
            countryCodeEnumList.add(en);
            if (en.mcc != null)
                mccCountryCodeEnumMap.put(en.mcc, en);
        }
    }

    public static Map<String, String> getMap() {
        return countryCodeMap;
    }

    public static CountryCode getEnum(String countryCode) {
        return countryCodeEnumMap.get(countryCode);
    }
    public static String getCountryCode(String countryNameWithCode) {
        return countryCodeMap.get(countryNameWithCode);
    }

    public static CountryCode getEnumByMcc(String mcc) {
        return mccCountryCodeEnumMap.get(mcc);
    }

    public static List<CountryCode> getCountryCodeEnumList() {
        return countryCodeEnumList;
    }

    public static int getUserCountryPosition(String userCountryCode) {
        CountryCode userCountryCodeEnum = countryCodeEnumMap.get(userCountryCode);
        int position=0;
        for(CountryCode countryCode : countryCodeEnumList) {
               if(countryCode == userCountryCodeEnum) {
                   return position;
               }
            position++;
        }
        return 0;//TODO default to particular country.
    }
}
