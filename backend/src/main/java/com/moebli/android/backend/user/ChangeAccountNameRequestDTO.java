package com.moebli.android.backend.user;

public class ChangeAccountNameRequestDTO {

    private String accountName;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}
