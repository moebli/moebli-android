package com.moebli.android.backend.billing.bill;

import com.moebli.android.backend.util.ErrorDTO;

import java.util.ArrayList;
import java.util.List;

public class UpdateMemoResponseDTO {

    private BillDTO bill;
    
    private String success;

    private List<ErrorDTO> errorList = new ArrayList<ErrorDTO>();

    public BillDTO getBill() {
        return bill;
    }

    public void setBill(BillDTO bill) {
        this.bill = bill;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<ErrorDTO> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<ErrorDTO> errorList) {
        this.errorList = errorList;
    }

    public StringBuilder toStringBuilder() {
        StringBuilder buf = new StringBuilder(100);
        buf.append("DeleteMemoResponseDTO { ");
        buf.append("bill: ").append(bill);
        buf.append("; errors:").append(errorList);
        buf.append(" }");
        return buf;
    }

    @Override
    public String toString() {
        return toStringBuilder().toString();
    }
}
