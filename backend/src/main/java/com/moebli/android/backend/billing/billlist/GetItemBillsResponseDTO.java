package com.moebli.android.backend.billing.billlist;

import com.moebli.android.backend.billing.bill.BillDTO;

import java.util.List;

public class GetItemBillsResponseDTO {
	
	private String itemUuid;

    private List<BillDTO> billList;
    
    private Long version;

    public String getItemUuid() {
		return itemUuid;
	}

	public void setItemUuid(String itemUuid) {
		this.itemUuid = itemUuid;
	}

	public List<BillDTO> getBillList() {
        return billList;
    }

    public void setBillList(List<BillDTO> billList) {
        this.billList = billList;
    }

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
}
