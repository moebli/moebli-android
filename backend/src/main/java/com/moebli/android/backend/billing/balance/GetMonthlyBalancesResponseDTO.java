package com.moebli.android.backend.billing.balance;

import java.util.ArrayList;
import java.util.List;

public class GetMonthlyBalancesResponseDTO {

    private List<MonthlyBalanceDTO> monthlyBalanceList = new ArrayList<MonthlyBalanceDTO>();

    public List<MonthlyBalanceDTO> getMonthlyBalanceList() {
        return monthlyBalanceList;
    }

    public void setMonthlyBalanceList(List<MonthlyBalanceDTO> monthlyBalanceList) {
        this.monthlyBalanceList = monthlyBalanceList;
    }
}
