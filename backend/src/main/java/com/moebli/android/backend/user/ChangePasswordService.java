package com.moebli.android.backend.user;

import com.moebli.android.backend.util.BaseResponseDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class ChangePasswordService {
    private static String C = ChangePasswordService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static HttpClientCallback changePasswordCallback;

    public void changePassword(HttpClientCallback callback, String oldPassword, String newPassword) {
        changePasswordCallback = callback;
        ChangePasswordRequestDTO requestDTO = new ChangePasswordRequestDTO();
            requestDTO.setOldPassword(oldPassword);
            requestDTO.setNewPassword(newPassword);
            httpClient.executeRequest(HttpClientUtil.Method.POST, BASE_URL +
                            "/api/changePassword",
                    requestDTO, new ChangePasswordBackendCallback(), BaseResponseDTO.class);

    }

    public class ChangePasswordBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            changePasswordCallback.handleResponse(o);
        }
    }
}
