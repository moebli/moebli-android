package com.moebli.android.backend.enums;

import org.apache.commons.lang3.LocaleUtils;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

public enum LocaleEnum {
    en_US("English", Locale.US),
    bn_BD("Bengali", LocaleUtils.toLocale("bn_BD")),
    fr_FR("French", Locale.FRANCE);

    private String name;
    private Locale locale;

    LocaleEnum(String name, Locale locale) {
        this.name = name;
        this.locale = locale;
    }

    public String getName() {
        return name;
    }

    public Locale getLocale() {
        return locale;
    }

    private static final Map<String, String> localeMap = new LinkedHashMap<String, String>();
    static {
        for (LocaleEnum en : values()) {
            localeMap.put(en.name(), en.name);
        }
    }

    public static Map<String, String> getMap() {
        return localeMap;
    }
}
