package com.moebli.android.backend.billing.billlist;

public class GetMonthlyBillsRequestDTO {

	private String monthYearCode;
	
	private String startIndex;
	
	private String range;

	public String getMonthYearCode() {
		return monthYearCode;
	}

	public void setMonthYearCode(String monthYearCode) {
		this.monthYearCode = monthYearCode;
	}

	public String getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(String startIndex) {
		this.startIndex = startIndex;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}
 }
