package com.moebli.android.backend.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public enum Currency {
    AUD("A$", 2, "Australian dollar"), //
    BDT("৳", 2, "Bangladeshi taka", true, false), // poisha rarely used
    BRL("R$", 2, "Brazilian real"), //
    CAD("C$", 2, "Canadian dollar"), //
    CNY("¥", 2, "Chinese yuan"), //
    CZK("Kč", 0, "Czech koruna", false, false), //
    DKK("kr", 2, "Danish krone", false, false), // 50 øre available, but not showing
    EUR("€", 2, "Euro"), //
    HKD("HK$", 2, "Hong Kong dollar"), //
    HUF("Ft", 0, "Hungarian forint", false, false), //
    ILS("₪", 2, "Israeli new shekel"), // symbol is ₪ , if not displayable, display ILS
    INR("₹", 2, "Indian rupee"), //
    IDR("Rp", 0, "Indonesian rupiah"), // there is space between symbol and amount
    JPY("¥", 0, "Japanese yen"), //
    MYR("RM", 2, "Malaysian ringgit"), //
    MXN("Mex$", 2, "Mexican peso"), //
    NGN("₦", 2, "Nigerian naira"), //
    NOK("kr", 0, "Norwegian krone", false, false), //
    NZD("NZ$", 2, "New Zealand dollar"), //
    PHP("P", 2, "Philippine peso", true, false), // cents available but rarely used
    PKR("Rs", 0, "Pakistani rupee"), //
    PLN("zł", 2, "Polish złoty", false, true), // no space when symbol last
    GBP("£", 2, "Pound sterling"), //
    RUB("руб", 2, "Russian ruble"), // ₽ from 2014
    SGD("S$", 2, "Singapore dollar"), //
    SEK("kr", 2, "Swedish krona", false, false), //
    CHF("Fr", 2, "Swiss franc"), //
    TWD("NT$", 2, "New Taiwan dollar", true, false), // NT$½ coin available, but rare
    THB("฿", 2, "Thai baht"), //
    TRY("₺", 2, "Turkish lira"), //
    USD("$", 2, "United States dollar"),
    VND("₫", 0, "Vietnamese dong", false, false), // no space when symbol last
    ;

    private String symbol;
    private int fractional;
    private String name;
    private boolean symbolFirst;
    private boolean showDecimal;
    private String code;

    Currency(String symbol, int fractional, String name) {
        this.symbol = symbol;
        this.symbolFirst = true;
        this.fractional = fractional;
        this.name = name;
        this.code = this.name();
    }

    Currency(String symbol, int fractional, String name, boolean symbolFirst, boolean showDecimal) {
        this.symbol = symbol;
        this.symbolFirst = symbolFirst;
        this.fractional = fractional;
        this.name = name;
        this.code = this.name();
    }

    public String getSymbol() {
        return symbol;
    }

    public int getFractional() {
        return fractional;
    }

    public String getName() {
        return name;
    }

    public boolean isSymbolFirst() {
        return symbolFirst;
    }

    public boolean isShowDecimal() {
        return showDecimal;
    }

    public String getCode() {
        return code;
    }

    private static final Map<String, String> currencyCodeMap = new LinkedHashMap<>();
    private static final Map<String, Currency> currencyCodeEnumMap = new LinkedHashMap<>();
    private static final List<Currency> currencyCodeEnumList = new ArrayList<>();

    static {
        for (Currency en : values()) {
            currencyCodeMap.put(en.name(), en.name);
            currencyCodeEnumMap.put(en.name(), en);
            currencyCodeEnumList.add(en);
        }
    }

    public static Map<String, String> getMap() {
        return currencyCodeMap;
    }

    public static Map<String, Currency> getEnumMap() {
        return currencyCodeEnumMap;
    }

    public static Currency getEnum(String currencyCode) {
        return currencyCodeEnumMap.get(currencyCode);
    }

    public static List<Currency> getCurrencyCodeEnumList() {
        return currencyCodeEnumList;
    }
}
