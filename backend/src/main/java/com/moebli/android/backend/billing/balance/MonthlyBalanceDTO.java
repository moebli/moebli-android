package com.moebli.android.backend.billing.balance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MonthlyBalanceDTO implements Serializable {

    private String monthYearCode;

    private String monthYear;

    private List<BalanceDTO> balanceList = new ArrayList<BalanceDTO>();

    public String getMonthYearCode() {
        return monthYearCode;
    }

    public void setMonthYearCode(String monthYearCode) {
        this.monthYearCode = monthYearCode;
    }

    public String getMonthYear() {
        return monthYear;
    }

    public void setMonthYear(String monthYear) {
        this.monthYear = monthYear;
    }

    public List<BalanceDTO> getBalanceList() {
        return balanceList;
    }

    public void setBalanceList(List<BalanceDTO> balanceList) {
        this.balanceList = balanceList;
    }
}