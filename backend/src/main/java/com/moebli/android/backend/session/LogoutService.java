package com.moebli.android.backend.session;

import com.moebli.android.backend.util.BaseResponseDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class LogoutService {
    private static String C = LogoutService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static LogoutService mLogoutService;

    public void logout(HttpClientCallback callback) {
        httpClient.executeRequest(HttpClientUtil.Method.POST, BASE_URL + "/api/logout",
                null, callback, BaseResponseDTO.class);
    }

    public static LogoutService getInstance() {
        if (mLogoutService == null)
            mLogoutService = new LogoutService();
        return mLogoutService;
    }

    private LogoutService() {
    }
}
