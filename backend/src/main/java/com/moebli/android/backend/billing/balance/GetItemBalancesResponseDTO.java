package com.moebli.android.backend.billing.balance;

import java.util.List;

public class GetItemBalancesResponseDTO {

    private List<ItemBalanceDTO> itemBalanceList;

    public List<ItemBalanceDTO> getItemBalanceList() {
        return itemBalanceList;
    }

    public void setItemBalanceList(List<ItemBalanceDTO> itemBalanceList) {
        this.itemBalanceList = itemBalanceList;
    }
}
