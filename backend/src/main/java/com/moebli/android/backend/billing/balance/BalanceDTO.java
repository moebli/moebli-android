package com.moebli.android.backend.billing.balance;

import java.io.Serializable;

public class BalanceDTO  implements Serializable {

    private String currency;

    private Integer balance;
    
    private String balanceFormatted;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getBalanceFormatted() {
        return balanceFormatted;
    }

    public void setBalanceFormatted(String balanceFormatted) {
        this.balanceFormatted = balanceFormatted;
    }

}
