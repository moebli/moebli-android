package com.moebli.android.backend.billing.billlist;

import com.moebli.android.backend.billing.bill.BillDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class ItemBillsService {
    private static String C = ItemBillsService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static ItemBillsService mItemBillsService;
    private static HttpClientCallback mGetItemBillsCallback;

    private static Map<String, List<BillDTO>> mBillListMap = new HashMap<>();

    public void getItemBills(HttpClientCallback callback, String itemUuid) {
        if (mBillListMap.get(itemUuid) != null) {
            GetItemBillsResponseDTO dto = new GetItemBillsResponseDTO();
            dto.setBillList(mBillListMap.get(itemUuid));
            callback.handleResponse(dto);
            return;
        }
        mGetItemBillsCallback = callback;
        GetItemBillsRequestDTO requestDTO = new GetItemBillsRequestDTO();
        requestDTO.setItemUuid(itemUuid);
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                        "/api/getItemBills",
                requestDTO, new GetItemBillsBackendCallback(), GetItemBillsResponseDTO.class);
    }

    public class GetItemBillsBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if(o instanceof Throwable) {
                mGetItemBillsCallback.handleResponse(o);
                return;
            }
            GetItemBillsResponseDTO responseDTO = (GetItemBillsResponseDTO) o;
            mBillListMap.put(responseDTO.getItemUuid(), responseDTO.getBillList());
            mGetItemBillsCallback.handleResponse(o);
        }
    }

    public void addNewBills(List<BillDTO> newBillDTOs) {
        for (BillDTO newBillDTO : newBillDTOs) {
            List<BillDTO> billList = mBillListMap.get(newBillDTO.getItemUuid());
            if (billList == null)
                continue;

            boolean inserted = false;
            for (int i = 0; i < billList.size(); i++) {
                if (billList.get(i).getMonthYearCode().compareTo(newBillDTO.getMonthYearCode()) <=
                        0) {
                    billList.add(i, newBillDTO);
                    inserted = true;
                    break;
                }
            }
            if (!inserted) {
                billList.add(newBillDTO);
            }
        }
    }

    public void updateBill(BillDTO updatedBill) {
        // itemUuid may have changed; find original bill first and delete
        for (String itemUuid : mBillListMap.keySet()) {
            List<BillDTO> billList = mBillListMap.get(itemUuid);
            boolean deleted = false;
            for (int i = 0; i < billList.size(); i++) {
                if (billList.get(i).getUuid().equals(updatedBill.getUuid())) {
                    billList.remove(i);
                    deleted = true;
                    break;
                }
            }
            if (deleted)
                break;
        }

        // now insert bill
        List<BillDTO> billList = new ArrayList<>();
        billList.add(updatedBill);
        addNewBills(billList);
    }

    public void deleteBill(String deletedBillUuid) {
        for (List<BillDTO> billList : mBillListMap.values()) {
            for (int i = 0; i < billList.size(); i++) {
                if (billList.get(i).getUuid().equals(deletedBillUuid)) {
                    billList.remove(i);
                    return;
                }
            }
        }
    }

    public void flushCache() {
        mBillListMap = new HashMap<>();
    }

    public static ItemBillsService getInstance() {
        if (mItemBillsService == null)
            mItemBillsService = new ItemBillsService();
        return mItemBillsService;
    }

    private ItemBillsService() {
    }
}
