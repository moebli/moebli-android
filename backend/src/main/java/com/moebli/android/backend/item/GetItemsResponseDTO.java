package com.moebli.android.backend.item;

import java.util.List;

public class GetItemsResponseDTO {

    private List<ItemDTO> itemList;

    public List<ItemDTO> getItemList() {
        return itemList;
    }

    public void setItemList(List<ItemDTO> itemsList) {
        this.itemList = itemsList;
    }

}
