package com.moebli.android.backend.enums;

public enum SharedPrefs {
    ECID, LOGIN_ID, SESSION_STORE_KEY, TERMS_ACCEPTED, UPDATE_APP_SHOWN_DATE,

    // google cloud messaging registration
    SENT_TOKEN_TO_SERVER, REGISTRATION_COMPLETE
}
