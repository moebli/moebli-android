package com.moebli.android.backend.user;

import com.moebli.android.backend.util.BaseResponseDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class InitPasswordResetService {

    private static String C = InitPasswordResetService.class.getName();
    private static InitPasswordResetService mInitPasswordResetService;
    private static HttpClientUtil httpClient = new HttpClientUtil();

    public void initPasswordReset(HttpClientCallback callback, String phoneOrEmail) {
        InitPasswordResetRequestDTO requestDTO = new InitPasswordResetRequestDTO();
        requestDTO.setPhoneOrEmail(phoneOrEmail);
        httpClient.executeRequest(HttpClientUtil.Method.POST, BASE_URL +
                        "/api/initPasswordReset",
                requestDTO, callback, BaseResponseDTO.class);
    }

    public static InitPasswordResetService getInstance() {
        if (mInitPasswordResetService == null)
            mInitPasswordResetService = new InitPasswordResetService();
        return mInitPasswordResetService;
    }

    private InitPasswordResetService() {
    }
}
