package com.moebli.android.backend.session;

import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import org.json.JSONException;
import org.json.JSONObject;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class FacebookLoginService {
    private static String C = FacebookLoginService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();

    public void facebookLogin(HttpClientCallback callback, JSONObject jsonObject) {
        FacebookLoginRequestDTO requestDTO = new FacebookLoginRequestDTO();
        try {
            requestDTO.setId(jsonObject.getString("id"));
            requestDTO.setEmail(jsonObject.getString("email"));
            requestDTO.setFirstName(jsonObject.getString("first_name"));
            requestDTO.setLastName(jsonObject.getString("last_name"));
            requestDTO.setLink(jsonObject.getString("link"));
            requestDTO.setLocale(jsonObject.getString("locale"));
            requestDTO.setName(jsonObject.getString("name"));
            requestDTO.setTimezone(String.valueOf(jsonObject.getDouble("time_zone")));
            requestDTO.setVerified(String.valueOf(jsonObject.getBoolean("verified")));
            requestDTO.setUpdatedTime(requestDTO.getUpdatedTime());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        httpClient.executeRequest(HttpClientUtil.Method.POST, BASE_URL +
                        "/api/facebookLogin",
                requestDTO, callback, LoginResponseDTO.class);
    }

}
