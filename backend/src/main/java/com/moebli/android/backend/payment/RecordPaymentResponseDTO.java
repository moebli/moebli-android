package com.moebli.android.backend.payment;

import com.moebli.android.backend.billing.balance.ContactBalanceDTO;
import com.moebli.android.backend.billing.balance.ItemBalanceDTO;
import com.moebli.android.backend.billing.balance.MonthlyBalanceDTO;
import com.moebli.android.backend.billing.bill.BillDTO;
import com.moebli.android.backend.contact.ContactDTO;
import com.moebli.android.backend.item.ItemDTO;
import com.moebli.android.backend.util.ErrorDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RecordPaymentResponseDTO implements Serializable {

    private List<BillDTO> billList = new ArrayList<BillDTO>();

    // even though this is a set, call it a list for consistency in API specs
    // newly created contacts are returned
    private List<ContactDTO> newContactList = new ArrayList<>();

    // new item is returned, if created
    private ItemDTO newItem;

    private String success;

    private List<ErrorDTO> errorList;

    private List<String> blockedContactList;

    private List<MonthlyBalanceDTO> monthlyBalanceList;

    private List<ContactBalanceDTO> contactBalanceList;

    private List<ItemBalanceDTO> itemBalanceList;

    public StringBuilder toStringBuilder() {
        StringBuilder buf = new StringBuilder(100);
        buf.append("CreateBillResponseDTO { ");
        buf.append("billList:").append(billList);
        buf.append("; newContactList:").append(newContactList);
        buf.append("; newItem:").append(newItem == null ? "null" : newItem);
        buf.append("; errors:").append(errorList);
        buf.append("; blockedContactList:").append(blockedContactList);
        buf.append(" }");
        return buf;
    }

    public List<BillDTO> getBillList() {
        return billList;
    }

    public void setBillList(List<BillDTO> billList) {
        this.billList = billList;
    }

    public List<ContactDTO> getNewContactList() {
        return newContactList;
    }

    public void setNewContactList(List<ContactDTO> newContactList) {
        this.newContactList = newContactList;
    }

    public ItemDTO getNewItem() {
        return newItem;
    }

    public void setNewItem(ItemDTO newItem) {
        this.newItem = newItem;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<ErrorDTO> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<ErrorDTO> errorList) {
        this.errorList = errorList;
    }

    public List<String> getBlockedContactList() {
        return blockedContactList;
    }

    public void setBlockedContactList(List<String> blockedContactList) {
        this.blockedContactList = blockedContactList;
    }

    public List<MonthlyBalanceDTO> getMonthlyBalanceList() {
        return monthlyBalanceList;
    }

    public void setMonthlyBalanceList(List<MonthlyBalanceDTO> monthlyBalanceList) {
        this.monthlyBalanceList = monthlyBalanceList;
    }

    public List<ContactBalanceDTO> getContactBalanceList() {
        return contactBalanceList;
    }

    public void setContactBalanceList(List<ContactBalanceDTO> contactBalanceList) {
        this.contactBalanceList = contactBalanceList;
    }

    public List<ItemBalanceDTO> getItemBalanceList() {
        return itemBalanceList;
    }

    public void setItemBalanceList(List<ItemBalanceDTO> itemBalanceList) {
        this.itemBalanceList = itemBalanceList;
    }

    @Override
    public String toString() {
        return toStringBuilder().toString();
    }
}