package com.moebli.android.backend.billing.bill;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@SuppressWarnings("serial")
public class DeleteBillRequestDTO implements Serializable {

    private String billUuid;

    public String getBillUuid() {
        return billUuid;
    }

    public void setBillUuid(String billUuid) {
        if (StringUtils.isNotBlank(billUuid))
            this.billUuid = billUuid;
    }

}
