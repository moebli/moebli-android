package com.moebli.android.backend.billing.billlist;

public class GetItemBillsRequestDTO {

	private String itemUuid;
	
	private String startIndex;
	
	private String range;

	public String getItemUuid() {
		return itemUuid;
	}

	public void setItemUuid(String itemUuid) {
		this.itemUuid = itemUuid;
	}

	public String getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(String startIndex) {
		this.startIndex = startIndex;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}
 }
