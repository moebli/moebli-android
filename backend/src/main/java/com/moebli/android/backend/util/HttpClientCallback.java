package com.moebli.android.backend.util;

public interface HttpClientCallback {
    void handleResponse(Object responseDTO);
}
