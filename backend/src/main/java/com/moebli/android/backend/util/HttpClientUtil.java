package com.moebli.android.backend.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.util.Log;

import com.moebli.android.backend.session.SessionService;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.EntityEnclosingRequestWrapper;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// cannot make this a singleton because couple of non-static class variables
public class HttpClientUtil {

    private static String C = HttpClientUtil.class.getName();

    private HttpClientCallback callback;
    private Class clazz;

    public enum Method {GET, POST}

    public <T> void executeRequest(Method method, String url, Object object, HttpClientCallback callback,
                                   Class<T> clazz) {
        this.callback = callback;
        this.clazz = clazz;

        HttpRequestBase request = null;
        switch (method) {
            case GET:
                StringBuilder buf = new StringBuilder(url);
                boolean firstParamAdded = false;
                if (object != null) {
                    Field[] fields = object.getClass().getDeclaredFields();
                    for (Field field : fields) {
                        field.setAccessible(true);
                        String value;
                        try {
                            value = (String) field.get(object);
                        } catch (IllegalAccessException e) {
                            throw new RuntimeException(e);
                        }
                        if (value == null)
                            continue;
                        if (!firstParamAdded) {
                            buf.append(url.contains("?") ? "&" : "?");
                            firstParamAdded = true;
                        } else {
                            buf.append("&");
                        }
                        try {
                            value = URLEncoder.encode(value, "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            throw new RuntimeException(e);
                        }
                        buf.append(field.getName()).append("=").append(value);
                    }
                }
                request = new HttpGet(buf.toString());
                break;
            case POST:
                request = new HttpPost(url);
                List<NameValuePair> params = new ArrayList<>();
                if (object != null) {
                    Field[] fields = object.getClass().getDeclaredFields();
                    for (Field field : fields) {
                        field.setAccessible(true);
                        try {
                            params.add(new BasicNameValuePair(field.getName(), (String) field.get(object)));
                        } catch (IllegalAccessException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }
                try {
                    ((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException(e);
                }
        }

        new HttpAsyncTask().execute(request);
    }

    private class HttpAsyncTask extends AsyncTask<HttpRequestBase, Void, HttpResponseWrapper> {

        private Exception exception;

        protected HttpResponseWrapper doInBackground(HttpRequestBase... requests) {
            // get default http client
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpRequestBase request = requests[0];

            // set timeout params
            HttpParams httpParams = httpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
            HttpConnectionParams.setSoTimeout(httpParams, 10000);
            httpClient.addRequestInterceptor(new HttpClientRequestLogger());

            // add session id, ecid header
            String sessionId = SessionService.getInstance().getSessionId();
            if (sessionId != null) {
                Header header = new BasicHeader("sessionId", sessionId);
                request.addHeader(header);
            }
            String ecid = SessionService.getInstance().getEcid();
            if (ecid != null) {
                Header header = new BasicHeader("ecid", ecid);
                request.addHeader(header);
            }

            /*
            // add ecid cookie
            CookieStore cookieStore = new BasicCookieStore();
            BasicClientCookie cookie = new BasicClientCookie("ecid", "global"); // TODO
            cookie.setPath("/");
            cookieStore.addCookie(cookie);
            httpClient.setCookieStore(cookieStore);
            */

            // response object
            HttpResponseWrapper responseWrapper = new HttpResponseWrapper();

            // call api
            try {
                long startTime = System.currentTimeMillis();
                HttpResponse response = httpClient.execute(request);
                long responseTime = System.currentTimeMillis() - startTime;
                String content = (request instanceof HttpPost) ? getRequestContent(((HttpPost) request).getEntity())
                        : ((HttpGet) request).getURI().toString();
                Log.i(C, StringUtils.substring(content, 0, 1000));
                responseWrapper.setStatusCode(response.getStatusLine().getStatusCode());
                responseWrapper.setContent(logAndGetResponseContent(response));
                responseWrapper.setHeaders(logAndGetResponseHeaders(response));
                Log.i(C, request.getRequestLine() + " " + responseTime);
            } catch (IOException e) {
                Log.i(C, e.getStackTrace().toString());
                this.exception = e;
                return null;
            }
            return responseWrapper;
        }

        protected void onPostExecute(HttpResponseWrapper response) {
            if (callback == null)
                return;

            if (this.exception != null) {
                callback.handleResponse(this.exception);
                return;
            }

            if (200 == response.getStatusCode()) {
                Object object = JsonUtil.getJSONtoObject(response.getContent(), clazz);
                callback.handleResponse(object);
            }
        }
    }

    private static class HttpClientRequestLogger implements HttpRequestInterceptor {
        public void process(HttpRequest request, HttpContext context) throws HttpException, IOException {
            StringBuilder buf = new StringBuilder();
            buf.append("\n" + request.getRequestLine() + "\n");
            for (Header header : request.getAllHeaders())
                buf.append(header.getName() + ": " + header.getValue() + "\n");
            buf.append("\n");
            if (request instanceof EntityEnclosingRequestWrapper) {
                HttpEntity httpEntity = ((EntityEnclosingRequestWrapper) request).getEntity();
                buf.append(getRequestContent(httpEntity));
            }
            Log.i(C, buf.toString());
        }
    }

    private static String getRequestContent(HttpEntity httpEntity) throws IOException {
        OutputStream out = new ByteArrayOutputStream();
        httpEntity.writeTo(out);
        String content = out.toString();
        out.close();
        return content;
    }

    private String logAndGetResponseContent(HttpResponse response) throws IllegalStateException, IOException {
        if (response.getStatusLine().getStatusCode() != 200)
            Log.i(C, response.getStatusLine().toString());

        // stream to string
        StringBuilder buf = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line;
        while ((line = rd.readLine()) != null) {
            buf.append(line);
        }
        String resp = buf.toString();

        // log and return response string
        buf = new StringBuilder();
        buf.append("\n" + response.getStatusLine() + "\n");
        buf.append("\n").append(resp);
        Log.i(C, StringUtils.substring(buf.toString(), 0, 1000));
        return resp;
    }

    private Map<String, String> logAndGetResponseHeaders(HttpResponse response) {
        Map<String, String> headers = new HashMap<String, String>();
        for (Header header : response.getAllHeaders())
            headers.put(header.getName(), header.getValue());
        Log.i(C, headers.toString());
        return headers;
    }

    private static class HttpResponseWrapper {
        private int statusCode;
        private String content;
        private Map<String, String> headers;

        public int getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(int statusCode) {
            this.statusCode = statusCode;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public void setHeaders(Map<String, String> headers) {
            this.headers = headers;
        }

        public Map<String, String> getHeaders() {
            return headers;
        }

        public String getHeader(String name) {
            return (headers == null) ? "" : headers.get(name);
        }
    }
}
