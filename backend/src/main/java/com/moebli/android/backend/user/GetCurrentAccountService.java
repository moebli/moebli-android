package com.moebli.android.backend.user;

import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class GetCurrentAccountService {
    private static String C = GetCurrentAccountService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static HttpClientCallback mGetCurrentAccountCallback;

    public void getCurrentAccount(HttpClientCallback callback) {
        mGetCurrentAccountCallback = callback;
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                        "/api/getCurrentAccount",
                null, new GetCurrentAccountBackendCallback(), GetAccountResponseDTO.class);
    }

    public class GetCurrentAccountBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            mGetCurrentAccountCallback.handleResponse(o);
        }
    }
}
