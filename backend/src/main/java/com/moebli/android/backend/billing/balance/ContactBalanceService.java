package com.moebli.android.backend.billing.balance;

import com.moebli.android.backend.contact.ContactDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class ContactBalanceService {
    private static String C = ContactBalanceService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static ContactBalanceService mContactBalanceService;
    private static HttpClientCallback getContactBalancesCallback;
    private static List<ContactBalanceDTO> mContactBalanceList;

    public void getContactBalances(HttpClientCallback callback) {
        if (mContactBalanceList != null) {
            GetContactBalancesResponseDTO dto = new GetContactBalancesResponseDTO();
            dto.setContactBalanceList(mContactBalanceList);
            callback.handleResponse(dto);
            return;
        }
        getContactBalancesCallback = callback;
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                        "/api/getContactBalances",
                null, new GetContactBalancesBackendCallback(), GetContactBalancesResponseDTO.class);
    }

    public class GetContactBalancesBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if(o instanceof Throwable) {
                getContactBalancesCallback.handleResponse(o);
                return;
            }
            GetContactBalancesResponseDTO response = (GetContactBalancesResponseDTO) o;
            mContactBalanceList = response.getContactBalanceList();
            getContactBalancesCallback.handleResponse(o);
        }
    }

    public ContactBalanceDTO getContactBalanceDTO(String contactUuid) {
        for(ContactBalanceDTO contactDTO: mContactBalanceList) {
            if(contactDTO.getAccountUuid().equals(contactUuid))
                return contactDTO;
        }
        return null;
    }

    public List<ContactBalanceDTO>  getUnblockedContactBalances() {
        List<ContactBalanceDTO> unblockedList = new ArrayList<>();
        for (ContactBalanceDTO contactDTO : mContactBalanceList) {
            if (! contactDTO.getStatus().equals("blocked"))
                unblockedList.add(contactDTO);
        }
        return unblockedList;
    }

    public static List<ContactBalanceDTO> getUnblockedContactBalances(List<ContactBalanceDTO>
                                                                              contactBalanceDTOList) {
        List<ContactBalanceDTO> unblockedList = new ArrayList<>();
        for (ContactBalanceDTO contactDTO : contactBalanceDTOList) {
            if (!contactDTO.getStatus().equals("blocked")) unblockedList.add(contactDTO);
        }
        return unblockedList;
    }

    public List<String> getUnblockedContactEmails() {
        List<String> emails = new ArrayList<>();
        List<ContactBalanceDTO> contactList = getUnblockedContactBalances();
        for(ContactBalanceDTO contactDTO : contactList) {
            if(StringUtils.isNotBlank(contactDTO.getEmail()))
                emails.add(contactDTO.getEmail());
        }
        return emails;
    }

    public List<String> getUnblockedContactStrings() {
        List<String> strings = new ArrayList<>();
        List<ContactBalanceDTO> contactList = getUnblockedContactBalances();
        for(ContactBalanceDTO contactDTO : contactList) {
            if(StringUtils.isNotBlank(contactDTO.getEmail())) {
                if (contactDTO.getEmail().equals(contactDTO.getAccountName()))
                    strings.add(contactDTO.getEmail());
                else
                    strings.add(contactDTO.getAccountName() + " ( " + contactDTO.getEmail() + " )");
            }
        }
        return strings;
    }

    public void updateContactStatus(ContactDTO contactDTO) {
        if (mContactBalanceList == null)
            return;
        for (ContactBalanceDTO contactBalanceDTO : mContactBalanceList) {
            if (contactBalanceDTO.getAccountUuid().equals(contactDTO.getAccountUuid())) {
                contactBalanceDTO.setStatus(contactDTO.getStatus());
                break;
            }
        }
    }

    public void updateContactEmailAcceptance(ContactDTO contactDTO) {
        if (mContactBalanceList == null)
            return;
        for (ContactBalanceDTO contactBalanceDTO : mContactBalanceList) {
            if (contactBalanceDTO.getAccountUuid().equals(contactDTO.getAccountUuid())) {
                contactBalanceDTO.setEmailAcceptance(contactDTO.getEmailAcceptance());
                break;
            }
        }
    }

    public void addNewContacts(List<ContactDTO> contactList) {
        for (ContactDTO contactDTO : contactList) {
            ContactBalanceDTO contactBalanceDTO = getContactBalanceDTO(contactDTO);
            boolean inserted = false;
            for (int i = 0; i < mContactBalanceList.size(); i++) {
                if (mContactBalanceList.get(i).getAccountName().compareToIgnoreCase(contactBalanceDTO
                        .getAccountName()) > 0) {
                    mContactBalanceList.add(i, contactBalanceDTO);
                    inserted = true;
                    break;
                }
            }
            if (! inserted) {
                mContactBalanceList.add(contactBalanceDTO);
            }
        }
    }

    private ContactBalanceDTO getContactBalanceDTO(ContactDTO contactDTO) {
        ContactBalanceDTO contactBalanceDTO = new ContactBalanceDTO();
        contactBalanceDTO.setAccountUuid(contactDTO.getAccountUuid());
        contactBalanceDTO.setAccountName(contactDTO.getAccountName());
        contactBalanceDTO.setFirstName(contactDTO.getFirstName());
        contactBalanceDTO.setLastName(contactDTO.getLastName());
        contactBalanceDTO.setEmail(contactDTO.getEmail());
        contactBalanceDTO.setMobileIddCode(contactDTO.getMobileIddCode());
        contactBalanceDTO.setMobileNumber(contactDTO.getMobileNumber());
        contactBalanceDTO.setSourceInternalNotes(contactDTO.getSourceInternalNotes());
        contactBalanceDTO.setStatus(contactDTO.getStatus());
        contactBalanceDTO.setEmailAcceptance(contactDTO.getEmailAcceptance());
        return contactBalanceDTO;
    }

    public void addOrUpdateContactBalances(List<ContactBalanceDTO> contactBalanceList) {
        if (mContactBalanceList == null)
            return;

        List<ContactBalanceDTO> newContactList = new ArrayList<>();
        boolean foundContact = false;
        for (ContactBalanceDTO newContactBalance : contactBalanceList) {
            for (ContactBalanceDTO contactBalance : mContactBalanceList) {
                if (contactBalance.getAccountUuid().equals(newContactBalance.getAccountUuid())) {
                    boolean foundBalanceCurrency = false;
                    for (BalanceDTO balanceDTO : contactBalance.getBalanceList()) {
                        for (BalanceDTO newBalanceDTO : newContactBalance.getBalanceList()) {
                            if (balanceDTO.getCurrency().equals(newBalanceDTO.getCurrency())) {
                                balanceDTO.setBalance(newBalanceDTO.getBalance());
                                balanceDTO.setBalanceFormatted(newBalanceDTO.getBalanceFormatted());
                                foundBalanceCurrency = true;
                                break;
                            }
                        }
                    }
                    if (! foundBalanceCurrency) {
                        contactBalance.getBalanceList().addAll(newContactBalance.getBalanceList());
                    }
                    foundContact = true;
                    break;
                }
            }
            if (! foundContact) {
                newContactList.add(newContactBalance);
            }
        }

        // following is not needed because new contacts were added prior to calling this method
        // insert new contacts alphabetically
        /*
        for (ContactBalanceDTO newContact : newContactList) {
            boolean inserted = false;
            for (int i = 0; i < mContactBalanceList.size(); i++) {
                if (mContactBalanceList.get(i).getAccountName().compareTo(newContact
                        .getAccountName()) > 0) {
                    mContactBalanceList.add(i, newContact);
                    inserted = true;
                    break;
                }
            }
            if (! inserted) {
                mContactBalanceList.add(newContact);
            }
        }
        */
    }

    public void flushCache() {
        mContactBalanceList = null;
    }

    public static ContactBalanceService getInstance() {
        if (mContactBalanceService == null)
            mContactBalanceService = new ContactBalanceService();
        return mContactBalanceService;
    }

    private ContactBalanceService() {
    }
}
