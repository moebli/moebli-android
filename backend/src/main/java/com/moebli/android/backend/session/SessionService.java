package com.moebli.android.backend.session;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.moebli.android.backend.enums.Constants;
import com.moebli.android.backend.enums.SharedPrefs;
import com.moebli.android.backend.util.JsonUtil;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

public class SessionService {
    private static String C = SessionService.class.getName();
    private static SessionService mSessionService;

    // class variables
    private SessionVO mSessionVO;
    private String mEcid;

    public String getSessionId(Activity activity) {
        return getSessionVO(activity).getSessionId();
    }

    public String getLocale(Activity activity) {
        return getSessionVO(activity).getLocale();
    }

    public String getCurrencyCode(Activity activity) {
        return getSessionVO(activity).getCurrencyCode();
    }

    public String getCountryCode(Activity activity) {
        return getSessionVO(activity).getCountryCode();
    }

    public String getJavaDataFormat(Activity activity) {
        return getSessionVO(activity).getJavaDateFormat();
    }

    private SessionVO getSessionVO(Activity activity) {
        if (mSessionVO != null) return mSessionVO;
        SharedPreferences sharedPref = activity.getSharedPreferences(Constants.PREF_FILE, Context
                .MODE_PRIVATE);
        String sessionStr = sharedPref.getString(SharedPrefs.SESSION_STORE_KEY.name(), null);
        if (sessionStr == null) {
            mSessionVO = new SessionVO();
            return mSessionVO;
        }
        mSessionVO = JsonUtil.getJSONtoObject(sessionStr, SessionVO.class);
        return mSessionVO;
    }

    public String getSessionId() {
        if (mSessionVO == null)
            return null;
        return mSessionVO.getSessionId();
    }

    public void saveNewSession(Activity activity, GetSessionResponseDTO responseDTO) {
        SessionVO sessionVO = getSessionVO(activity);
        sessionVO.setSessionId(responseDTO.getSessionId());
        sessionVO.setCountryCode(responseDTO.getCountryCode());
        sessionVO.setLocale(responseDTO.getLocale());

        String sessionStr = JsonUtil.getObjectToJSON(sessionVO);
        SharedPreferences sharedPref = activity.getSharedPreferences(Constants.PREF_FILE, Context
                .MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(SharedPrefs.SESSION_STORE_KEY.name(), sessionStr);
        editor.commit();
    }

    public void postLoginSessionUpdate(Activity activity, String loginId, LoginResponseDTO
                                       loginResponseDTO) {
        SessionVO sessionVO = getSessionVO(activity);
        sessionVO.setCurrencyCode(loginResponseDTO.getCurrencyCode());
        sessionVO.setCurrencySymbol(loginResponseDTO.getCurrencySymbol());
        sessionVO.setJavaDateFormat(loginResponseDTO.getJavaDateFormat());

        String sessionStr = JsonUtil.getObjectToJSON(sessionVO);
        SharedPreferences sharedPrefs = activity.getSharedPreferences(Constants.PREF_FILE, Context
                .MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(SharedPrefs.SESSION_STORE_KEY.name(), sessionStr);
        if (StringUtils.isNotBlank(loginId))
            editor.putString(SharedPrefs.LOGIN_ID.name(), loginId.trim());
        editor.commit();
    }

    public void removeSession(Activity activity) {
        SharedPreferences sharedPref = activity.getSharedPreferences(Constants.PREF_FILE, Context
                .MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(SharedPrefs.SESSION_STORE_KEY.name()).commit();
        editor.remove(SharedPrefs.LOGIN_ID.name()).commit();
        mSessionVO = null;
    }

    public void initializeEcid(Activity activity) {
        SharedPreferences sharedPref = activity.getSharedPreferences(Constants.PREF_FILE, Context
                .MODE_PRIVATE);
        mEcid = sharedPref.getString(SharedPrefs.ECID.name(), null);
        if (mEcid == null) {
            SharedPreferences.Editor editor = sharedPref.edit();
            mEcid = RandomStringUtils.randomAlphanumeric(8);
            editor.putString(SharedPrefs.ECID.name(), mEcid);
            editor.commit();
        }
    }

    public String getEcid() {
        return mEcid;
    }

    public static SessionService getInstance() {
        if (mSessionService == null)
            mSessionService = new SessionService();
        return mSessionService;
    }

    private SessionService() {
    }

}
