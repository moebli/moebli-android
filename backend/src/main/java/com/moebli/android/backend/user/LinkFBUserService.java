package com.moebli.android.backend.user;

import com.moebli.android.backend.session.LoginResponseDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class LinkFBUserService {

    private static String C = LinkFBUserService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();

    public void linkFBUser(HttpClientCallback callback, String password) {
        LinkFBUserRequestDTO requestDTO = new LinkFBUserRequestDTO();
        requestDTO.setPassword(password);
        httpClient.executeRequest(HttpClientUtil.Method.POST, BASE_URL +
                        "/api/linkFacebookUser",
                requestDTO, callback, LoginResponseDTO.class);
    }
}
