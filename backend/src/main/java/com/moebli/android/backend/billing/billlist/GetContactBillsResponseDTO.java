package com.moebli.android.backend.billing.billlist;

import com.moebli.android.backend.billing.bill.BillDTO;

import java.util.List;

public class GetContactBillsResponseDTO {

    private String contactAccountUuid;

	private List<BillDTO> billList;

	private Long version;

    public String getContactAccountUuid() {
        return contactAccountUuid;
    }

    public void setContactAccountUuid(String contactAccountUuid) {
        this.contactAccountUuid = contactAccountUuid;
    }

    public List<BillDTO> getBillList() {
		return billList;
	}

	public void setBillList(List<BillDTO> billList) {
		this.billList = billList;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
}
