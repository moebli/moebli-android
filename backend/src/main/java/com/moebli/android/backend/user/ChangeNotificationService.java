package com.moebli.android.backend.user;

import com.moebli.android.backend.util.BaseResponseDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class ChangeNotificationService {
    private static String C = ChangeNotificationService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static HttpClientCallback changeNotificationCallback;

    public void changeNotification(HttpClientCallback callback,boolean a) {
        changeNotificationCallback = callback;
        ChangeNotificationRequestDTO requestDTO = new ChangeNotificationRequestDTO();
        requestDTO.setSubscribe(a);
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                        "/api/changeNotificationSettings",
                requestDTO, new ChangeNotificationBackendCallback(), BaseResponseDTO.class);
    }

    public class ChangeNotificationBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            changeNotificationCallback.handleResponse(o);
        }
    }
}
