package com.moebli.android.backend.user;

import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class GetCurrentUserService {
    private static String C = GetCurrentUserService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static HttpClientCallback getCurrentUserCallback;

    public void getCurrentUser(HttpClientCallback callback) {
        getCurrentUserCallback = callback;
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                        "/api/getCurrentUser",
                null, new GetCurrentUserBackendCallback(), GetCurrentUserResponseDTO.class);
    }

    public class GetCurrentUserBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            getCurrentUserCallback.handleResponse(o);
        }
    }
}
