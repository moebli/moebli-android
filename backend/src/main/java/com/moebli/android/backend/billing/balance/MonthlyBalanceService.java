package com.moebli.android.backend.billing.balance;

import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import java.util.ArrayList;
import java.util.List;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class MonthlyBalanceService {
    private static String C = MonthlyBalanceService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static MonthlyBalanceService mMonthlyBalanceService;
    private static HttpClientCallback mGetMonthlyBalancesCallback;
    private static List<MonthlyBalanceDTO> mMonthlyBalanceList;

    public void getMonthlyBalances(HttpClientCallback callback) {
        if (mMonthlyBalanceList != null) {
            GetMonthlyBalancesResponseDTO dto = new GetMonthlyBalancesResponseDTO();
            dto.setMonthlyBalanceList(mMonthlyBalanceList);
            callback.handleResponse(dto);
            return;
        }
        mGetMonthlyBalancesCallback = callback;
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                        "/api/getMonthlyBalances",
                null, new GetMonthlyBalancesBackendCallback(), GetMonthlyBalancesResponseDTO.class);
    }

    public MonthlyBalanceDTO getMonthlyBalanceDTO(String monthYearCode) {
        for(MonthlyBalanceDTO monthlyBalanceDTO: mMonthlyBalanceList) {
            if(monthlyBalanceDTO.getMonthYearCode().equals(monthYearCode))
                return monthlyBalanceDTO;
        }
        return null;
    }

    class GetMonthlyBalancesBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if(o instanceof Throwable) {
                mGetMonthlyBalancesCallback.handleResponse(o);
                return;
            }
            GetMonthlyBalancesResponseDTO response = (GetMonthlyBalancesResponseDTO) o;
            mMonthlyBalanceList = response.getMonthlyBalanceList();
            mGetMonthlyBalancesCallback.handleResponse(o);
        }
    }

    public void addOrUpdateMonthlyBalances(List<MonthlyBalanceDTO> monthlyBalanceList) {
        if (mMonthlyBalanceList == null)
            return;

        List<MonthlyBalanceDTO> newMonthList = new ArrayList<>();
        boolean foundMonth = false;
        for (MonthlyBalanceDTO newMonthlyBalance : monthlyBalanceList) {
            for (MonthlyBalanceDTO monthlyBalance : mMonthlyBalanceList) {
                if (monthlyBalance.getMonthYearCode().equals(newMonthlyBalance.getMonthYearCode())) {
                    boolean foundBalanceCurrency = false;
                    for (BalanceDTO balanceDTO : monthlyBalance.getBalanceList()) {
                        for (BalanceDTO newBalanceDTO : newMonthlyBalance.getBalanceList()) {
                            if (balanceDTO.getCurrency().equals(newBalanceDTO.getCurrency())) {
                                balanceDTO.setBalance(newBalanceDTO.getBalance());
                                balanceDTO.setBalanceFormatted(newBalanceDTO.getBalanceFormatted());
                                foundBalanceCurrency = true;
                                break;
                            }
                        }
                    }
                    if (! foundBalanceCurrency) {
                        monthlyBalance.getBalanceList().addAll(newMonthlyBalance.getBalanceList());
                    }
                    foundMonth = true;
                    break;
                }
            }
            if (! foundMonth) {
                newMonthList.add(newMonthlyBalance);
            }
        }

        // add new month-year reverse chronologically
        for (MonthlyBalanceDTO newMonth : newMonthList) {
            boolean inserted = false;
            for (int i = 0; i < mMonthlyBalanceList.size(); i++) {
                if (mMonthlyBalanceList.get(i).getMonthYearCode().compareTo(newMonth
                        .getMonthYearCode()) < 0) {
                    mMonthlyBalanceList.add(i, newMonth);
                    inserted = true;
                    break;
                }
            }
            if (! inserted) {
                mMonthlyBalanceList.add(newMonth);
            }
        }
    }

    public void deleteMonthlyBalance(String monthYearCode) {
        for (int i = 0; i < mMonthlyBalanceList.size(); i++) {
            if (mMonthlyBalanceList.get(i).getMonthYearCode().equals(monthYearCode)) {
                mMonthlyBalanceList.remove(i);
                return;
            }
        }
    }

    public void flushCache() {
        mMonthlyBalanceList = null;
    }

    public static MonthlyBalanceService getInstance() {
        if (mMonthlyBalanceService == null)
            mMonthlyBalanceService = new MonthlyBalanceService();
        return mMonthlyBalanceService;
    }

    private MonthlyBalanceService() {
    }
}
