package com.moebli.android.backend.contact;

import java.io.Serializable;

public class ContactDTO implements Serializable {

    private String accountUuid;

    private String accountName;

    private String firstName;

    private String lastName;

    private String email;

    private String mobileIddCode;

    private String mobileNumber;

    private String sourceInternalNotes;

    private String status;

    private String emailAcceptance;

    public String getAccountUuid() {
        return accountUuid;
    }

    public void setAccountUuid(String accountUuid) {
        this.accountUuid = accountUuid;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileIddCode() {
        return mobileIddCode;
    }

    public void setMobileIddCode(String mobileIddCode) {
        this.mobileIddCode = mobileIddCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getSourceInternalNotes() {
        return sourceInternalNotes;
    }

    public void setSourceInternalNotes(String sourceInternalNotes) {
        this.sourceInternalNotes = sourceInternalNotes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmailAcceptance() {
        return emailAcceptance;
    }

    public void setEmailAcceptance(String emailAcceptance) {
        this.emailAcceptance = emailAcceptance;
    }

}
