package com.moebli.android.backend.payment;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.List;

public class RecordPaymentRequestDTO implements Serializable {

    private String paymentMethod;

    private String xPaidY;

    private String itemName;

    private String itemUuid;

    private String itemDate;

    private String currencyCode;

    private String contact0;

    private String contact1;

    private String contact2;

    private String contact3;

    private String contact4;

    private String contact5;

    private String contact6;

    private String contact7;

    private String contact8;

    private String contact9;

    private String amount0;

    private String amount1;

    private String amount2;

    private String amount3;

    private String amount4;

    private String amount5;

    private String amount6;

    private String amount7;

    private String amount8;

    private String amount9;

    private String externalNotes;

    private List<String> contacts;

    private List<String> amounts;

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getxPaidY() {
        return xPaidY;
    }

    public void setxPaidY(String xPaidY) {
        this.xPaidY = xPaidY;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemUuid() {
        return itemUuid;
    }

    public void setItemUuid(String itemUuid) {
        if (StringUtils.isNotBlank(itemUuid))
            this.itemUuid = itemUuid;
    }

    public String getItemDate() {
        return itemDate;
    }

    public void setItemDate(String itemDate) {
        this.itemDate = itemDate;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getContact0() {
        return contact0;
    }

    public void setContact0(String contact0) {
        this.contact0 = contact0;
    }

    public String getContact1() {
        return contact1;
    }

    public void setContact1(String contact1) {
        this.contact1 = contact1;
    }

    public String getContact2() {
        return contact2;
    }

    public void setContact2(String contact2) {
        this.contact2 = contact2;
    }

    public String getContact3() {
        return contact3;
    }

    public void setContact3(String contact3) {
        this.contact3 = contact3;
    }

    public String getContact4() {
        return contact4;
    }

    public void setContact4(String contact4) {
        this.contact4 = contact4;
    }

    public String getContact5() {
        return contact5;
    }

    public void setContact5(String contact5) {
        this.contact5 = contact5;
    }

    public String getContact6() {
        return contact6;
    }

    public void setContact6(String contact6) {
        this.contact6 = contact6;
    }

    public String getContact7() {
        return contact7;
    }

    public void setContact7(String contact7) {
        this.contact7 = contact7;
    }

    public String getContact8() {
        return contact8;
    }

    public void setContact8(String contact8) {
        this.contact8 = contact8;
    }

    public String getContact9() {
        return contact9;
    }

    public void setContact9(String contact9) {
        this.contact9 = contact9;
    }

    public String getAmount0() {
        return amount0;
    }

    public void setAmount0(String amount0) {
        this.amount0 = amount0;
    }

    public String getAmount1() {
        return amount1;
    }

    public void setAmount1(String amount1) {
        this.amount1 = amount1;
    }

    public String getAmount2() {
        return amount2;
    }

    public void setAmount2(String amount2) {
        this.amount2 = amount2;
    }

    public String getAmount3() {
        return amount3;
    }

    public void setAmount3(String amount3) {
        this.amount3 = amount3;
    }

    public String getAmount4() {
        return amount4;
    }

    public void setAmount4(String amount4) {
        this.amount4 = amount4;
    }

    public String getAmount5() {
        return amount5;
    }

    public void setAmount5(String amount5) {
        this.amount5 = amount5;
    }

    public String getAmount6() {
        return amount6;
    }

    public void setAmount6(String amount6) {
        this.amount6 = amount6;
    }

    public String getAmount7() {
        return amount7;
    }

    public void setAmount7(String amount7) {
        this.amount7 = amount7;
    }

    public String getAmount8() {
        return amount8;
    }

    public void setAmount8(String amount8) {
        this.amount8 = amount8;
    }

    public String getAmount9() {
        return amount9;
    }

    public void setAmount9(String amount9) {
        this.amount9 = amount9;
    }

    public String getExternalNotes() {
        return externalNotes;
    }

    public void setExternalNotes(String externalNotes) {
        this.externalNotes = externalNotes;
    }

    public List<String> getContacts() {
        return contacts;
    }

    public void setContacts(List<String> contacts) {
        this.contacts = contacts;
    }

    public List<String> getAmounts() {
        return amounts;
    }

    public void setAmounts(List<String> amounts) {
        this.amounts = amounts;
    }
}

