package com.moebli.android.backend.billing.bill;


public class GetBillByUuidResponseDTO {

    private BillDTO bill;

    public BillDTO getBill() {
        return bill;
    }

    public void setBill(BillDTO bill) {
        this.bill = bill;
    }
    
}
