package com.moebli.android.backend.enums;

public enum TimeZoneEnum
{
    india("India Standard Time", "Asia/Kolkata"),
    pacific("Pacific Time", "America/Los_Angeles"),
    utc("Universal Time", "Etc/UTC"),
    ;

    private String name;
    private String javaCode;

    TimeZoneEnum(String name, String javaCode)
    {
        this.name = name;
        this.javaCode = javaCode;
    }

    public String getName()
    {
        return name;
    }

    public String getJavaCode()
    {
        return javaCode;
    }

}
