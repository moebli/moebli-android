package com.moebli.android.backend.billing.balance;

import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import java.util.ArrayList;
import java.util.List;
import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class ItemBalanceService {
    private static String C = ItemBalanceService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static ItemBalanceService mItemBalanceService;
    private static HttpClientCallback mGetItemBalancesCallback;
    private static List<ItemBalanceDTO> mItemBalanceList;

    public void getItemBalances(HttpClientCallback callback) {
        if (mItemBalanceList != null) {
            GetItemBalancesResponseDTO dto = new GetItemBalancesResponseDTO();
            dto.setItemBalanceList(mItemBalanceList);
            callback.handleResponse(dto);
            return;
        }
        mGetItemBalancesCallback = callback;
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                        "/api/getItemBalances",
                null, new GetItemBalancesBackendCallback(), GetItemBalancesResponseDTO.class);
    }

    public ItemBalanceDTO getItemBalanceDTO(String itemUuid) {
        for(ItemBalanceDTO itemBalanceDTO: mItemBalanceList) {
            if(itemBalanceDTO.getUuid().equals(itemUuid))
                return itemBalanceDTO;
        }
        return null;
    }

    public class GetItemBalancesBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if(o instanceof Throwable) {
                mGetItemBalancesCallback.handleResponse(o);
                return;
            }
            GetItemBalancesResponseDTO response = (GetItemBalancesResponseDTO) o;
            mItemBalanceList = response.getItemBalanceList();
            mGetItemBalancesCallback.handleResponse(o);
        }
    }

    public void addOrUpdateItemBalances(List<ItemBalanceDTO> itemBalanceList) {
        if (mItemBalanceList == null)
            return;

        List<ItemBalanceDTO> newItemList = new ArrayList<>();
        boolean foundItem = false;
        for (ItemBalanceDTO newItemBalance : itemBalanceList) {
            for (ItemBalanceDTO itemBalance : mItemBalanceList) {
                if (itemBalance.getUuid().equals(newItemBalance.getUuid())) {
                    itemBalance.setItemName(newItemBalance.getItemName());
                    boolean foundBalanceCurrency = false;
                    for (BalanceDTO balanceDTO : itemBalance.getBalanceList()) {
                        for (BalanceDTO newBalanceDTO : newItemBalance.getBalanceList()) {
                            if (balanceDTO.getCurrency().equals(newBalanceDTO.getCurrency())) {
                                balanceDTO.setBalance(newBalanceDTO.getBalance());
                                balanceDTO.setBalanceFormatted(newBalanceDTO.getBalanceFormatted());
                                foundBalanceCurrency = true;
                                break;
                            }
                        }
                    }
                    if (! foundBalanceCurrency) {
                        itemBalance.getBalanceList().addAll(newItemBalance.getBalanceList());
                    }
                    foundItem = true;
                    break;
                }
            }
            if (! foundItem) {
                newItemList.add(newItemBalance);
            }
        }

        // insert new items alphabetically
        for (ItemBalanceDTO newItem : newItemList) {
            boolean inserted = false;
            for (int i = 0; i < mItemBalanceList.size(); i++) {
                if (mItemBalanceList.get(i).getItemName().compareToIgnoreCase(newItem
                        .getItemName()) > 0) {
                    mItemBalanceList.add(i, newItem);
                    inserted = true;
                    break;
                }
            }
            if (! inserted) {
                mItemBalanceList.add(newItem);
            }
        }
    }

    public void deleteItemBalance(String itemUuid) {
        for (int i = 0; i < mItemBalanceList.size(); i++) {
            if (mItemBalanceList.get(i).getUuid().equals(itemUuid)) {
                mItemBalanceList.remove(i);
                return;
            }
        }
    }

    public void flushCache() {
        mItemBalanceList = null;
    }

    public static ItemBalanceService getInstance() {
        if (mItemBalanceService == null)
            mItemBalanceService = new ItemBalanceService();
        return mItemBalanceService;
    }

    private ItemBalanceService() {
    }
}
