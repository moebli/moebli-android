package com.moebli.android.backend.session;

import android.os.Build;
import android.telephony.TelephonyManager;

import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import org.apache.commons.lang3.StringUtils;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class GetSessionService {
    private static String C = GetSessionService.class.getName();
    private static HttpClientUtil mHttpClient = new HttpClientUtil();
    private static GetSessionService mGetSessionService;

    public void getSession(String versionName, TelephonyManager telephonyManager,
                           HttpClientCallback callback) {
        GetSessionRequestDTO requestDTO = new GetSessionRequestDTO();
        requestDTO.setAppVersion(versionName);
        requestDTO.setPlatform("android");
        requestDTO.setPlatformVersion(Build.VERSION.RELEASE); // android.os.Build.VERSION.RELEASE
        requestDTO.setMake(Build.MANUFACTURER); // android.os.Build.MANUFACTURER
        requestDTO.setModel(Build.MODEL); // android.os.Build.MODEL

        // http://stackoverflow.com/questions/2785485/is-there-a-unique-android-device-id
        String networkOperator = telephonyManager.getNetworkOperator();
        if (StringUtils.isNotBlank(networkOperator)) {
            requestDTO.setMcc(StringUtils.substring(networkOperator, 0, 3));
            requestDTO.setMnc(StringUtils.substring(networkOperator, 3));
            requestDTO.setDeviceId(telephonyManager.getDeviceId());
            requestDTO.setSubscriberId(telephonyManager.getSubscriberId());
        }

        mHttpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                "/api/getSession", requestDTO, callback,
                GetSessionResponseDTO.class);
    }

    public static GetSessionService getInstance() {
        if (mGetSessionService == null)
            mGetSessionService = new GetSessionService();
        return mGetSessionService;
    }

    private GetSessionService() {
    }

}
