package com.moebli.android.backend.session;

public class SetDeviceRegistrationRequestDTO {

    private String deviceId;

    private String registrationToken;

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getRegistrationToken() {
		return registrationToken;
	}

	public void setRegistrationToken(String registrationToken) {
		this.registrationToken = registrationToken;
	}

}
