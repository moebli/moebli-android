package com.moebli.android.backend.util;

import java.io.Serializable;

public class ErrorDTO implements Serializable
{
    private String fieldName;

    private String message;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return toStringBuilder().toString();
    }

    public StringBuilder toStringBuilder() {
        StringBuilder buf = new StringBuilder(100);
        buf.append("{ field:").append(fieldName).append("; message:").append(message).append(" }");
        return buf;
    }
}
