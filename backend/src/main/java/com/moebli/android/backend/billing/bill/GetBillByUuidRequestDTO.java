package com.moebli.android.backend.billing.bill;

public class GetBillByUuidRequestDTO {

    private String billUuid;

    public String getBillUuid() {
        return billUuid;
    }

    public void setBillUuid(String billUuid) {
        this.billUuid = billUuid;
    }

}
