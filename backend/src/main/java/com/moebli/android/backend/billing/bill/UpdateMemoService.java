package com.moebli.android.backend.billing.bill;

import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class UpdateMemoService {
    private static String C = UpdateMemoService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static UpdateMemoService mUpdateMemoService;
    private static HttpClientCallback updateMemoCallback;

    public void updateMemo(HttpClientCallback callback, UpdateMemoRequestDTO request) {
        updateMemoCallback = callback;
        httpClient.executeRequest(HttpClientUtil.Method.POST, BASE_URL + "/api/updateMemo",
                request, new UpdateMemoBackendCallback(), UpdateMemoResponseDTO.class);
    }

    public class UpdateMemoBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if(o instanceof Throwable) {
                updateMemoCallback.handleResponse(o);
                return;
            }
            UpdateMemoResponseDTO response = (UpdateMemoResponseDTO) o;
            updateMemoCallback.handleResponse(response);
        }
    }

    public static UpdateMemoService getInstance() {
        if (mUpdateMemoService == null)
            mUpdateMemoService = new UpdateMemoService();
        return mUpdateMemoService;
    }

    private UpdateMemoService() {

    }
}
