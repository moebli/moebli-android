package com.moebli.android.backend.billing.billlist;

public class GetContactBillsRequestDTO {

    private String contactAccountUuid;

    private String startIndex;

    private String range;

    public String getContactAccountUuid() {
        return contactAccountUuid;
    }

    public void setContactAccountUuid(String contactAccountUuid) {
        this.contactAccountUuid = contactAccountUuid;
    }

    public String getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(String startIndex) {
        this.startIndex = startIndex;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }
}
