package com.moebli.android.backend.session;

import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class LoginService {
    private static String C = LoginService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();

    public void login(HttpClientCallback callback, String phoneOrEmail, String password) {
        LoginRequestDTO requestDTO = new LoginRequestDTO(phoneOrEmail, password);
        httpClient.executeRequest(HttpClientUtil.Method.POST, BASE_URL + "/api/login",
                requestDTO, callback, LoginResponseDTO.class);
    }
}
