package com.moebli.android.backend.billing.bill;

import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class DeleteBillService {
    private static String C = DeleteBillService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static DeleteBillService mDeleteBillService;
    private static HttpClientCallback deleteBillCallback;

    public void deleteBill(HttpClientCallback callback, DeleteBillRequestDTO request) {
        deleteBillCallback = callback;
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL + "/api/deleteBill",
                request, new DeleteBillBackendCallback(), DeleteBillResponseDTO.class);
    }

    public class DeleteBillBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if(o instanceof Throwable) {
                deleteBillCallback.handleResponse(0);
                return;
            }
            DeleteBillResponseDTO response = (DeleteBillResponseDTO) o;
            deleteBillCallback.handleResponse(response);
        }
    }

    public static DeleteBillService getInstance() {
        if (mDeleteBillService == null)
            mDeleteBillService = new DeleteBillService();
        return mDeleteBillService;
    }

    private DeleteBillService() {
    }
}
