package com.moebli.android.backend.user;

public class InitPasswordResetRequestDTO {

    private String phoneOrEmail;

    public String getPhoneOrEmail() {
        return phoneOrEmail;
    }

    public void setPhoneOrEmail(String phoneOrEmail) {
        this.phoneOrEmail = phoneOrEmail;
    }

}
