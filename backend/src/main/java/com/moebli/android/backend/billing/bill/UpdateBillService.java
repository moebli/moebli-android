package com.moebli.android.backend.billing.bill;

import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class UpdateBillService {
    private static String C = UpdateBillService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static UpdateBillService mUpdateBillService;
    private static HttpClientCallback updateBillCallback;

    public void updateBill(HttpClientCallback callback, UpdateBillRequestDTO request) {
        updateBillCallback = callback;
        httpClient.executeRequest(HttpClientUtil.Method.POST, BASE_URL + "/api/updateBill",
                request, new UpdateBillBackendCallback(), UpdateBillResponseDTO.class);
    }

    public class UpdateBillBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if(o instanceof Throwable) {
                updateBillCallback.handleResponse(o);
                return;
            }
            UpdateBillResponseDTO response = (UpdateBillResponseDTO) o;
            updateBillCallback.handleResponse(response);
        }
    }

    public static UpdateBillService getInstance() {
        if (mUpdateBillService == null)
            mUpdateBillService = new UpdateBillService();
        return mUpdateBillService;
    }

    private UpdateBillService() {
    }
}
