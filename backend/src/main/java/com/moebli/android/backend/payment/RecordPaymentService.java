package com.moebli.android.backend.payment;

import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class RecordPaymentService {

    private static String C = RecordPaymentService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static RecordPaymentService mRecordPaymentService;
    private static HttpClientCallback paymentCallback;

    public void recordCashPayment(HttpClientCallback callback, RecordPaymentRequestDTO requestDTO) {
        paymentCallback = callback;
        httpClient.executeRequest(HttpClientUtil.Method.POST, BASE_URL +
                "/api/recordPayment", requestDTO, new RecordCashPaymentCallback(), RecordPaymentResponseDTO.class);
    }


    private class RecordCashPaymentCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if(o instanceof Throwable) {
                paymentCallback.handleResponse(o);
                return;
            }
            RecordPaymentResponseDTO paymentResponseDTO = (RecordPaymentResponseDTO) o;
            paymentCallback.handleResponse(paymentResponseDTO);
        }
    }

    private RecordPaymentService() {
    }

    public static RecordPaymentService getInstance() {
        if (mRecordPaymentService == null)
            mRecordPaymentService = new RecordPaymentService();
        return mRecordPaymentService;
    }
}
