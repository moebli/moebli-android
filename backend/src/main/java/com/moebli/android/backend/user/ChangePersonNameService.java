package com.moebli.android.backend.user;

import com.moebli.android.backend.util.BaseResponseDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class ChangePersonNameService {
    private static String C = ChangePersonNameService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static HttpClientCallback changePersonNameCallback;

    public void changeName(HttpClientCallback callback, String firstName, String lastName) {
        changePersonNameCallback = callback;
        ChangePersonNameRequestDTO requestDTO = new ChangePersonNameRequestDTO();

            requestDTO.setFirstName(firstName);
            requestDTO.setLastName(lastName);
            httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                            "/api/changePersonName",
                    requestDTO, new ChangeNameBackendCallback(), BaseResponseDTO.class);

    }

    public class ChangeNameBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            changePersonNameCallback.handleResponse(o);
        }
    }
}
