package com.moebli.android.backend.session;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;

import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import org.apache.commons.lang3.StringUtils;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class SetDeviceRegistrationService {
    private static String C = SetDeviceRegistrationService.class.getName();
    private static HttpClientUtil mHttpClient = new HttpClientUtil();
    private static SetDeviceRegistrationService mGetSessionService;

    public void setDeviceRegistration(Context context, String registrationToken,
                           HttpClientCallback callback) {
        SetDeviceRegistrationRequestDTO requestDTO = new SetDeviceRegistrationRequestDTO();
        requestDTO.setRegistrationToken(registrationToken);

        // http://stackoverflow.com/questions/2785485/is-there-a-unique-android-device-id
        TelephonyManager telephonyManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        String networkOperator = telephonyManager.getNetworkOperator();
        if (StringUtils.isNotBlank(networkOperator)) {
            requestDTO.setDeviceId(telephonyManager.getDeviceId());
        }

        mHttpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                "/api/setDeviceRegistration", requestDTO, callback,
                GetSessionResponseDTO.class);
    }

    public static SetDeviceRegistrationService getInstance() {
        if (mGetSessionService == null)
            mGetSessionService = new SetDeviceRegistrationService();
        return mGetSessionService;
    }

    private SetDeviceRegistrationService() {
    }

}
