package com.moebli.android.backend.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class JsonUtil {
    private static String C = JsonUtil.class.getName();

    public static String getMapToJSON(Map<String, String> map)
    {
        ObjectMapper mapper = new ObjectMapper();
        try
        {
            return mapper.writeValueAsString(map);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static String getObjectToJSON(Object object)
    {
        ObjectMapper mapper = new ObjectMapper();
        FilterProvider filters = new SimpleFilterProvider();
        ObjectWriter writer = mapper.writer(filters);

        try
        {
            return writer.writeValueAsString(object);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public static Map<String, String> getObjectToMap(Object object)
    {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> map = null;
        map = mapper.convertValue(object, HashMap.class);
        return map;
    }

    public static Map<String, String> getJSONToMap(String json)
    {
        ObjectMapper mapper = new ObjectMapper();
        try
        {
            return mapper.readValue(json, HashMap.class);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public static <T> T getJSONtoObject(String json, Class<T> clazz)
    {
        ObjectMapper mapper = new ObjectMapper();
        // mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try
        {
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return mapper.readValue(json, clazz);

        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

}
