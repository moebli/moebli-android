package com.moebli.android.backend.util;

public class GetContentRequestDTO {

	private String code;
	private String locale;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

}
