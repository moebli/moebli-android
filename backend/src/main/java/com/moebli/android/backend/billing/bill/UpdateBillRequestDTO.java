package com.moebli.android.backend.billing.bill;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@SuppressWarnings("serial")
public class UpdateBillRequestDTO implements Serializable {

    private String billUuid;

    private String itemName;

    private String payTo;

    private String billDate;

    private String amount;

    private String internalNotes;

    private String externalNotes;

    public String getBillUuid() {
        return billUuid;
    }

    public void setBillUuid(String billUuid) {
        if (StringUtils.isNotBlank(billUuid))
            this.billUuid = billUuid;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getPayTo() {
        return payTo;
    }

    public void setPayTo(String payTo) {
        this.payTo = payTo;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getInternalNotes() {
        return internalNotes;
    }

    public void setInternalNotes(String internalNotes) {
        this.internalNotes = internalNotes;
    }

    public String getExternalNotes() {
        return externalNotes;
    }

    public void setExternalNotes(String externalNotes) {
        this.externalNotes = externalNotes;
    }

}
