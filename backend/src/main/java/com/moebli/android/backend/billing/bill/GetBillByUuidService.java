package com.moebli.android.backend.billing.bill;

import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class GetBillByUuidService {
    private static String C = GetBillByUuidService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();

    private static HttpClientCallback getBillByUuidCallback;

    public void getBillByUuid(HttpClientCallback callback, String uuid) {
        getBillByUuidCallback = callback;
        GetBillByUuidRequestDTO requestDTO = new GetBillByUuidRequestDTO();
        requestDTO.setBillUuid(uuid);
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                        "/api/getBillByUuid",
                requestDTO, new GetBillByUuidBackendCallback(), GetBillByUuidResponseDTO.class);
    }

    public class GetBillByUuidBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if(o instanceof Throwable) {
                getBillByUuidCallback.handleResponse(o);
                return;
            }
            GetBillByUuidResponseDTO response = (GetBillByUuidResponseDTO) o;
            getBillByUuidCallback.handleResponse(response);
        }
    }

}
