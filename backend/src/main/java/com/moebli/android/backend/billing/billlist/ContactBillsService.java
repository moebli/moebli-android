package com.moebli.android.backend.billing.billlist;

import com.moebli.android.backend.billing.bill.BillDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class ContactBillsService {
    private static String C = ContactBillsService.class.getName();
    private static ContactBillsService mContactBillsService;
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static HttpClientCallback mGetContactBillsCallback;
    private static Map<String, List<BillDTO>> mBillListMap = new HashMap<>();

    public void getContactBills(HttpClientCallback callback, String contactAccountUuid) {
        if (mBillListMap.get(contactAccountUuid) != null) {
            GetContactBillsResponseDTO dto = new GetContactBillsResponseDTO();
            dto.setBillList(mBillListMap.get(contactAccountUuid));
            callback.handleResponse(dto);
            return;
        }
        mGetContactBillsCallback = callback;
        GetContactBillsRequestDTO requestDTO = new GetContactBillsRequestDTO();
        requestDTO.setContactAccountUuid(contactAccountUuid);
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                        "/api/getContactBills",
                requestDTO, new GetContactBillsBackendCallback(), GetContactBillsResponseDTO.class);
    }

    public class GetContactBillsBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                mGetContactBillsCallback.handleResponse(o);
                return;
            }
            GetContactBillsResponseDTO responseDTO = (GetContactBillsResponseDTO) o;
            mBillListMap.put(responseDTO.getContactAccountUuid(), responseDTO.getBillList());
            mGetContactBillsCallback.handleResponse(o);
        }
    }

    public void addNewBills(List<BillDTO> newBillDTOs) {
        for (BillDTO newBillDTO : newBillDTOs) {
            List<BillDTO> billList = mBillListMap.get(newBillDTO.getContactAccountUuid());
            if (billList == null)
                continue;

            boolean inserted = false;
            for (int i = 0; i < billList.size(); i++) {
                if (billList.get(i).getMonthYearCode().compareTo(newBillDTO.getMonthYearCode()) <=
                        0) {
                    billList.add(i, newBillDTO);
                    inserted = true;
                    break;
                }
            }
            if (!inserted) {
                billList.add(newBillDTO);
            }
        }
    }

    public void updateBill(BillDTO billDTO) {
        List<BillDTO> billList = mBillListMap.get(billDTO.getContactAccountUuid());
        if (billList == null)
            return;

        for (int i = 0; i < billList.size(); i++) {
            if (billList.get(i).getUuid().equals(billDTO.getUuid())) {
                billList.set(i, billDTO);
                return;
            }
        }
    }

    public void deleteBill(String deletedBillUuid) {
        for (List<BillDTO> billList : mBillListMap.values()) {
            for (int i = 0; i < billList.size(); i++) {
                if (billList.get(i).getUuid().equals(deletedBillUuid)) {
                    billList.remove(i);
                    return;
                }
            }
        }
    }

    public void flushCache() {
        mBillListMap = new HashMap<>();
    }

    public static ContactBillsService getInstance() {
        if (mContactBillsService == null)
            mContactBillsService = new ContactBillsService();
        return mContactBillsService;
    }

    private ContactBillsService() {
    }
}
