package com.moebli.android.backend.session;

import com.moebli.android.backend.util.ErrorDTO;

import java.util.ArrayList;
import java.util.List;

public class GetSessionResponseDTO {

    private String sessionId;
    private String countryCode;
    private String locale;
    private boolean forceUpdate;
    private boolean recommendUpdate;
    private String message;
    private List<ErrorDTO> errorList = new ArrayList<ErrorDTO>();

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public boolean isForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public boolean isRecommendUpdate() {
        return recommendUpdate;
    }

    public void setRecommendUpdate(boolean recommendUpdate) {
        this.recommendUpdate = recommendUpdate;
    }

    public List<ErrorDTO> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<ErrorDTO> errorList) {
        this.errorList = errorList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
