package com.moebli.android.backend.billing.billlist;

import com.moebli.android.backend.billing.bill.BillDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class MonthlyBillsService {
    private static String C = MonthlyBillsService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static MonthlyBillsService mMonthlyBillsService;
    private static HttpClientCallback mGetMonthlyBillsCallback;
    private static Map<String, List<BillDTO>> mBillListMap = new HashMap<>();

    public void getMonthlyBills(HttpClientCallback callback, String monthYearCode) {
        if (mBillListMap.get(monthYearCode) != null) {
            GetMonthlyBillsResponseDTO dto = new GetMonthlyBillsResponseDTO();
            dto.setBillList(mBillListMap.get(monthYearCode));
            callback.handleResponse(dto);
            return;
        }
        mGetMonthlyBillsCallback = callback;
        GetMonthlyBillsRequestDTO requestDTO = new GetMonthlyBillsRequestDTO();
        requestDTO.setMonthYearCode(monthYearCode);
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                        "/api/getMonthlyBills",
                requestDTO, new GetMonthlyBillsBackendCallback(), GetMonthlyBillsResponseDTO.class);
    }

    public class GetMonthlyBillsBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if(o instanceof Throwable) {
                mGetMonthlyBillsCallback.handleResponse(o);
                return;
            }
            GetMonthlyBillsResponseDTO responseDTO = (GetMonthlyBillsResponseDTO) o;
            mBillListMap.put(responseDTO.getMonthYearCode(), responseDTO.getBillList());
            mGetMonthlyBillsCallback.handleResponse(o);
        }
    }

    public void addNewBills(List<BillDTO> newBillDTOs) {
        for (BillDTO newBillDTO : newBillDTOs) {
            List<BillDTO> billList = mBillListMap.get(newBillDTO.getMonthYearCode());
            if (billList == null)
                continue;

            boolean inserted = false;
            for (int i = 0; i < billList.size(); i++) {
                if (billList.get(i).getItemName().compareTo(newBillDTO.getItemName()) >= 0) {
                    billList.add(i, newBillDTO);
                    inserted = true;
                    break;
                }
            }
            if (!inserted) {
                billList.add(newBillDTO);
            }
        }
    }

    public void updateBill(BillDTO updatedBill) {
        // monthYearCode may have changed; find original bill first and delete
        for (String monthYearCode : mBillListMap.keySet()) {
            List<BillDTO> billList = mBillListMap.get(monthYearCode);
            boolean deleted = false;
            for (int i = 0; i < billList.size(); i++) {
                if (billList.get(i).getUuid().equals(updatedBill.getUuid())) {
                    billList.remove(i);
                    deleted = true;
                    break;
                }
            }
            if (deleted)
                break;
        }

        // now insert bill
        List<BillDTO> billList = new ArrayList<>();
        billList.add(updatedBill);
        addNewBills(billList);
    }

    public void deleteBill(String deletedBillUuid) {
        for (List<BillDTO> billList : mBillListMap.values()) {
            for (int i = 0; i < billList.size(); i++) {
                if (billList.get(i).getUuid().equals(deletedBillUuid)) {
                    billList.remove(i);
                    return;
                }
            }
        }
    }

    public void flushCache() {
        mBillListMap = new HashMap<>();
    }

    public static MonthlyBillsService getInstance() {
        if (mMonthlyBillsService == null)
            mMonthlyBillsService = new MonthlyBillsService();
        return mMonthlyBillsService;
    }

    private MonthlyBillsService() {
    }
}
