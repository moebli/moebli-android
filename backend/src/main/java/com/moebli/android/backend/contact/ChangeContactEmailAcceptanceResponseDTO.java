package com.moebli.android.backend.contact;

import com.moebli.android.backend.util.ErrorDTO;

import java.util.ArrayList;
import java.util.List;

public class ChangeContactEmailAcceptanceResponseDTO {
    
    private ContactDTO changedContact;
        
    private List<ErrorDTO> errorList = new ArrayList<ErrorDTO>();

    public ContactDTO getChangedContact() {
        return changedContact;
    }

    public void setChangedContact(ContactDTO changedContact) {
        this.changedContact = changedContact;
    }

    public List<ErrorDTO> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<ErrorDTO> errorList) {
        this.errorList = errorList;
    }
}
