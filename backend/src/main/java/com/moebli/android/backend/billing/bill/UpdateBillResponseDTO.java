package com.moebli.android.backend.billing.bill;

import com.moebli.android.backend.billing.balance.ContactBalanceDTO;
import com.moebli.android.backend.billing.balance.ItemBalanceDTO;
import com.moebli.android.backend.billing.balance.MonthlyBalanceDTO;
import com.moebli.android.backend.item.ItemDTO;
import com.moebli.android.backend.util.ErrorDTO;

import java.util.ArrayList;
import java.util.List;

public class UpdateBillResponseDTO {

    private BillDTO bill;

    private ItemDTO item;

    private String deletedItemUuid;

    private String deletedMonthYearCode;

    private List<MonthlyBalanceDTO> monthlyBalanceList = new ArrayList<MonthlyBalanceDTO>();

    private List<ContactBalanceDTO> contactBalanceList = new ArrayList<ContactBalanceDTO>();

    private List<ItemBalanceDTO> itemBalanceList = new ArrayList<ItemBalanceDTO>();

    private String success;

    private List<ErrorDTO> errorList = new ArrayList<ErrorDTO>();

    public BillDTO getBill() {
        return bill;
    }

    public void setBill(BillDTO bill) {
        this.bill = bill;
    }

    public List<MonthlyBalanceDTO> getMonthlyBalanceList() {
        return monthlyBalanceList;
    }

    public void setMonthlyBalanceList(List<MonthlyBalanceDTO> monthlyBalanceList) {
        this.monthlyBalanceList = monthlyBalanceList;
    }

    public List<ContactBalanceDTO> getContactBalanceList() {
        return contactBalanceList;
    }

    public void setContactBalanceList(List<ContactBalanceDTO> contactBalanceList) {
        this.contactBalanceList = contactBalanceList;
    }

    public List<ItemBalanceDTO> getItemBalanceList() {
        return itemBalanceList;
    }

    public void setItemBalanceList(List<ItemBalanceDTO> itemBalanceList) {
        this.itemBalanceList = itemBalanceList;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<ErrorDTO> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<ErrorDTO> errorList) {
        this.errorList = errorList;
    }

    public StringBuilder toStringBuilder() {
        StringBuilder buf = new StringBuilder(100);/*
        buf.append("DeleteBillResponseDTO { ");
        buf.append("billList:").append(billList);
        buf.append("; newContactList:").append(newContactList);
        buf.append("; newItem:").append(newItem == null ? "null" : newItem);
        buf.append("; errors:").append(errorList);
        buf.append("; blockedConnList:").append(blockedContactList);
        buf.append(" }");*/
        return buf;
    }

    @Override
    public String toString() {
        return toStringBuilder().toString();
    }

    public String getDeletedItemUuid() {
        return deletedItemUuid;
    }

    public void setDeletedItemUuid(String deletedItemUuid) {
        this.deletedItemUuid = deletedItemUuid;
    }

    public String getDeletedMonthYearCode() {
        return deletedMonthYearCode;
    }

    public void setDeletedMonthYearCode(String deletedMonthYearCode) {
        this.deletedMonthYearCode = deletedMonthYearCode;
    }

    public ItemDTO getItem() {
        return item;
    }

    public void setItem(ItemDTO item) {
        this.item = item;
    }
}
