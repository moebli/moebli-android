package com.moebli.android.backend.billing.balance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.moebli.android.backend.contact.ContactDTO;

public class ContactBalanceDTO extends ContactDTO implements Serializable {

    private List<BalanceDTO> balanceList = new ArrayList<BalanceDTO>();



    public List<BalanceDTO> getBalanceList() {
        return balanceList;
    }

    public void setBalanceList(List<BalanceDTO> balanceList) {
        this.balanceList = balanceList;
    }

}
