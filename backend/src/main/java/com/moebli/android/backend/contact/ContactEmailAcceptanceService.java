package com.moebli.android.backend.contact;

import com.moebli.android.backend.billing.balance.ContactBalanceService;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class ContactEmailAcceptanceService {
    private static String C = ContactEmailAcceptanceService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static ContactEmailAcceptanceService mContactEmailAcceptanceService;
    private static HttpClientCallback mChangeContactEmailAcceptanceCallback;

    public void changeContactEmailAcceptance(HttpClientCallback callback, String contactUuid, String
            emailAcceptance) {
        ChangeContactEmailAcceptanceRequestDTO requestDTO = new ChangeContactEmailAcceptanceRequestDTO();
        requestDTO.setUuid(contactUuid);
        requestDTO.setEmailAcceptance(emailAcceptance);
        mChangeContactEmailAcceptanceCallback = callback;
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                        "/api/changeContactEmailAcceptance",
                requestDTO, new ChangeContactEmailAcceptanceBackendCallback(), ChangeContactEmailAcceptanceResponseDTO
                        .class);
    }

    class ChangeContactEmailAcceptanceBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if(o instanceof Throwable) {
                mChangeContactEmailAcceptanceCallback.handleResponse(o);
                return;
            }
            ChangeContactEmailAcceptanceResponseDTO responseDTO = (ChangeContactEmailAcceptanceResponseDTO) o;
            ContactBalanceService.getInstance().updateContactEmailAcceptance(responseDTO.getChangedContact());
            mChangeContactEmailAcceptanceCallback.handleResponse(responseDTO);
        }
    }

    public static ContactEmailAcceptanceService getInstance() {
        if (mContactEmailAcceptanceService == null)
            mContactEmailAcceptanceService = new ContactEmailAcceptanceService();
        return mContactEmailAcceptanceService;
    }

    private ContactEmailAcceptanceService() {
    }
}
