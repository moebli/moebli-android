package com.moebli.android.backend.item;

import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import java.util.ArrayList;
import java.util.List;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class ItemService {
    private static String C = ItemService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static ItemService mItemService;
    private static HttpClientCallback getItemsCallback;
    private static List<ItemDTO> mItemList;

    public void getItems(HttpClientCallback callback) {
        if (mItemList != null) {
            GetItemsResponseDTO dto = new GetItemsResponseDTO();
            dto.setItemList(mItemList);
            callback.handleResponse(dto);
            return;
        }
        getItemsCallback = callback;
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL + "/api/getAccessibleItems",
                null, new GetItemsBackendCallback(), GetItemsResponseDTO.class);
    }

    public class GetItemsBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if(o instanceof Throwable) {
                getItemsCallback.handleResponse(o);
                return;
            }
            GetItemsResponseDTO response = (GetItemsResponseDTO) o;
            mItemList = response.getItemList();
            getItemsCallback.handleResponse(o);
        }
    }

    public List<String> getOwnedItemStrings() {
        List<String> itemStrings = new ArrayList<>();
        for (ItemDTO item : mItemList)
            if (item.isOwner())
                itemStrings.add(item.getItemName());
        return itemStrings;
    }

    public List<String> getAccessibleItemStrings(String contactAccountUuid) {
        List<String> itemStrings = new ArrayList<>();
        for (ItemDTO item : mItemList)
            if (item.isOwner() || item.getAccountUuid().equals(contactAccountUuid))
                itemStrings.add(item.getItemName());
        return itemStrings;
    }

    public String getItemUuid(String itemName) {
        if (mItemList == null || itemName == null)
            return null;
        itemName = itemName.trim();
        for (ItemDTO item : mItemList)
            if (item.getItemName().equalsIgnoreCase(itemName))
                return item.getUuid();
        return null;
    }

    public void addOrUpdateItem(ItemDTO itemDTO) {
        if (mItemList == null || itemDTO == null)
            return;

        for (int i = 0; i < mItemList.size(); i++) {
            if (mItemList.get(i).getUuid().equals(itemDTO.getUuid())) {
                mItemList.set(i, itemDTO);
                return;
            }
        }

        boolean inserted = false;
        for (int i = 0; i < mItemList.size(); i++) {
            if (mItemList.get(i).getItemName().compareToIgnoreCase(itemDTO.getItemName()) > 0) {
                mItemList.add(i, itemDTO);
                inserted = true;
                break;
            }
        }
        if (!inserted) {
            mItemList.add(itemDTO);
        }
    }

    public void deleteItem(String itemUuid) {
        for (int i = 0; i < mItemList.size(); i++) {
            if (mItemList.get(i).getUuid().equals(itemUuid)) {
                mItemList.remove(i);
                return;
            }
        }
    }

    public void flushCache() {
        mItemList = null;
    }

    public static ItemService getInstance() {
        if (mItemService == null)
            mItemService = new ItemService();
        return mItemService;
    }

    private ItemService() {
    }

}
