package com.moebli.android.backend.user;


public class GetCurrentUserResponseDTO {

    private String email;
    private String mobileIddCode;
    private String mobileNumber;
    private String firstName;
    private String lastName;
    private String identityProvider;
    private boolean billEmailUnsubscribe;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileIddCode() {
        return mobileIddCode;
    }

    public void setMobileIddCode(String mobileIddCode) {
        this.mobileIddCode = mobileIddCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdentityProvider() {
        return identityProvider;
    }

    public void setIdentityProvider(String identityProvider) {
        this.identityProvider = identityProvider;
    }

    public boolean isBillEmailUnsubscribe() {
        return billEmailUnsubscribe;
    }

    public void setBillEmailUnsubscribe(boolean billEmailUnsubscribe) {
        this.billEmailUnsubscribe = billEmailUnsubscribe;
    }

}
