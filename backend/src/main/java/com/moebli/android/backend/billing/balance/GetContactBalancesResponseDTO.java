package com.moebli.android.backend.billing.balance;

import java.util.ArrayList;
import java.util.List;

public class GetContactBalancesResponseDTO {

    private List<ContactBalanceDTO> contactBalanceList = new ArrayList<ContactBalanceDTO>();

    public List<ContactBalanceDTO> getContactBalanceList() {
        return contactBalanceList;
    }

    public void setContactBalanceList(List<ContactBalanceDTO> contactBalanceList) {
        this.contactBalanceList = contactBalanceList;
    }

}
