package com.moebli.android.backend.user;

import com.moebli.android.backend.session.LoginResponseDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class SignUpService {

    private static String C = SignUpService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();

    public void signUp(HttpClientCallback callback, SignUpRequestDTO requestDTO) {
        httpClient.executeRequest(HttpClientUtil.Method.POST, BASE_URL + "/api/signup",
                requestDTO, callback, LoginResponseDTO.class);
    }
}
