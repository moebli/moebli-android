package com.moebli.android.backend.user;

import com.moebli.android.backend.util.BaseResponseDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class ChangeEmailService {
    private static String C = ChangeEmailService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static HttpClientCallback changeEmailCallback;

    public void changeEmail(HttpClientCallback callback, String email) {
        changeEmailCallback = callback;
        ChangeEmailRequestDTO requestDTO = new ChangeEmailRequestDTO();
        requestDTO.setEmail(email);
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL + "/api/changeEmail",
                requestDTO, new ChangeEmailBackendCallback(), BaseResponseDTO.class);
    }

    public class ChangeEmailBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            changeEmailCallback.handleResponse(o);
        }
    }
}
