package com.moebli.android.backend.util;

import java.util.ArrayList;
import java.util.List;

public class BaseResponseDTO {

    private String success;

    private List<ErrorDTO> errorList = new ArrayList<ErrorDTO>();

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<ErrorDTO> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<ErrorDTO> errorList) {
        this.errorList = errorList;
    }
}
