package com.moebli.android.backend.contact;

public class ChangeContactEmailAcceptanceRequestDTO {

    private String uuid;

    private String emailAcceptance;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getEmailAcceptance() {
        return emailAcceptance;
    }

    public void setEmailAcceptance(String emailAcceptance) {
        this.emailAcceptance = emailAcceptance;
    }

}
