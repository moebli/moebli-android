package com.moebli.android.backend.enums;

public enum DateFormat {
    DDdMMdYY("dd.MM.yyyy", "dd.mm.yyyy", "dd.MM"), //
    DDhMMhYY("dd-MM-yyyy", "dd-mm-yyyy", "dd-MM"), //
    DDsMMsYY("dd/MM/yyyy", "dd/mm/yyyy", "dd/MM"), //
    MMsDDsYY("MM/dd/yyyy", "mm/dd/yyyy", "MM/dd"), //
    YYdMMdDD("yyyy-MM-dd", "yyyy-mm-dd", "MM-dd"), ;

    private String javaDateFormat;
    private String javaScriptDateFormat;
    private String dateMonth;

    DateFormat(String javaDateFormat, String javaScriptDateFormat, String dateMonth) {
        this.javaDateFormat = javaDateFormat;
        this.javaScriptDateFormat = javaScriptDateFormat;
        this.dateMonth = dateMonth;
    }

    public String getJavaDateFormat() {
        return javaDateFormat;
    }

    public String getJavaScriptDateFormat() {
        return javaScriptDateFormat;
    }

    public String getDateMonth() {
        return dateMonth;
    }

}