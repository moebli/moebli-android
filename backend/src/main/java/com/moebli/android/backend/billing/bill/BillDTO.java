package com.moebli.android.backend.billing.bill;

import java.io.Serializable;

public class BillDTO implements Serializable {

    private String uuid;

    private String recordType;

    private boolean owner;

    private String payTo;

    private String monthYearCode;

    private String contactAccountUuid;

    private String itemUuid;

    private String itemName;

    private String currencyCode;

    private String currencySymbol;

    private String amount;

    private String amountFormatted;

    private String provider;

    private String date;

    private String dateFormatted;

    private String billMessage;

    private String internalNotes;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public boolean isOwner() {
        return owner;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }

    public String getPayTo() {
        return payTo;
    }

    public void setPayTo(String payTo) {
        this.payTo = payTo;
    }

    public String getMonthYearCode() {
        return monthYearCode;
    }

    public void setMonthYearCode(String monthYearCode) {
        this.monthYearCode = monthYearCode;
    }

    public String getContactAccountUuid() {
        return contactAccountUuid;
    }

    public void setContactAccountUuid(String contactAccountUuid) {
        this.contactAccountUuid = contactAccountUuid;
    }

    public String getItemUuid() {
        return itemUuid;
    }

    public void setItemUuid(String itemUuid) {
        this.itemUuid = itemUuid;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmountFormatted() {
        return amountFormatted;
    }

    public void setAmountFormatted(String amountFormatted) {
        this.amountFormatted = amountFormatted;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateFormatted() {
        return dateFormatted;
    }

    public void setDateFormatted(String dateFormatted) {
        this.dateFormatted = dateFormatted;
    }

    public String getBillMessage() {
        return billMessage;
    }

    public void setBillMessage(String billMessage) {
        this.billMessage = billMessage;
    }

    public String getInternalNotes() {
        return internalNotes;
    }

    public void setInternalNotes(String internalNotes) {
        this.internalNotes = internalNotes;
    }

}