package com.moebli.android.backend.billing.bill;

import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class CreateBillService {
    private static String C = CreateBillService.class.getName();
    private static CreateBillService mCreateBillService;
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static HttpClientCallback createBillCallback;

    public enum RecordTypeEnum { bill, payment }

    public void createBill(HttpClientCallback callback, CreateBillRequestDTO request) {
        createBillCallback = callback;
        httpClient.executeRequest(HttpClientUtil.Method.POST, BASE_URL + "/api/createBill",
                request, new CreateBillBackendCallback(), CreateBillResponseDTO.class);
    }

    public class CreateBillBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                createBillCallback.handleResponse(o);
                return;
            }
            CreateBillResponseDTO response = (CreateBillResponseDTO) o;
            createBillCallback.handleResponse(response);
        }
    }

    private CreateBillService() {}

    public static CreateBillService getInstance() {
        if (mCreateBillService == null)
            mCreateBillService = new CreateBillService();
        return mCreateBillService;
    }
}
