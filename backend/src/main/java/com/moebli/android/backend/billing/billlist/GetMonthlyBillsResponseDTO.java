package com.moebli.android.backend.billing.billlist;

import com.moebli.android.backend.billing.bill.BillDTO;

import java.util.List;

public class GetMonthlyBillsResponseDTO {

    private String monthYearCode;

    private List<BillDTO> billList;

    public String getMonthYearCode() {
        return monthYearCode;
    }

    public void setMonthYearCode(String monthYearCode) {
        this.monthYearCode = monthYearCode;
    }

    public List<BillDTO> getBillList() {
        return billList;
    }

    public void setBillList(List<BillDTO> billList) {
        this.billList = billList;
    }
}
