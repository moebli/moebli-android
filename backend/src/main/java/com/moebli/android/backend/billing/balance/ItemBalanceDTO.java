package com.moebli.android.backend.billing.balance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ItemBalanceDTO implements Serializable {

    private String uuid; //ItemUuid.

    private String itemName;

    private List<BalanceDTO> balanceList = new ArrayList<BalanceDTO>();

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public List<BalanceDTO> getBalanceList() {
        return balanceList;
    }

    public void setBalanceList(List<BalanceDTO> balanceList) {
        this.balanceList = balanceList;
    }

}
