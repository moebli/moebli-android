package com.moebli.android.backend.user;

import com.moebli.android.backend.util.BaseResponseDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class ChangeAccountNameService {
    private static String C = ChangeAccountNameService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static HttpClientCallback changeAccountNameCallback;

    public void changeAccountName(HttpClientCallback callback, String accountName) {
        changeAccountNameCallback = callback;
        ChangeAccountNameRequestDTO requestDTO = new ChangeAccountNameRequestDTO();
        requestDTO.setAccountName(accountName);
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                        "/api/changeAccountName",
                requestDTO, new ChangeAccountNameBackendCallback(), BaseResponseDTO.class);
    }

    public class ChangeAccountNameBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            changeAccountNameCallback.handleResponse(o);
        }
    }
}
