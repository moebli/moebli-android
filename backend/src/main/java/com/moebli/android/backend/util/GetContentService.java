package com.moebli.android.backend.util;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class GetContentService {
    private static String C = GetContentService.class.getName();
    private static HttpClientUtil httpClient = new HttpClientUtil();
    private static GetContentService mGetContentService;
    private static HttpClientCallback mGetContentCallback;

    public void getContent(HttpClientCallback callback, String code, String locale) {
        mGetContentCallback = callback;
        GetContentRequestDTO requestDTO = new GetContentRequestDTO();
        requestDTO.setCode(code);
        requestDTO.setLocale(locale);
        httpClient.executeRequest(HttpClientUtil.Method.GET, BASE_URL +
                        "/api/getContent",
                requestDTO, new GetContentBackendCallback(), GetContentResponseDTO.class);
    }

    public class GetContentBackendCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            mGetContentCallback.handleResponse(o);
        }
    }

    public static GetContentService getInstance() {
        if (mGetContentService == null)
            mGetContentService = new GetContentService();
        return mGetContentService;
    }

    private GetContentService() {
    }
}
