package com.moebli.android.backend.user;

import com.moebli.android.backend.session.LoginResponseDTO;
import com.moebli.android.backend.util.BaseResponseDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.HttpClientUtil;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class DoPasswordResetService {

    private static String C = DoPasswordResetService.class.getName();
    private static DoPasswordResetService mDoPasswordResetService;
    private static HttpClientUtil httpClient = new HttpClientUtil();

    public void doPasswordReset(HttpClientCallback callback, String phoneOrEmail, String
            resetCode, String newPassword) {
        DoPasswordResetRequestDTO requestDTO = new DoPasswordResetRequestDTO();
        requestDTO.setPhoneOrEmail(phoneOrEmail);
        requestDTO.setResetCode(resetCode);
        requestDTO.setNewPassword(newPassword);
        httpClient.executeRequest(HttpClientUtil.Method.POST, BASE_URL +
                        "/api/doPasswordReset",
                requestDTO, callback, LoginResponseDTO.class);
    }

    public static DoPasswordResetService getInstance() {
        if (mDoPasswordResetService == null)
            mDoPasswordResetService = new DoPasswordResetService();
        return mDoPasswordResetService;
    }

    private DoPasswordResetService() {
    }
}
