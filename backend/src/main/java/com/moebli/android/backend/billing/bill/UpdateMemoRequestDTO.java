package com.moebli.android.backend.billing.bill;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@SuppressWarnings("serial")
public class UpdateMemoRequestDTO implements Serializable {

    private String billUuid;

    private String internalNotes;

    public String getBillUuid() {
        return billUuid;
    }

    public void setBillUuid(String billUuid) {
        if (StringUtils.isNotBlank(billUuid))
            this.billUuid = billUuid;
    }

    public String getInternalNotes() {
        return internalNotes;
    }

    public void setInternalNotes(String internalNotes) {
        this.internalNotes = internalNotes;
    }
}
