package com.moebli.android.frontend.bill;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.moebli.android.backend.billing.bill.CreateBillResponseDTO;
import com.moebli.android.frontend.R;


public class CreateBillActivity extends AppCompatActivity implements CreateBillFormFragment.CreateBillListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_with_back_activity);

        // toolbar as actionbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.action_create_bill));

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container_view);

        if (fragment == null) {
            fragment = new CreateBillFormFragment();
            fm.beginTransaction().add(R.id.fragment_container_view, fragment)
                    .commit();
        }
    }

    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // http://stackoverflow.com/questions/10108774/how-to-implement-the-android-actionbar-back-button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }
*/


    @Override
    public void onCreateBillSuccess(CreateBillResponseDTO responseDTO) {
        CreateBillSuccessFragment successFragment = new CreateBillSuccessFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(CreateBillSuccessFragment.CREATE_BILL_RESPONSE_DTO, responseDTO);
        successFragment.setArguments(bundle);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.fragment_container_view, successFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
