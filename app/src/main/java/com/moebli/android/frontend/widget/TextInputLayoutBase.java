package com.moebli.android.frontend.widget;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;

public class TextInputLayoutBase extends TextInputLayout {

    public TextInputLayoutBase(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextInputLayoutBase(Context context) {
        super(context);
        init();
    }

    public void init() {
        /*
        // text
        FontManager fontManager = FontManager.getInstance(getContext().getAssets());
        Typeface tf = fontManager.getFont("fonts/Lato-Regular.ttf");
        setTypeface(tf, 1);
        // setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        setTextColor(Color.parseColor("#2c3e50"));
        setHintTextColor(Color.parseColor("#d0d0d0"));

        // layout
        setCompoundDrawablePadding(SizeConverter.dpToPx(getResources(), 10));
        int padding = SizeConverter.dpToPx(getResources(), 10);
        setPadding(padding, padding, padding, padding);
        setBackgroundResource(R.drawable.border_edit_text);
        */
    }
}
