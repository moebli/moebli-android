package com.moebli.android.frontend.bill;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.moebli.android.backend.billing.balance.ContactBalanceService;
import com.moebli.android.backend.billing.balance.ItemBalanceService;
import com.moebli.android.backend.billing.balance.MonthlyBalanceService;
import com.moebli.android.backend.billing.billlist.ContactBillsService;
import com.moebli.android.backend.billing.billlist.ItemBillsService;
import com.moebli.android.backend.billing.billlist.MonthlyBillsService;
import com.moebli.android.backend.enums.SharedPrefs;
import com.moebli.android.backend.item.ItemService;
import com.moebli.android.backend.session.LogoutService;
import com.moebli.android.backend.session.SessionService;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.contact.ContactListActivity;
import com.moebli.android.frontend.gcm.GCMRegistrationService;
import com.moebli.android.frontend.user.LoginActivity;
import com.moebli.android.frontend.user.AccountSettingsActivity;

public class ContactBalancesActivity extends AppCompatActivity {

    // ui elements
    private Toolbar mToolbar;

    // class variables
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.contact_balance_list_activity);

        // toolbar as actionbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.create_bill_fab);
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent createBillIntent = new Intent(getApplicationContext(), CreateBillActivity.class);
                startActivity(createBillIntent);
            }
        });

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.contact_balance_list_container);

        if (fragment == null) {
            fragment = new ContactBalancesFragment();
            fm.beginTransaction().add(R.id.contact_balance_list_container, fragment)
                    .commit();
        }

        initNavDrawer2();

        // register for google cloud messaging
        mRegistrationBroadcastReceiver = GCMRegistrationService.getInstance().registerForGCM(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // not null only on first login
        if (mRegistrationBroadcastReceiver != null)
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(SharedPrefs.REGISTRATION_COMPLETE.name()));
    }

    @Override
    protected void onPause() {
        super.onPause();
        // not null only on first login
        if (mRegistrationBroadcastReceiver != null)
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    private void initNavDrawer2() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
        final DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        NavigationView mNavigationView = (NavigationView) findViewById(R.id.drawer_nav_view);

        mNavigationView.setNavigationItemSelectedListener(new NavigationView
                .OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                Intent intent = null;
                switch (menuItem.getItemId()) {
                    case R.id.account_settings_nav:
                        intent = new Intent(getApplicationContext(), AccountSettingsActivity.class);
                        break;
                    /*
                    case R.id.change_email_nav:
                        intent = new Intent(getApplicationContext(), ChangeEmailActivity.class);
                        break;
                    case R.id.change_name_nav:
                        intent = new Intent(getApplicationContext(), ChangeNameActivity.class);
                        break;
                    case R.id.change_password_nav:
                        intent = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                        break;
                    case R.id.change_account_name_nav:
                        intent = new Intent(getApplicationContext(), ChangeAccountNameActivity
                                .class);
                        break;
                    case R.id.notification_settings_nav:
                        intent = new Intent(getApplicationContext(),
                                NotificationSettingActivity.class);
                        break;
                    */
                    case R.id.contact_settings_nav:
                        intent = new Intent(getApplicationContext(), ContactListActivity.class);
                        break;
                    case R.id.bills_by_month_nav:
                        intent = new Intent(getApplicationContext(), MonthlyBalancesActivity.class);
                        break;
                    case R.id.bills_by_item_nav:
                        intent = new Intent(getApplicationContext(), ItemBalancesActivity.class);
                        break;
                    case R.id.logout_nav:
                        logout();
                        return true;
                }

                startActivity(intent);
                return true;
            }
        });
    }

    private void logout() {
        LogoutService.getInstance().logout(new HttpClientCallback() {
            @Override
            public void handleResponse(Object o) {
            }
        });

        SessionService.getInstance().removeSession(this);
        ContactBalanceService.getInstance().flushCache();
        ItemBalanceService.getInstance().flushCache();
        MonthlyBalanceService.getInstance().flushCache();
        ContactBillsService.getInstance().flushCache();
        ItemBillsService.getInstance().flushCache();
        MonthlyBillsService.getInstance().flushCache();
        ItemService.getInstance().flushCache();

        // http://stackoverflow.com/a/9580057/1193781
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.putExtra(LoginActivity.LOGOUT, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // not using menu right now, but keep the code for possible future ise
        // getMenuInflater().inflate(R.menu.contact_balance_list_page_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // not using menu right now, but keep the code for possible future ise

        //if (mDrawerToggle != null && mDrawerToggle.onOptionsItemSelected(item)) {
        //    return true;
        //}

        /*
        int id = item.getItemId();

        if (id == R.id.action_new_tab) {
            Intent createBillIntent = new Intent(getApplicationContext(), CreateBillActivity.class);
            startActivity(createBillIntent);
            return true;
        }
        */

        return super.onOptionsItemSelected(item);
    }

}
