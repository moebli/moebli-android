package com.moebli.android.frontend.analytics;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.moebli.android.frontend.generic.Payminder;
import com.moebli.android.frontend.R;

import java.util.HashMap;

public class MTracker {
    // constants
    private static final String PROPERTY_ID = "UA-65103848-2";

    private HashMap<TrackerName, Tracker> mTrackers = new HashMap<>();
    private static MTracker mTracker;

    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    public void GA(Screen screen, Category category, Action action) {
        GA(screen, category, action, null, null);
    }

    public void GA(Screen screen, Category category, Action action, Long value) {
        GA(screen, category, action, null, value);
    }

    public void GA(Screen screen, Category category, Action action, String label) {
        GA(screen, category, action, label, null);
    }

    public void GA(Screen screen, Category category, Action action, String label, Long value) {
        Tracker tracker = getTracker(TrackerName.APP_TRACKER);
        if (action == Action.view)
            tracker.setScreenName(screen.name());
        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder();
        builder.setCategory(category.name());
        if (action != null)
            builder.setAction(action.name());
        if (label != null)
            builder.setLabel(label);
        if (value != null)
            builder.setValue(value);
        tracker.send(builder.build());
    }

    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(Payminder.getInstance());
            analytics.setLocalDispatchPeriod(30);
            Tracker tracker = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
                    // : analytics.newTracker(R.xml.ecommerce_tracker);
                    : analytics.newTracker(PROPERTY_ID);

            tracker.enableExceptionReporting(true);
            // tracker.enableAdvertisingIdCollection(true);
            tracker.enableAutoActivityTracking(true);

            mTrackers.put(trackerId, tracker);
        }
        return mTrackers.get(trackerId);
    }

    public static MTracker getInstance() {
        if (mTracker == null)
            mTracker = new MTracker();
        return mTracker;
    }

    private MTracker() {}
}
