package com.moebli.android.frontend.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moebli.android.frontend.R;

public class AccountSettingsFragment extends Fragment {

    public AccountSettingsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.account_settings_fragment, container, false);
        TextView emailLink = (TextView) v.findViewById(R.id.update_email_link);
        TextView nameLink = (TextView) v.findViewById(R.id.update_name_link);
        TextView passwordLink = (TextView) v.findViewById(R.id.update_password_link);
        TextView accountNameLink = (TextView) v.findViewById(R.id.update_acocunt_name_link);
        TextView notifSettingsLink = (TextView) v.findViewById(R.id.notification_settings_link);

        emailLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changeEmailActivity = new Intent(getActivity(), ChangeEmailActivity.class);
                startActivity(changeEmailActivity);
            }
        });
        nameLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changeNameActivity = new Intent(getActivity(), ChangeNameActivity.class);
                startActivity(changeNameActivity);
            }
        });
        passwordLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changePasswordActivity = new Intent(getActivity(), ChangePasswordActivity.class);
                startActivity(changePasswordActivity);
            }
        });
        accountNameLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changeAccountNameActivity = new Intent(getActivity(), ChangeAccountNameActivity.class);
                startActivity(changeAccountNameActivity);
            }
        });
        notifSettingsLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent notifSettingsActivity = new Intent(getActivity(), NotificationSettingActivity.class);
                startActivity(notifSettingsActivity);
            }
        });
        return v;
    }

}
