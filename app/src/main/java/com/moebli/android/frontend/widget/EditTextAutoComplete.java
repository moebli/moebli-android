package com.moebli.android.frontend.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.util.FontManager;
import com.moebli.android.frontend.util.SizeConverter;

public class EditTextAutoComplete extends AutoCompleteTextView {

    public EditTextAutoComplete(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public EditTextAutoComplete(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextAutoComplete(Context context) {
        super(context);
        init();
    }

    private void init() {
        // text
        FontManager fontManager = FontManager.getInstance(getContext().getAssets());
        Typeface tf = fontManager.getFont("fonts/Lato-Regular.ttf");
        setTypeface(tf, 1);
        // EditText default text size seems to be 18sp
        // setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        setTextColor(getResources().getColor(R.color.m_form_input));
        setHintTextColor(getResources().getColor(R.color.m_form_hint));

        // layout
        setCompoundDrawablePadding(SizeConverter.dpToPx(getResources(), 10));
        setBackgroundResource(R.drawable.border_edit_text); // set padding after background
        int padding = SizeConverter.dpToPx(getResources(), 10);
        setPadding(padding, padding, padding, padding);

        // clear error when typed
        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void afterTextChanged(Editable edt) {
                setError(null);
            }
        });
    }
}
