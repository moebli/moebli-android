package com.moebli.android.frontend.generic;

import android.support.v4.app.Fragment;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.SingleFragmentWithBackActivity;
import com.moebli.android.frontend.user.AccountSettingsFragment;

public class AboutAppActivity extends SingleFragmentWithBackActivity {

    @Override
    protected Fragment createFragment() {
        return new AboutAppFragment();
    }

    @Override
    protected int getActivityTitle() {
        return R.string.action_link_about_pm;
    }
}
