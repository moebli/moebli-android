package com.moebli.android.frontend.bill;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.moebli.android.backend.billing.bill.BillDTO;
import com.moebli.android.backend.billing.bill.CreateBillResponseDTO;
import com.moebli.android.backend.util.JsonUtil;
import com.moebli.android.frontend.R;

import java.util.List;

public class CreateBillSuccessFragment extends Fragment {

    public static final String CREATE_BILL_RESPONSE_DTO = "create_bill_response_dto";

    private TextView mSuccessMessage;
    private TextView mBlockedMessage;

    public CreateBillSuccessFragment() { // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.create_bill_success_fragment, container, false);
        mSuccessMessage = (TextView) v.findViewById(R.id.success_text);
        mBlockedMessage = (TextView) v.findViewById(R.id.blocked_message);

        CreateBillResponseDTO responseDTO = (CreateBillResponseDTO) getArguments()
                .getSerializable(CREATE_BILL_RESPONSE_DTO);

        showMessage(responseDTO.getBillList(), responseDTO.getBlockedContactList()
                , responseDTO.getSuccess(), mSuccessMessage, mBlockedMessage, v.findViewById(R.id
                .bill_row), getResources(), new BillClickListener(responseDTO.getBillList()));

        Button addAnother = (Button) v.findViewById(R.id.button_add_another);
        addAnother.setOnClickListener(new AddAnotherClickListener());

        return v;
    }

    public static void showMessage(List<BillDTO> billDTOList, List<String>
            blockedList, String success, TextView successMessage, TextView blockedMessage, View
            billRow, Resources resources, View.OnClickListener billClickListener) {
        if (billDTOList.size() == 0) {
            billRow.setVisibility(View.GONE);
            successMessage.setVisibility(View.GONE);
        } else {
            billRow.setVisibility(View.VISIBLE);
            successMessage.setVisibility(View.VISIBLE);
        }
        if (blockedList.size() == 0) blockedMessage.setVisibility(View.GONE);
        else blockedMessage.setVisibility(View.VISIBLE);
        successMessage.setText(success);

        StringBuilder buf = new StringBuilder(resources.getString(R.string.info_bill_not_sent_to)
        ).append(" ");
        boolean first = true;
        for (String c : blockedList) {
            if (!first) buf.append(", ");
            buf.append(c);
            first = false;
        }
        blockedMessage.setText(buf.toString());

        if (billDTOList.size() == 0) return;
        final BillDTO billDTO = billDTOList.get(0);
        TextView billDateText = (TextView) billRow.findViewById(R.id.bill_date_text);
        billDateText.setText(billDTO.getDateFormatted());
        TextView billNameText = (TextView) billRow.findViewById(R.id.bill_name_text);
        billNameText.setText(billDTO.getItemName());
        TextView amountText = (TextView) billRow.findViewById(R.id.amount_text);
        amountText.setText(billDTO.getAmountFormatted());

        if (!billDTO.getPayTo().equals("me")) {
            amountText.setTextColor(resources.getColor(R.color.balance_negative));
        }

        billNameText.setOnClickListener(billClickListener);
    }

    private class BillClickListener implements View.OnClickListener {
        private BillDTO mBillDTO;
        BillClickListener(List<BillDTO> billDTOs) {
            if (billDTOs.size() > 0)
                mBillDTO = billDTOs.get(0);
        }
        @Override
        public void onClick(View view) {
            Intent updateBillIntent = new Intent(getActivity(), UpdateBillActivity.class);
            updateBillIntent.putExtra(UpdateBillActivity.BILL_DTO, JsonUtil.getObjectToJSON
                    (mBillDTO));
            // send a flag indicating bill update from success page
            updateBillIntent.putExtra(UpdateBillActivity.IS_CREATE_SUCCESS, true);
            startActivity(updateBillIntent);
        }
    }

    private class AddAnotherClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent createBillIntent = new Intent(getActivity(), CreateBillActivity.class);
            startActivity(createBillIntent);
        }
    }

}
