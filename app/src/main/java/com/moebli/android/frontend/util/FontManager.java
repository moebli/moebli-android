package com.moebli.android.frontend.util;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.util.Log;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

// http://stackoverflow.com/questions/9327053/using-custom-font-in-android-textview-using-xml
public class FontManager {
    private static FontManager instance;

    private AssetManager mgr;

    private Map<String, Typeface> fontsMap;

    private FontManager(AssetManager _mgr) {
        mgr = _mgr;
        fontsMap = new HashMap<>();
    }

    public static FontManager getInstance(AssetManager mgr) {
        if (instance == null) {
            instance = new FontManager(mgr);
        }
        return instance;
    }

    public Typeface getFont(String asset) {
        if (fontsMap.containsKey(asset))
            return fontsMap.get(asset);

        Typeface font = null;

        try {
            font = Typeface.createFromAsset(mgr, asset);
            fontsMap.put(asset, font);
        } catch (Exception e) {
            Log.w("font error", e);
        }

        if (font == null) {
            try {
                String fixedAsset = fixAssetFilename(asset);
                font = Typeface.createFromAsset(mgr, fixedAsset);
                fontsMap.put(asset, font);
                fontsMap.put(fixedAsset, font);
            } catch (Exception e) {

            }
        }

        return font;
    }

    private String fixAssetFilename(String asset) {
        // Empty font filename?
        // Just return it. We can't help.
        if (StringUtils.isEmpty(asset))
            return asset;

        // Make sure that the font ends in '.ttf' or '.ttc'
        if ((!asset.endsWith(".ttf")) && (!asset.endsWith(".ttc")))
            asset = String.format("%s.ttf", asset);

        return asset;
    }
}