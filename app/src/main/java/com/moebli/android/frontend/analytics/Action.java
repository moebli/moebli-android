package com.moebli.android.frontend.analytics;

public enum Action {
    view, confirm, submit, cancel, success, error, more_action
}
