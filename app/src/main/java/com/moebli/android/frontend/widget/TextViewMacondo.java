package com.moebli.android.frontend.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.moebli.android.frontend.util.FontManager;

public class TextViewMacondo extends TextView {

    public TextViewMacondo(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextViewMacondo(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewMacondo(Context context) {
        super(context);
        init();
    }

    private void init() {
        FontManager fontManager = FontManager.getInstance(getContext().getAssets());
        Typeface tf = fontManager.getFont("fonts/MacondoSwashCaps-Regular.ttf");
        setTypeface(tf, 1);
    }
}
