package com.moebli.android.frontend.user;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.moebli.android.backend.billing.balance.ContactBalanceService;
import com.moebli.android.backend.item.ItemService;
import com.moebli.android.backend.session.FacebookLoginService;
import com.moebli.android.backend.session.LoginResponseDTO;
import com.moebli.android.backend.session.SessionService;
import com.moebli.android.backend.user.LinkFBUserService;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.bill.ContactBalancesActivity;
import com.moebli.android.frontend.util.RemoteCallError;
import com.moebli.android.frontend.widget.EditTextBase;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;

public class FacebookLoginFragment extends Fragment {

    private static final String C = FacebookLoginFragment.class.getName();
    private static List<String> permissions = Arrays.asList("email", "public_profile");

    // class variables
    private boolean mRefreshCurrentAccessTokenCalled = false;

    // ui elements
    private View mDialogLayout;
    // private EditTextBase mPasswordEdit;

    // service classes
    private static SessionService sessionService = SessionService.getInstance();
    private static FacebookLoginService fbLoginService = new FacebookLoginService();
    private LinkFBUserService linkFBUserService = new LinkFBUserService();
    private static CallbackManager facebookCallbackManager;

    public FacebookLoginFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.fb_login_fragment, container, false);

        // mPasswordEdit = (EditTextBase) v.findViewById(R.id.password_edit);

        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        facebookCallbackManager = CallbackManager.Factory.create();
        final LoginManager loginManager = LoginManager.getInstance();
        loginManager.registerCallback(facebookCallbackManager, new MFacebookCallback<LoginResult>
                ());

        Button fbButton = (Button) v.findViewById(R.id.fb_login_button);
        fbButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginManager.logInWithReadPermissions(getActivity(), permissions);
                MTracker.getInstance().GA(Screen.fb_login, Category.fb_login, Action.submit);
            }
        });

        return v;
    }

    private class MFacebookCallback<T> implements FacebookCallback<LoginResult> {

        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.i(C, "login success");
            GraphRequest.newMeRequest(loginResult.getAccessToken(), new MGraphJSONObjectCallback
                    ()).executeAsync();
            MTracker.getInstance().GA(Screen.fb_login, Category.fb_login, Action
                    .success);
        }

        @Override
        public void onCancel() {
            Snackbar.make(getActivity().findViewById(android.R.id.content), getResources()
                    .getString(R.string.info_fb_login_cancelled), Snackbar.LENGTH_LONG).show();
            MTracker.getInstance().GA(Screen.fb_login, Category.fb_login, Action
                    .cancel);
        }

        @Override
        public void onError(FacebookException exception) {
            Log.e(C, "facebook callback error", exception);
            // error can happen if user went to fb.com and de-authorized a previous authorization.
            // clear current access token but prevent infinite loop in case of other errors.
            // prevent infinite loop
            if (!mRefreshCurrentAccessTokenCalled) {
                mRefreshCurrentAccessTokenCalled = true;
                AccessToken.refreshCurrentAccessTokenAsync();

                // refreshCurrentAccessToken is asynchronous, hence cant call loginManager
                // immediately. show a dialog to user, and upon click, call login manager
                showRetryLoginDialog();
                MTracker.getInstance().GA(Screen.fb_login, Category.fb_login, Action.error, "refreshCurrentAccessTokenAsync");
            }
            // http://stackoverflow.com/questions/29791618
            if (exception instanceof FacebookAuthorizationException) {
                if (AccessToken.getCurrentAccessToken() != null) {
                    LoginManager.getInstance().logOut();

                    LoginManager.getInstance().logInWithReadPermissions(getActivity(), permissions);
                    MTracker.getInstance().GA(Screen.fb_login, Category.fb_login, Action.error,
                            "logOut,logIn");
                }
            }
            MTracker.getInstance().GA(Screen.fb_login, Category.fb_login, Action.error, exception.getMessage());
        }
    }

    private void showRetryLoginDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), R.style
                .AlertDialog);

        alertDialogBuilder.setTitle("Continue to Facebook?"); // TODO resource

        // set dialog message
        alertDialogBuilder.setCancelable(true).setPositiveButton("Yes", new DialogInterface
                .OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                LoginManager loginManager = LoginManager.getInstance();
                loginManager.registerCallback(facebookCallbackManager, new
                        MFacebookCallback<LoginResult>());
                loginManager.logInWithReadPermissions(getActivity(), permissions);
                MTracker.getInstance().GA(Screen.fb_retry, Category.fb_retry, Action
                        .submit);
            }
        });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MTracker.getInstance().GA(Screen.fb_retry, Category.fb_retry, Action
                        .cancel);
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        MTracker.getInstance().GA(Screen.fb_retry, Category.fb_retry, Action.view);
        alertDialog.show();
    }

    // {
//     "id": "1435337943437821",
//         "email": "dev-05@moebli.com",
//         "first_name": "DevMoebli",
//         "gender": "male",
//         "last_name": "DevMoebli",
//         "link": "https://www.facebook.com/app_scoped_user_id/1435337943437821/",
//         "locale": "en_US",
//         "name": "DevMoebli DevMoebli",
//         "timezone": -7,
//         "updated_time": "2015-01-05T02:49:46+0000",
//         "verified": false
// }
    private class MGraphJSONObjectCallback implements GraphRequest.GraphJSONObjectCallback {
        @Override
        public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
            if (graphResponse.getError() != null) {
                MTracker.getInstance().GA(Screen.fb_me_request, Category.fb_me_request,
                        Action.error);
            } else {
                String jsonresult = String.valueOf(jsonObject);
                System.out.println("JSON Result" + jsonresult);
                fbLoginService.facebookLogin(new FacebookLoginCallback(), jsonObject);
                MTracker.getInstance().GA(Screen.fb_me_request, Category
                        .fb_me_request, Action.success);
            }
        }
    }

    public class FacebookLoginCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            LoginResponseDTO response = (LoginResponseDTO) o;
            if (response.getErrorList().size() > 0) {
                Toast.makeText(getActivity(), response.getErrorList().get(0).getMessage(), Toast
                        .LENGTH_LONG).show();
            } else {
                switch (response.getStatusCodeEnum()) {
                    case Success:
                        sessionService.postLoginSessionUpdate(getActivity(), null, response);
                        MTracker.getInstance().GA(Screen.fb_session, Category.fb_session, Action
                                .success);

                        Intent contactBalancesIntent = new Intent(getActivity(), ContactBalancesActivity.class);
                        startActivity(contactBalancesIntent);
                        break;
                    case EmailRequired:
                        showEmailRequiredDialog();
                        MTracker.getInstance().GA(Screen.fb_session, Category
                                .fb_session, Action.more_action, "email_required");
                        break;
                    case DisconnectedAccountExists:
                        showConnectAccountDialog();
                        MTracker.getInstance().GA(Screen.fb_session, Category
                                .fb_session, Action.more_action,
                                "disconnected_account_exists");
                        break;
                }
            }
        }
    }

    private void showEmailRequiredDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), R.style
                .AlertDialog);

        alertDialogBuilder.setTitle(getResources().getString(R.string.info_title_email_required));
        alertDialogBuilder.setMessage(getResources().getString(R.string.info_fb_email_req_ms));
        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.action_continue),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        LoginManager loginManager = LoginManager.getInstance();
                        loginManager.registerCallback(facebookCallbackManager, new
                                MFacebookCallback<LoginResult>());
                        loginManager.logInWithReadPermissions(getActivity(), permissions);
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private void showConnectAccountDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style
                .AlertDialog);

        builder.setTitle(getResources().getString(R.string.action_link_facebook_acct));
        builder.setMessage(getResources().getString(R.string.info_fb_link_account));
        builder.setCancelable(true);

        mDialogLayout = getActivity().getLayoutInflater().inflate(R.layout
                .fb_link_user_fragment, null);
        builder.setView(mDialogLayout);

        builder.setPositiveButton(getResources().getString(R.string.action_submit), new
                DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // dont do anything here, stuff will be done below where View.OnClickListener is
                // being set again. this is needed so that dialog remains open in case of an error.
                // http://stackoverflow.com/questions/6142308/android-dialog-keep-dialog-open-when-button-is-pressed
            }
        });
        builder.setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MTracker.getInstance().GA(Screen.fb_link_account, Category
                        .fb_link_account, Action.cancel);
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = builder.create();
        TextView textView = (TextView) dialog.findViewById(android.R.id.message);
        //textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        dialog.show();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener
                () {
            @Override
            public void onClick(View v) {
                MTracker.getInstance().GA(Screen.fb_link_account, Category
                        .fb_link_account, Action.submit);

                EditTextBase passwordEdit = (EditTextBase) mDialogLayout.findViewById(R.id
                        .password_edit);

                if (StringUtils.isBlank(passwordEdit.getText().toString())) {
                    String s = MessageFormat.format(getResources().getString(R.string
                            .error_required_field), getResources().getString(R.string
                            .label_password));
                    passwordEdit.setError(s);
                    MTracker.getInstance().GA(Screen.fb_link_account, Category.fb_link_account,
                            Action.error, "empty_password");
                    return;
                }
                linkFBUserService.linkFBUser(new LinkFBUserCallback(), passwordEdit.getText().toString());
            }
        });
    }

    private class LinkFBUserCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            LoginResponseDTO response = (LoginResponseDTO) o;
            if (response.getErrorList().size() > 0) {
                EditTextBase passwordEdit = (EditTextBase) mDialogLayout.findViewById(R.id
                        .password_edit);
                passwordEdit.setError(response.getErrorList().get(0).getMessage());
                MTracker.getInstance().GA(Screen.fb_link_account, Category.fb_link_account,
                        Action.error, response.getErrorList().get(0).getFieldName());
            }
            else{
                sessionService.postLoginSessionUpdate(getActivity(), null, response);
                MTracker.getInstance().GA(Screen.fb_link_account, Category.fb_link_account,
                        Action.success);
                Intent contactBalancesIntent = new Intent(getActivity(), ContactBalancesActivity.class);
                startActivity(contactBalancesIntent);
            }
        }
    }

    public CallbackManager getFacebookCallbackManager() {
        return facebookCallbackManager;
    }

}
