package com.moebli.android.frontend.user;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.moebli.android.backend.user.ChangePersonNameService;
import com.moebli.android.backend.user.GetCurrentUserService;
import com.moebli.android.backend.user.GetCurrentUserResponseDTO;
import com.moebli.android.backend.util.BaseResponseDTO;
import com.moebli.android.backend.util.ErrorDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.util.RemoteCallError;

import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.List;

public class ChangeNameFragment extends Fragment {

    private static ChangePersonNameService changeNameService = new ChangePersonNameService();
    private static GetCurrentUserService getCurrentUserService = new GetCurrentUserService();

    private EditText mFirstName;
    private Button mButton;
    private EditText mLastName;

    public ChangeNameFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_change_name, container, false);

        mFirstName = (EditText) v.findViewById(R.id.firstname);
        mLastName = (EditText) v.findViewById(R.id.lastname);
        mButton = (Button) v.findViewById(R.id.changeName);

        getCurrentUserService.getCurrentUser(new GetCurrentUserCallback());

        mButton.setOnClickListener(new ButtonClickListener());

        MTracker.getInstance().GA(Screen.update_name, Category.update_name, Action.view);
        return v;
    }

    private class ButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            MTracker.getInstance().GA(Screen.update_name, Category.update_name, Action.submit);

            String firstName = mFirstName.getText().toString();
            String lastName = mLastName.getText().toString();
            if (StringUtils.isBlank(firstName)||StringUtils.isBlank(lastName)) {
                if (StringUtils.isBlank(firstName)) {
                    String s = MessageFormat.format(getResources().getString(R.string
                            .error_required_field), getResources().getString(R.string
                            .label_first_name));
                    mFirstName.setError(s);
                    MTracker.getInstance().GA(Screen.update_name, Category.update_name, Action
                            .error, "empty_first_name");
                }
                if (StringUtils.isBlank(lastName)) {
                    String s = MessageFormat.format(getResources().getString(R.string
                            .error_required_field), getResources().getString(R.string
                            .label_last_name));
                    mLastName.setError(s);
                    MTracker.getInstance().GA(Screen.update_name, Category.update_name, Action
                            .error, "empty_last_name");
                }
                return;
            }

            changeNameService.changeName(new ChangeNameCallback(),firstName,lastName);
        }
    }
    public class ChangeNameCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            BaseResponseDTO responseDTO = (BaseResponseDTO) o;
            //mMessageContainer.setVisibility(View.VISIBLE);
            List<ErrorDTO> errors = responseDTO.getErrorList();
            if (errors.size() > 0) {
                for(ErrorDTO error: errors) {
                    if (error.getFieldName().equals("firstName")) {
                        mFirstName.setError(error.getMessage());
                    }
                    if (error.getFieldName().equals("lastName")) {
                        mLastName.setError(error.getMessage());
                    }
                    MTracker.getInstance().GA(Screen.update_name, Category.update_name, Action
                            .error, error.getFieldName());
                }
                return;
            }
            else
            {
                Snackbar.make(getActivity().findViewById(android.R.id.content), responseDTO.getSuccess(), Snackbar.LENGTH_LONG).show();
                MTracker.getInstance().GA(Screen.update_name, Category.update_name,
                        Action.success);
            }
        }
    }
    private class GetCurrentUserCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            GetCurrentUserResponseDTO responseDTO = (GetCurrentUserResponseDTO) o;
            mFirstName.setText(responseDTO.getFirstName());
            mLastName.setText(responseDTO.getLastName());
        }
    }

}
