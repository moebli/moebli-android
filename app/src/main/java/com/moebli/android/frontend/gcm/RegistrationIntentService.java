package com.moebli.android.frontend.gcm;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.moebli.android.backend.enums.Constants;
import com.moebli.android.backend.enums.SharedPrefs;
import com.moebli.android.backend.session.SetDeviceRegistrationService;
import com.moebli.android.frontend.R;

import java.io.IOException;

public class RegistrationIntentService extends IntentService {

    private static final String C = "RegIntentService";
    private static final String[] TOPICS = {"global"};

    public RegistrationIntentService() {
        super("RegistrationIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sharedPrefs = this.getSharedPreferences(Constants.PREF_FILE, Context
                .MODE_PRIVATE);

        try {
            // [START register_for_gcm]
            // Initially this call goes out to the network to retrieve the token, subsequent calls
            // are local.
            // R.string.gcm_defaultSenderId (the Sender ID) is typically derived from
            // google-services.json.
            // See https://developers.google.com/cloud-messaging/android/start for details on
            // this file.
            // [START get_token]
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            // [END get_token]
            Log.i(C, "GCM Registration Token: " + token);

            // send any registration to your app's servers.
            sendRegistrationToServer(token);

            // Subscribe to topic channels
            // subscribeToTopics(token);

            // You should store a boolean that indicates whether the generated token has been
            // sent to your server. If the boolean is false, send the token to your server,
            // otherwise your server should have already received the token.
            sharedPrefs.edit().putBoolean(SharedPrefs.SENT_TOKEN_TO_SERVER.name(), true)
                    .apply();
            // [END register_for_gcm]
        } catch (Exception e) {
            Log.d(C, "Failed to complete token refresh", e);
            // If an exception happens while fetching the new token or updating our registration
            // data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
            sharedPrefs.edit().putBoolean(SharedPrefs.SENT_TOKEN_TO_SERVER.name(), false)
                    .apply();
        }
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(SharedPrefs.REGISTRATION_COMPLETE.name());
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(String registrationToken) {
        SetDeviceRegistrationService.getInstance().setDeviceRegistration(getApplicationContext(),
                registrationToken, null);
    }

    private void subscribeToTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }

}
