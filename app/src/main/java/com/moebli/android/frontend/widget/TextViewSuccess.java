package com.moebli.android.frontend.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.util.SizeConverter;

public class TextViewSuccess extends TextViewBase {

    public TextViewSuccess(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextViewSuccess(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewSuccess(Context context) {
        super(context);
        init();
    }

    public void init() {
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        setTextColor(getResources().getColor(android.R.color.white));
        setBackgroundResource(R.drawable.success_bg);
        int padding = SizeConverter.dpToPx(getResources(), 10);
        setPadding(padding, padding, padding, padding);
    }
}
