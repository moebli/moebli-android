package com.moebli.android.frontend.bill;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.moebli.android.backend.billing.balance.BalanceDTO;
import com.moebli.android.backend.billing.balance.GetItemBalancesResponseDTO;
import com.moebli.android.backend.billing.balance.ItemBalanceDTO;
import com.moebli.android.backend.billing.balance.ItemBalanceService;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.util.RemoteCallError;

import java.util.List;

/*
 * https://developer.android.com/training/material/lists-cards.html
 * http://stackoverflow.com/questions/24471109/recyclerview-onclick
 */
public class ItemBalancesFragment extends Fragment {

    // ui elements
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public ItemBalancesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.object_list_fragment, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.object_list_recycler);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        ItemBalanceService.getInstance().getItemBalances(new GetItemBalancesCallback());
    }

    class GetItemBalancesCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            List<ItemBalanceDTO> itemBalanceDTOList = ((GetItemBalancesResponseDTO) o)
                    .getItemBalanceList();
            mAdapter = new ItemBalanceListAdapter(itemBalanceDTOList);
            mRecyclerView.setAdapter(mAdapter);

            MTracker.getInstance().GA(Screen.item_balances, Category.item_balances, Action
                    .view, 0l + itemBalanceDTOList.size());
        }
    }

    private List<ItemBalanceDTO> mItemBalanceDTOList;

    public class ItemBalanceListAdapter extends RecyclerView.Adapter<ItemBalanceViewHolder> {

        // Provide a suitable constructor (depends on the kind of dataset)
        public ItemBalanceListAdapter(List<ItemBalanceDTO> itemBalanceDTOList) {
            mItemBalanceDTOList = itemBalanceDTOList;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public ItemBalanceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.balance_row,
                    parent, false);
            // set the view's size, margins, paddings and layout parameters

            ItemBalanceViewHolder vh = new ItemBalanceViewHolder(v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ItemBalanceViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            ItemBalanceDTO itemBalanceDTO = mItemBalanceDTOList.get(position);
            holder.mNameText.setText(itemBalanceDTO.getItemName());

            List<BalanceDTO> balanceDTOs = itemBalanceDTO.getBalanceList();
            if (balanceDTOs.size() == 0)
                return;
            holder.mBalanceText.setText(ContactActivity.getBalanceHtml(balanceDTOs));
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mItemBalanceDTOList.size();
        }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ItemBalanceViewHolder extends RecyclerView.ViewHolder implements View
            .OnClickListener {
        public LinearLayout mBalanceRow;
        // each data item is just a string in this case
        public TextView mNameText;
        public TextView mBalanceText;

        public ItemBalanceViewHolder(View view) {
            super(view);
            mBalanceRow = (LinearLayout) view.findViewById(R.id.balance_row_layout);
            mNameText = (TextView) view.findViewById(R.id.name_text);
            mBalanceText = (TextView) view.findViewById(R.id.balance_text);

            // taking a simple approach to make the entire row clickable
            // other solutions exist at:
            // http://stackoverflow.com/questions/24471109/
            // http://stackoverflow.com/questions/24885223/
            mBalanceRow.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int itemPosition = getPosition();
            ItemBalanceDTO itemBalanceDTO = mItemBalanceDTOList.get(itemPosition);

            Intent itemBillsIntent = new Intent(getActivity(), ItemBillsActivity.class);
            itemBillsIntent.putExtra(ItemBillsActivity.ITEM_UUID, itemBalanceDTO.getUuid());
            startActivity(itemBillsIntent);
        }

    }

}
