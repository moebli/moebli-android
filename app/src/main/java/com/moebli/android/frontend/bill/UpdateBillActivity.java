package com.moebli.android.frontend.bill;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.moebli.android.backend.billing.bill.BillDTO;
import com.moebli.android.backend.billing.bill.DeleteBillResponseDTO;
import com.moebli.android.backend.billing.bill.UpdateBillResponseDTO;
import com.moebli.android.backend.billing.bill.UpdateMemoResponseDTO;
import com.moebli.android.backend.util.JsonUtil;
import com.moebli.android.frontend.R;


public class UpdateBillActivity extends AppCompatActivity {

    // constants
    public static final String BILL_DTO = "bill_dto";
    public static final String IS_CREATE_SUCCESS = "is_create_success";

    // class variables
    private BillDTO mBillDTO;
    private boolean mIsCreateSuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent updateBillIntent = getIntent();
        mBillDTO = JsonUtil.getJSONtoObject(updateBillIntent.getStringExtra(BILL_DTO), BillDTO
                .class);
        mIsCreateSuccess = updateBillIntent.getBooleanExtra(IS_CREATE_SUCCESS, false);

        setContentView(R.layout.single_fragment_with_back_activity);

        // toolbar as actionbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.label_title_bill_dtl));

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container_view);

        if (fragment == null) {
            fragment = new UpdateBillFormFragment();
            fm.beginTransaction().add(R.id.fragment_container_view, fragment).commit();
        }
    }

    public BillDTO getBillDTO() {
        return mBillDTO;
    }

    public boolean isCreateSuccess() {
        return mIsCreateSuccess;
    }
}
