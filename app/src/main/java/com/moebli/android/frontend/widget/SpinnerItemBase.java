package com.moebli.android.frontend.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.TextView;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.util.FontManager;
import com.moebli.android.frontend.util.SizeConverter;

/**
 * Created by sushant on 12/09/15.
 */
public class SpinnerItemBase extends TextView {
    public SpinnerItemBase(Context context) {
        super(context);
        init();
    }

    public SpinnerItemBase(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SpinnerItemBase(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SpinnerItemBase(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init() {
        // text
        FontManager fontManager = FontManager.getInstance(getContext().getAssets());
        Typeface tf = fontManager.getFont("fonts/Lato-Regular.ttf");
        setTypeface(tf, 1);
        // EditText default text size seems to be 18sp
        // setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        setTextColor(getResources().getColor(R.color.m_form_input));
        setHintTextColor(getResources().getColor(R.color.m_form_hint));

        // layout
        setCompoundDrawablePadding(SizeConverter.dpToPx(getResources(), 5));
       // setBackgroundResource(R.drawable.border_edit_text); // set border before padding
        int padding = SizeConverter.dpToPx(getResources(), 5);
        setPadding(padding, padding, padding, padding);

        // clear error when typed
        addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            public void afterTextChanged(Editable edt){
                setError(null);
            }
        });

    }
}
