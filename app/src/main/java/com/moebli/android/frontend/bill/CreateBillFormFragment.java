package com.moebli.android.frontend.bill;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.moebli.android.backend.billing.balance.ContactBalanceService;
import com.moebli.android.backend.billing.balance.ItemBalanceService;
import com.moebli.android.backend.billing.balance.MonthlyBalanceService;
import com.moebli.android.backend.billing.bill.CreateBillRequestDTO;
import com.moebli.android.backend.billing.bill.CreateBillResponseDTO;
import com.moebli.android.backend.billing.bill.CreateBillService;
import com.moebli.android.backend.billing.billlist.ContactBillsService;
import com.moebli.android.backend.billing.billlist.ItemBillsService;
import com.moebli.android.backend.billing.billlist.MonthlyBillsService;
import com.moebli.android.backend.enums.Currency;
import com.moebli.android.backend.item.ItemService;
import com.moebli.android.backend.session.SessionService;
import com.moebli.android.backend.util.ErrorDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.util.RemoteCallError;
import com.moebli.android.frontend.widget.TextViewBase;

import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;


public class CreateBillFormFragment extends Fragment {

    private static CreateBillFormFragment mCreateBillFormFragment;
    private CreateBillListener mCreateBillListener;

    private AutoCompleteTextView mItemEdit;
    private RadioGroup payToRadioGroup;
    private RadioButton payToMeRadio;
    private RadioButton payToConnRadio;
    private AutoCompleteTextView mContact0Edit;
    private EditText mAmount0Edit;
    private Spinner mCurrencySpinner;

    private Date billDate = new Date();
    private Currency mSelectedCurrency;

    public CreateBillFormFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCreateBillListener = (CreateBillListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement " +
                    "mCreateBillListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.create_bill_form_fragment, container, false);

        mItemEdit = (AutoCompleteTextView) v.findViewById(R.id.item_edit);
        payToRadioGroup = (RadioGroup) v.findViewById(R.id.pay_to_radio_group);
        payToMeRadio = (RadioButton) v.findViewById(R.id.pay_to_me_radio);
        payToConnRadio = (RadioButton) v.findViewById(R.id.pay_to_conn_radio);
        mContact0Edit = (AutoCompleteTextView) v.findViewById(R.id.contact_1_edit);
        mAmount0Edit = (EditText) v.findViewById(R.id.amount_1_edit);
        // billDateButton = (Button) v.findViewById(R.id.bill_date_button);
        // messageEdit = (EditText) v.findViewById(R.id.message_edit);
        Button createBillButton = (Button) v.findViewById(R.id.create_bill_button);

        // currency
        mCurrencySpinner = (Spinner) v.findViewById(R.id.currency);
        List<Currency> currencies = Currency.getCurrencyCodeEnumList();
        ArrayAdapter<Currency> currencyArrayAdapter = new CurrencySpinnerAdapter(getActivity(), R
                .layout.currency_row, currencies);
        mCurrencySpinner.setAdapter(currencyArrayAdapter);
        String sessionCurrency = SessionService.getInstance().getCurrencyCode(getActivity());
        for (int i = 0; i < currencies.size(); i++) {
            if (currencies.get(i).getCode().equals(sessionCurrency)) {
                mCurrencySpinner.setSelection(i);
                break;
            }
        }
        mCurrencySpinner.setOnItemSelectedListener(new CurrencyItemSelectedListener());

        createBillButton.setOnClickListener(new CreateBillButtonClickListener());

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mItemEdit.setText("");
        mContact0Edit.setText("");
        mAmount0Edit.setText("");

        ItemService.getInstance().getItems(new GetItemsCallback());
        List<String> contacts = ContactBalanceService.getInstance().getUnblockedContactStrings();
        ArrayAdapter<String> contactAdapter = new ArrayAdapter(getActivity(), android.R.layout
                .simple_dropdown_item_1line, contacts);
        mContact0Edit.setAdapter(contactAdapter);

        MTracker.getInstance().GA(Screen.create_bill, Category.create_bill, Action.view);
    }

    public class GetItemsCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            ArrayAdapter<String> itemAdapter = new ArrayAdapter(getActivity(), android.R.layout
                    .simple_dropdown_item_1line, ItemService.getInstance().getOwnedItemStrings());
            mItemEdit.setAdapter(itemAdapter);
        }
    }

    public static class CurrencySpinnerAdapter extends ArrayAdapter<Currency> {

        private List<Currency> mCurrencyList;

        public CurrencySpinnerAdapter(Context context, int resourceId, List<Currency> objects) {
            super(context, resourceId, objects);
            mCurrencyList = objects;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.currency_row,
                    parent, false);
            TextViewBase name = (TextViewBase) row.findViewById(R.id.currency_name_text);
            name.setText(mCurrencyList.get(position).getName());
            name.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            TextViewBase symbol = (TextViewBase) row.findViewById(R.id.currency_symbol_text);
            symbol.setText(mCurrencyList.get(position).getSymbol());
            symbol.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            return row;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.currency_symbol,
                    parent, false);
            TextViewBase symbol = (TextViewBase) row.findViewById(R.id.currency_symbol_text);
            symbol.setText(mCurrencyList.get(position).getSymbol());
            return row;
        }
    }

    /*
    private class PayToGroupOnCheckedChangeListener implements RadioGroup.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case R.id.pay_to_me_radio:
                    mContact0Edit.setHint("From");
                    mContact0Edit.setHintTextColor(getResources().getColor(R.color.m_form_hint));
                    textInputBase.setHint("From");

                    break;
                case R.id.pay_to_conn_radio:
                    mContact0Edit.setHint("To");
                    textInputBase.setHint("To");
                    mContact0Edit.setHintTextColor(getResources().getColor(R.color.m_form_hint));
                    break;
            }
        }
    }
    */

    private class CreateBillButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            MTracker.getInstance().GA(Screen.create_bill, Category.create_bill, Action.submit);

            String itemName = mItemEdit.getText().toString();
            String contact = mContact0Edit.getText().toString();
            String amount = mAmount0Edit.getText().toString();

            if (StringUtils.isBlank(itemName)) {
                mItemEdit.setError(MessageFormat.format(getResources().getString(R.string.error_required_field), getResources().getString(R.string.label_event)));
                MTracker.getInstance().GA(Screen.create_bill, Category.create_bill, Action.error,
                        "empty_item");
            }
            if (StringUtils.isBlank(contact)) {
                mContact0Edit.setError(MessageFormat.format(getResources().getString(R.string.error_required_field), getResources().getString(R.string.label_email_address)));
                MTracker.getInstance().GA(Screen.create_bill, Category.create_bill, Action.error,
                        "empty_email");
            }
            if (StringUtils.isBlank(amount)) {
                mAmount0Edit.setError(MessageFormat.format(getResources().getString(R.string.error_required_field), getResources().getString(R.string.label_amount)));
                MTracker.getInstance().GA(Screen.create_bill, Category.create_bill, Action.error,
                        "empty_amount");
            }
            if (StringUtils.isBlank(itemName) || StringUtils.isBlank(contact) ||
                    StringUtils.isBlank(amount)) {
                return;
            }

            CreateBillRequestDTO requestDTO = new CreateBillRequestDTO();
            requestDTO.setItemName(itemName);
            requestDTO.setItemUuid(ItemService.getInstance().getItemUuid(itemName));
            requestDTO.setRecordType(CreateBillService.RecordTypeEnum.bill.name());
            requestDTO.setContact0(extractEmail(contact));
            // requestDTO.setItemDate("");
            // requestDTO.setExternalNotes(messageEdit.getText().toString());
            int selectedPayTo = payToRadioGroup.getCheckedRadioButtonId();
            String getMoney = selectedPayTo == payToConnRadio.getId() ? "-" : "";
            requestDTO.setAmount0(getMoney + amount);
            requestDTO.setCurrencyCode(mSelectedCurrency.getCode());

            CreateBillService.getInstance().createBill(new CreateBillCallback(), requestDTO);
        }
    }

    private String extractEmail(String contactStr) {
        if (contactStr.contains("(")) {
            String[] parts = contactStr.split("\\(");
            parts = parts[1].split("\\)");
            return parts[0].trim();
        }
        return contactStr.trim();
    }

    public class CreateBillCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            CreateBillResponseDTO responseDTO = (CreateBillResponseDTO) o;
            if (responseDTO.getErrorList().size() > 0) {
                for (ErrorDTO error : responseDTO.getErrorList()) {
                    if ("itemName".equals(error.getFieldName()))
                        mItemEdit.setError(error.getMessage());
                    if ("contact0".equals(error.getFieldName()))
                        mContact0Edit.setError(error.getMessage());
                    if ("amount0".equals(error.getFieldName()))
                        mAmount0Edit.setError(error.getMessage());
                    MTracker.getInstance().GA(Screen.create_bill, Category.create_bill, Action.error,
                            error.getFieldName());
                }
            } else {
                // on success, notify parent activity to replace fragment
                mCreateBillListener.onCreateBillSuccess(responseDTO);
                updateCache(responseDTO);
                MTracker.getInstance().GA(Screen.create_bill, Category.create_bill, Action
                        .success, 0l + responseDTO.getBillList().size());
            }
        }
    }

    public static void updateCache(CreateBillResponseDTO responseDTO) {
        ContactBalanceService.getInstance().addNewContacts(responseDTO.getNewContactList());
        ItemService.getInstance().addOrUpdateItem(responseDTO.getNewItem());
        ContactBalanceService.getInstance().addOrUpdateContactBalances(responseDTO
                .getContactBalanceList());
        MonthlyBalanceService.getInstance().addOrUpdateMonthlyBalances(responseDTO
                .getMonthlyBalanceList());
        ItemBalanceService.getInstance().addOrUpdateItemBalances(responseDTO
                .getItemBalanceList());
        ContactBillsService.getInstance().addNewBills(responseDTO.getBillList());
        MonthlyBillsService.getInstance().addNewBills(responseDTO.getBillList());
        ItemBillsService.getInstance().addNewBills(responseDTO.getBillList());
    }

    private class CurrencyItemSelectedListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            mSelectedCurrency = (Currency) parent.getSelectedItem();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    }

    // Container Activity must implement this interface
    public interface CreateBillListener {
        public void onCreateBillSuccess(CreateBillResponseDTO responseDTO);
    }
}
