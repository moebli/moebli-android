package com.moebli.android.frontend.analytics;

public enum Category {
    splash_screen, update_recommend, update_force, remote_call, // initials
    login, fb_login, fb_retry, fb_me_request, fb_session, fb_link_account, // login
    sign_up, //
    init_password_reset, do_password_reset, // password help
    contact_bills, monthly_bills, item_bills, // bills
    contact_balances, monthly_balances, item_balances, // balances
    create_bill, create_contact_bill, update_bill, update_memo, delete_bill, // bill
    create_cash_payment, update_cash_payment, delete_cash_payment, // cash payment
    update_provider_payment, // provider payment
    update_email, update_name, update_account_name, update_password, //
    contact_settings, email_notification, //
    terms_and_privacy, about_app,
}
