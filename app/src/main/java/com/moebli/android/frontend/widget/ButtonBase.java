package com.moebli.android.frontend.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.Button;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.util.FontManager;

public class ButtonBase extends AppCompatButton {

    public ButtonBase(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ButtonBase(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ButtonBase(Context context) {
        super(context);
        init();
    }

    public void init() {
        // text
        FontManager fontManager = FontManager.getInstance(getContext().getAssets());
        Typeface tf = fontManager.getFont("fonts/Lato-Regular.ttf");
        setTypeface(tf, 1);
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        setTextColor(getResources().getColor(R.color.m_button_text));

        // note: removing background resource will render the button in material design style
        // material design style to be adopted for buttons in future; currently it is not working
        // well
        setBackgroundResource(R.drawable.btn_bg);
        setTransformationMethod(null); // disable capitalizing
    }
}
