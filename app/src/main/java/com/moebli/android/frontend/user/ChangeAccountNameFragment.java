package com.moebli.android.frontend.user;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.moebli.android.backend.user.ChangeAccountNameService;
import com.moebli.android.backend.user.GetAccountResponseDTO;
import com.moebli.android.backend.user.GetCurrentAccountService;
import com.moebli.android.backend.util.BaseResponseDTO;
import com.moebli.android.backend.util.ErrorDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.util.RemoteCallError;

import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.List;

public class ChangeAccountNameFragment extends Fragment {

    private static ChangeAccountNameService changeAccountNameService = new ChangeAccountNameService();
    private static GetCurrentAccountService getCurrentAccountService = new GetCurrentAccountService();

    private EditText mNameEdit;
    private Button mButton;

    public ChangeAccountNameFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_change_account_name, container, false);

        mNameEdit = (EditText) v.findViewById(R.id.accountName);
        mButton = (Button) v.findViewById(R.id.changeAccountName);

        getCurrentAccountService.getCurrentAccount(new GetCurrentAccountCallback());

        mButton.setOnClickListener(new ButtonClickListener());

        MTracker.getInstance().GA(Screen.update_account_name, Category.update_account_name,
                Action.view);

        return v;
    }

    private class ButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            MTracker.getInstance().GA(Screen.update_account_name, Category.update_account_name,
                    Action.submit);

            String nameEdit = mNameEdit.getText().toString();
            if (StringUtils.isBlank(nameEdit)) {
                String s = MessageFormat.format(getResources().getString(R.string
                        .error_required_field), getResources().getString(R.string.label_account_name));
                mNameEdit.setError(s);
                MTracker.getInstance().GA(Screen.update_account_name, Category.update_account_name, Action
                        .error, "empty_account_name");
                return;
            }

            changeAccountNameService.changeAccountName(new ChangeAccountNameCallback(),nameEdit);
        }
    }

    public class ChangeAccountNameCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            BaseResponseDTO responseDTO = (BaseResponseDTO) o;
            List<ErrorDTO> errors = responseDTO.getErrorList();
            if (errors.size() > 0) {
                for (ErrorDTO error : errors) {
                    mNameEdit.setError(error.getMessage());
                    MTracker.getInstance().GA(Screen.update_account_name, Category.update_account_name, Action
                            .error, error.getFieldName());
                }
                return;
            }
            else {
                Snackbar.make(getActivity().findViewById(android.R.id.content),responseDTO.getSuccess(),Snackbar.LENGTH_LONG).show();
                MTracker.getInstance().GA(Screen.update_account_name, Category.update_account_name, Action
                        .success);
            }
        }
    }

    private class GetCurrentAccountCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            GetAccountResponseDTO responseDTO = (GetAccountResponseDTO) o;
            mNameEdit.setText(responseDTO.getAccountName());
        }
    }
}
