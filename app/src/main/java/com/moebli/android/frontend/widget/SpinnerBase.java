package com.moebli.android.frontend.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Spinner;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.util.FontManager;
import com.moebli.android.frontend.util.SizeConverter;

/**
 * Created by sushant on 11/09/15.
 */
public class SpinnerBase extends Spinner {
    public SpinnerBase(Context context) {
        super(context);
        init();
    }

    public SpinnerBase(Context context, int mode) {
        super(context, mode);
        init();
    }

    public SpinnerBase(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SpinnerBase(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public SpinnerBase(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
        super(context, attrs, defStyleAttr, mode);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SpinnerBase(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes, int mode) {
        super(context, attrs, defStyleAttr, defStyleRes, mode);
        init();
    }

    public void init() {
        // text
        FontManager fontManager = FontManager.getInstance(getContext().getAssets());
        Typeface tf = fontManager.getFont("fonts/Lato-Regular.ttf");
        //setTypeface(tf, 1);
        // EditText default text size seems to be 18sp
        // setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        //setTextColor(getResources().getColor(R.color.m_form_input));
        //setHintTextColor(getResources().getColor(R.color.m_form_hint));

        // layout
        //setCompoundDrawablePadding(SizeConverter.dpToPx(getResources(), 10));
        setBackgroundResource(R.drawable.border_edit_text); // set border before padding
        int padding = SizeConverter.dpToPx(getResources(), 5);
        setPadding(padding, padding, padding, padding);
    }
}
