package com.moebli.android.frontend.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.backend.session.SessionService;
import com.moebli.android.frontend.R;

import java.net.ConnectException;
import java.net.UnknownHostException;

public class RemoteCallError {
    public static void show(Activity activity, Throwable o) {
        if (o instanceof ConnectException || o instanceof UnknownHostException) {
            showConnectionDialog(activity, o);
            return;
        }
        showExceptionDialog(activity, o);
    }

    private static void showConnectionDialog(final Activity activity, Throwable o) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity, R.style
                .AlertDialog);

        alertDialogBuilder.setTitle(activity.getResources().getString(R.string.label_title_no_conn));
        String s = activity.getResources().getString(R.string.info_no_conn_msg) + "\n\n";
        // TODO remove exception message post-beta
        s += o.getLocalizedMessage();
        alertDialogBuilder.setMessage(s);
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton(activity.getResources().getString(R.string
                .action_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private static void showExceptionDialog(final Activity activity, Throwable o) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity, R.style
                .AlertDialog);

        alertDialogBuilder.setTitle(activity.getResources().getString(R.string.label_title_server_error));
        String s = activity.getResources().getString(R.string.info_server_error_msg) + "\n\n";
        s += "Reference: " + SessionService.getInstance().getEcid() + "\n";
        s += o.getClass().getSimpleName();
        alertDialogBuilder.setMessage(s);
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton(activity.getResources().getString(R.string
                .action_quit), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                activity.finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        MTracker.getInstance().GA(Screen.error, Category.remote_call, Action.error, o.getClass()
                .getSimpleName());
        alertDialog.show();
    }

}
