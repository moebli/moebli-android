package com.moebli.android.frontend.util;

import android.content.res.Resources;

public class SizeConverter {

    public static int dpToPx(Resources r, int dp) {
        float scale = r.getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    public static int spToPx(Resources r, int sp) {
        float scale = r.getDisplayMetrics().scaledDensity;
        return (int) (sp * scale + 0.5f);
    }
}
