package com.moebli.android.frontend.generic;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moebli.android.backend.session.SessionService;
import com.moebli.android.backend.util.GetContentResponseDTO;
import com.moebli.android.backend.util.GetContentService;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.util.RemoteCallError;

import org.apache.commons.lang3.StringUtils;

public class TermsAndPrivacyFragment extends Fragment {

    private TextView mTermsTitle;
    private TextView mPrivacyTitle;
    private TextView mTermsText;
    private TextView mPrivacyText;

    public TermsAndPrivacyFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.g_terms_n_privacy_fragment, container, false);
        mTermsTitle = (TextView) v.findViewById(R.id.terms_title);
        mPrivacyTitle = (TextView) v.findViewById(R.id.privacy_title);
        mTermsText = (TextView) v.findViewById(R.id.terms_text);
        mPrivacyText = (TextView) v.findViewById(R.id.privacy_text);

        GetContentService.getInstance().getContent(new TermsCallback(), "terms", SessionService
                .getInstance().getLocale(getActivity()));

        MTracker.getInstance().GA(Screen.terms_and_privacy, Category.terms_and_privacy, Action.view);
        return v;
    }

    class TermsCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            GetContentResponseDTO responseDTO = (GetContentResponseDTO) o;
            if (StringUtils.isBlank(responseDTO.getContent()) || StringUtils.isBlank(responseDTO
                    .getTitle()))
                MTracker.getInstance().GA(Screen.terms_and_privacy, Category
                        .terms_and_privacy, Action.error, "terms");
            else {
                mTermsTitle.setText(responseDTO.getTitle());
                mTermsText.setText(Html.fromHtml(responseDTO.getContent()));
            }

            GetContentService.getInstance().getContent(new PrivacyCallback(), "privacy",
                    SessionService.getInstance().getLocale(getActivity()));
        }
    }

    class PrivacyCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            GetContentResponseDTO responseDTO = (GetContentResponseDTO) o;
            if (StringUtils.isBlank(responseDTO.getContent()) || StringUtils.isBlank(responseDTO
                    .getTitle()))
                MTracker.getInstance().GA(Screen.terms_and_privacy, Category
                        .terms_and_privacy, Action.error, "privacy");
            else {
                mPrivacyTitle.setText(responseDTO.getTitle());
                mPrivacyText.setText(Html.fromHtml(responseDTO.getContent()));
            }
        }
    }
}
