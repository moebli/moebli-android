package com.moebli.android.frontend.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.EditText;
import android.widget.RadioButton;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.util.FontManager;

public class RadioButtonBase extends RadioButton {

    public RadioButtonBase(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public RadioButtonBase(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RadioButtonBase(Context context) {
        super(context);
        init();
    }

    public void init() {
        // text
        FontManager fontManager = FontManager.getInstance(getContext().getAssets());
        Typeface tf = fontManager.getFont("fonts/Lato-Regular.ttf");
        setTypeface(tf, 1);
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        setTextColor(getResources().getColor(R.color.m_form_label));
        setHintTextColor(getResources().getColor(R.color.m_form_hint));
    }
}
