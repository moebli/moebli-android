package com.moebli.android.frontend.user;


import android.support.v4.app.Fragment;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.SingleFragmentWithBackActivity;

public class NotificationSettingActivity extends SingleFragmentWithBackActivity {

    @Override
    protected Fragment createFragment() {
        return new NotificationSettingsFragment();
    }

    @Override
    protected int getActivityTitle() {
        return R.string.action_upd_notific_settings;
    }

}
