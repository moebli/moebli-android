package com.moebli.android.frontend.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.generic.SplashScreenActivity;

// needs to extend AppCompatActivity
// http://stackoverflow.com/questions/30612985/android-snackbar-not-working
public class LoginActivity extends AppCompatActivity {

    public static final String LOGOUT = "logout";
    private static final String C = LoginActivity.class.getName();

    private FacebookLoginFragment mFacebookLoginFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // http://stackoverflow.com/a/9580057/1193781
        boolean logout = getIntent().getBooleanExtra(LOGOUT, false);
        if (logout) {
            startActivity(new Intent(getApplicationContext(), SplashScreenActivity.class));
            finish();
            return;
        }

        setContentView(R.layout.login_activity);

        // toolbar as actionbar
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.login_fragment_container);
        if (fragment == null) {
            fragment = new LoginFragment();
            fm.beginTransaction().add(R.id.login_fragment_container, fragment).commit();
        }

        fragment = fm.findFragmentById(R.id.signup_btn_fragment_container);
        if (fragment == null) {
            fragment = new SignUpButtonFragment();
            fm.beginTransaction().add(R.id.signup_btn_fragment_container, fragment).commit();
        }

        fragment = fm.findFragmentById(R.id.fb_login_fragment_container);
        if (fragment == null) {
            fragment = new FacebookLoginFragment();
            fm.beginTransaction().add(R.id.fb_login_fragment_container, fragment).commit();
        }
        mFacebookLoginFragment = (FacebookLoginFragment) fragment;
    }

    // called upon return from facebook login
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mFacebookLoginFragment.getFacebookCallbackManager().onActivityResult(requestCode,
                resultCode, data);
    }


}
