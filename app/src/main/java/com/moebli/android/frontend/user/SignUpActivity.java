package com.moebli.android.frontend.user;

import android.support.v4.app.Fragment;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.SingleFragmentWithBackActivity;
import com.moebli.android.frontend.SingleFragmentWithLogoActivity;

public class SignUpActivity extends SingleFragmentWithBackActivity {

    @Override
    protected Fragment createFragment() {
        return new SignUpFragment();
    }

    @Override
    protected int getActivityTitle() {
        return R.string.action_sign_up;
    }

}
