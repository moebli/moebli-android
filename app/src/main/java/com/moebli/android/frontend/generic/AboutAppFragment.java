package com.moebli.android.frontend.generic;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moebli.android.backend.session.SessionService;
import com.moebli.android.backend.util.GetContentResponseDTO;
import com.moebli.android.backend.util.GetContentService;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.util.RemoteCallError;

import org.apache.commons.lang3.StringUtils;

public class AboutAppFragment extends Fragment {

    private TextView mAboutText;

    public AboutAppFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.g_about_app_fragment, container, false);
        mAboutText = (TextView) v.findViewById(R.id.about_app_text);

        GetContentService.getInstance().getContent(new AboutCallback(), "about_mobile_app",
                SessionService
                        .getInstance().getLocale(getActivity()));

        MTracker.getInstance().GA(Screen.about_app, Category.about_app, Action.view);
        return v;
    }

    class AboutCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            GetContentResponseDTO responseDTO = (GetContentResponseDTO) o;
            if (StringUtils.isBlank(responseDTO.getContent()) || StringUtils.isBlank(responseDTO
                    .getTitle()))
                MTracker.getInstance().GA(Screen.about_app, Category
                        .about_app, Action.error);
            else {
                mAboutText.setText(Html.fromHtml(responseDTO.getContent()));
            }
        }
    }

}
