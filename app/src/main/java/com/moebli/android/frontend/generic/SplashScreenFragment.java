package com.moebli.android.frontend.generic;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.moebli.android.backend.enums.Constants;
import com.moebli.android.backend.enums.SharedPrefs;
import com.moebli.android.backend.session.GetSessionResponseDTO;
import com.moebli.android.backend.session.GetSessionService;
import com.moebli.android.backend.session.SessionService;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.BuildConfig;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.user.LoginActivity;
import com.moebli.android.frontend.util.RemoteCallError;

import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;

public class SplashScreenFragment extends Fragment {

    public SplashScreenFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String versionName = BuildConfig.VERSION_NAME;
        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService
                (Context.TELEPHONY_SERVICE);
        GetSessionService.getInstance().getSession(versionName, telephonyManager, new
                GetSessionCallback());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.splash_screen_fragment, container, false);

        TextView agreeNContLink = (TextView) v.findViewById(R.id.link_agree_n_cont);
        TextView termsNPrivacyLink = (TextView) v.findViewById(R.id.link_terms_n_pvc);

        agreeNContLink.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                // set flag for terms accepted
                SharedPreferences sharedPrefs = getActivity().getSharedPreferences(Constants
                        .PREF_FILE, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putBoolean(SharedPrefs.TERMS_ACCEPTED.name(), true);
                editor.commit();

                // check if session is already available
                if (SessionService.getInstance().getSessionId() != null) {
                    Intent loginIntent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(loginIntent);
                } else {
                    callGetSessionApiAndNavigatetoLogin();
                }
            }
        });

        termsNPrivacyLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent termsNPrivacyActivity = new Intent(getActivity(), TermsAndPrivacyActivity.class);
                startActivity(termsNPrivacyActivity);
            }
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        MTracker.getInstance().GA(Screen.splash_screen, Category.splash_screen, Action.view);
    }

    public class GetSessionCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            // save session in shared preferences
            GetSessionResponseDTO responseDTO = (GetSessionResponseDTO) o;
            SessionService.getInstance().saveNewSession(getActivity(), responseDTO);

            // check update status
            if (responseDTO.isForceUpdate()) {
                showUpdateRequiredDialog(responseDTO.getMessage());
                return;
            }

            if (responseDTO.isRecommendUpdate()) {
                SharedPreferences sharedPref = getActivity().getSharedPreferences(Constants
                        .PREF_FILE, Context.MODE_PRIVATE);
                Long last = sharedPref.getLong(SharedPrefs.UPDATE_APP_SHOWN_DATE.name(), 0);
                Long now = System.currentTimeMillis();
                if (last == 0 || now - last > 1000 * 60 * 60 * 24 * 14) { // 2 weeks
                    showUpdateRecommendedDialog(responseDTO.getMessage());
                    return;
                }
                checkTermsAccepted();
                return;
            }

            if (StringUtils.isNotBlank(responseDTO.getMessage())) {
                showCustomMessageDialog(responseDTO.getMessage());
                return;
            }

            checkTermsAccepted();
        }
    }

    private void callGetSessionApiAndNavigatetoLogin() {
        String versionName = BuildConfig.VERSION_NAME;
        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService
                (Context.TELEPHONY_SERVICE);
        GetSessionService.getInstance().getSession(versionName, telephonyManager, new
                GetSessionCallbackAndNavigateToLogin());
    }

    class GetSessionCallbackAndNavigateToLogin implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            // save session in shared preferences
            GetSessionResponseDTO responseDTO = (GetSessionResponseDTO) o;
            SessionService.getInstance().saveNewSession(getActivity(), responseDTO);

            Intent loginIntent = new Intent(getActivity(), LoginActivity.class);
            startActivity(loginIntent);
        }
    }

    private void showUpdateRequiredDialog(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), R.style
                .AlertDialog);

        String appName = getResources().getString(R.string.label_app_name);
        alertDialogBuilder.setTitle(MessageFormat.format(getResources().getString(R.string
                .info_title_update_app), appName));
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.action_update),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MTracker.getInstance().GA(Screen.update_force, Category.update_force, Action
                                        .submit);
                        String packageName = getActivity().getApplicationContext().getPackageName();
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                                    ("market://details?id=" + packageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play" +
                                    ".google.com/store/apps/details?id=" + packageName)));
                        }
                    }
                });

        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.action_quit), new
                DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MTracker.getInstance().GA(Screen.update_force, Category.update_force, Action.cancel);
                getActivity().finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        MTracker.getInstance().GA(Screen.update_force, Category.update_force, Action.view);
        alertDialog.show();
    }

    private void showUpdateRecommendedDialog(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), R.style
                .AlertDialog);

        String appName = getResources().getString(R.string.label_app_name);
        alertDialogBuilder.setTitle(MessageFormat.format(getResources().getString(R.string
                .info_title_update_app), appName));
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.action_update),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MTracker.getInstance().GA(Screen.update_recommend, Category.update_recommend, Action
                                .submit);
                        String packageName = getActivity().getApplicationContext().getPackageName();
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                                    ("market://details?id=" + packageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play" +
                                    ".google.com/store/apps/details?id=" + packageName)));
                        }
                    }
                });

        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.action_not_now),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MTracker.getInstance().GA(Screen.update_recommend, Category.update_recommend, Action
                                        .cancel);
                        // save dialog last shown
                        SharedPreferences sharedPref = getActivity().getSharedPreferences
                                (Constants.PREF_FILE, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putLong(SharedPrefs.UPDATE_APP_SHOWN_DATE.name(), System
                                .currentTimeMillis());
                        editor.commit();

                        checkTermsAccepted();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        MTracker.getInstance().GA(Screen.update_recommend, Category.update_recommend, Action.view);
        alertDialog.show();
    }

    private void showCustomMessageDialog(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), R.style
                .AlertDialog);

        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.action_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                checkTermsAccepted();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        MTracker.getInstance().GA(Screen.splash_screen, Category.splash_screen, Action.view,
                "custom_message");
        alertDialog.show();
    }

    private void checkTermsAccepted() {
        // if terms accepted before then go to login page without waiting for user in splash screen
        SharedPreferences sharedPrefs = getActivity().getSharedPreferences(Constants.PREF_FILE,
                Context.MODE_PRIVATE);
        boolean termsAccepted = sharedPrefs.getBoolean(SharedPrefs.TERMS_ACCEPTED.name(), false);

        // if terms accepted before, go to login page
        if (termsAccepted || BuildConfig.BUILD_TYPE.equals("debug")) {
            Intent loginIntent = new Intent(getActivity(), LoginActivity.class);
            startActivity(loginIntent);
        }
    }

}
