package com.moebli.android.frontend.generic;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;

import com.facebook.appevents.AppEventsLogger;
import com.moebli.android.backend.enums.SharedPrefs;
import com.moebli.android.backend.session.SessionService;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.gcm.GCMRegistrationService;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initializeStuff();

        setContentView(R.layout.splash_screen_activity);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.splash_screen_container);

        if (fragment == null) {
            fragment = new SplashScreenFragment();
            fm.beginTransaction().add(R.id.splash_screen_container, fragment).commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    private void initializeStuff() {
        SessionService.getInstance().initializeEcid(this);
    }

}
