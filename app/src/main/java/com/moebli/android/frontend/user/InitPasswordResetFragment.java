package com.moebli.android.frontend.user;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.moebli.android.backend.user.InitPasswordResetService;
import com.moebli.android.backend.util.BaseResponseDTO;
import com.moebli.android.backend.util.ErrorDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.util.RemoteCallError;

import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.List;

public class InitPasswordResetFragment extends Fragment {

    private EditText mEmailEdit;
    private LinearLayout mResetForm;
    private LinearLayout mResetMsg;

    public InitPasswordResetFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.u_init_password_reset_fragment, container, false);
        mEmailEdit = (EditText) v.findViewById(R.id.email_edit_text);
        mResetForm = (LinearLayout) v.findViewById(R.id.init_password_reset_form);
        mResetForm.setVisibility(View.VISIBLE);

        mResetMsg = (LinearLayout) v.findViewById(R.id.reset_msg);
        mResetMsg.setVisibility(View.GONE);

        Button continueButton = (Button) v.findViewById(R.id.init_password_reset_button);
        continueButton.setOnClickListener(new InitPasswordResetOnClickListener());

        TextView recvdCodeLink1 = (TextView) v.findViewById(R.id.received_code_link_1);
        TextView recvdCodeLink2 = (TextView) v.findViewById(R.id.received_code_link_2);
        recvdCodeLink1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent doPasswordResetIntent = new Intent(getActivity(), DoPasswordResetActivity.class);
                doPasswordResetIntent.putExtra(DoPasswordResetFragment.PHONE_OR_EMAIL, mEmailEdit
                        .getText().toString());
                startActivity(doPasswordResetIntent);
            }
        });
        recvdCodeLink2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent doPasswordResetIntent = new Intent(getActivity(), DoPasswordResetActivity.class);
                doPasswordResetIntent.putExtra(DoPasswordResetFragment.PHONE_OR_EMAIL, mEmailEdit
                        .getText().toString());
                startActivity(doPasswordResetIntent);
            }
        });

        MTracker.getInstance().GA(Screen.init_password_reset, Category.init_password_reset, Action.view);
        return v;
    }

    private class InitPasswordResetOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            MTracker.getInstance().GA(Screen.init_password_reset, Category.init_password_reset,
                    Action.submit);

            String emailEdit = mEmailEdit.getText().toString();
            if (StringUtils.isBlank(emailEdit)) {
                String s = MessageFormat.format(getResources().getString(R.string
                        .error_required_field), getResources().getString(R.string.label_email_address));
                mEmailEdit.setError(s);
                MTracker.getInstance().GA(Screen.init_password_reset, Category.init_password_reset, Action
                        .error, "empty_email");
                return;
            }

            InitPasswordResetService.getInstance().initPasswordReset(new
                    InitPasswordResetCallback(), emailEdit);
        }
    }

    private class InitPasswordResetCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            BaseResponseDTO responseDTO = (BaseResponseDTO) o;
            List<ErrorDTO> errors = responseDTO.getErrorList();
            if (errors.size() > 0) {
                for (ErrorDTO error : errors) {
                    mEmailEdit.setError(error.getMessage());
                    MTracker.getInstance().GA(Screen.init_password_reset, Category.init_password_reset,
                            Action.error, error.getFieldName());
                }
                return;
            }
            else {
                MTracker.getInstance().GA(Screen.init_password_reset, Category
                        .init_password_reset, Action.success);
                crossFade();
            }
        }
    }

    private void crossFade() {
        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        mResetForm.animate().alpha(0f).setDuration(400).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mResetForm.setVisibility(View.GONE);
                // Set the content view to 0% opacity but visible, so that it is visible
                // (but fully transparent) during the animation.
                mResetMsg.setAlpha(0f);
                mResetMsg.setVisibility(View.VISIBLE);

                // Animate the content view to 100% opacity, and clear any animation
                // listener set on the view.
                mResetMsg.animate().alpha(1f).setDuration(400).setListener(null);

            }
        });
    }
}
