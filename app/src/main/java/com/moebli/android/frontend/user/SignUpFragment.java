package com.moebli.android.frontend.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.moebli.android.backend.enums.CountryCode;
import com.moebli.android.backend.session.LoginResponseDTO;
import com.moebli.android.backend.session.SessionService;
import com.moebli.android.backend.user.SignUpRequestDTO;
import com.moebli.android.backend.user.SignUpService;
import com.moebli.android.backend.util.ErrorDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.WebAppInterface;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.bill.ContactBalancesActivity;
import com.moebli.android.frontend.util.RemoteCallError;
import com.moebli.android.frontend.widget.EditTextBase;

import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

public class SignUpFragment extends Fragment {

    private static String C = SignUpFragment.class.getName();
    private static SessionService sessionService = SessionService.getInstance();
    private SignUpService signUpService = new SignUpService();

    private static EditTextBase mPhoneOrEmailEdit;
    private static EditText mPasswordEdit;
    private static EditText mFirstName;
    private static EditText mLastName;
    private static Spinner mCountryCode;

    private static WebView mRecaptchView;

    private WebAppInterface webAppInterface = new WebAppInterface(getActivity());


    public SignUpFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.signup_form_fragment, container, false);

        mPhoneOrEmailEdit = (EditTextBase) v.findViewById(R.id.phone_or_email_edit_text);
        mPasswordEdit = (EditText) v.findViewById(R.id.password_edit_text);
        mFirstName = (EditText) v.findViewById(R.id.first_name_edit_text);
        mLastName = (EditText) v.findViewById(R.id.last_name_edit_text);
        mCountryCode = (Spinner) v.findViewById(R.id.country_code);
        List<String> countries = new ArrayList<>(CountryCode.getMap().keySet());
        ArrayAdapter<String> countryCodeArrayAdapter = new ArrayAdapter<String>(getActivity(),R.layout.spinner_item, countries);
        mCountryCode.setAdapter(countryCodeArrayAdapter);
        mCountryCode.setSelection(CountryCode.getUserCountryPosition(sessionService.getCountryCode(getActivity())));

        mRecaptchView = (WebView) v.findViewById(R.id.recaptchaView);
        mRecaptchView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
        WebSettings settings = mRecaptchView.getSettings();
        settings.setJavaScriptEnabled(true);
        mRecaptchView.addJavascriptInterface(webAppInterface, "mRecaptcha");
        Map<String, String> param = new HashMap<String, String>();
        param.put("SessionId", sessionService.getSessionId(getActivity()));
        mRecaptchView.loadUrl(BASE_URL + "/recaptcha", param);

        Button button = (Button) v.findViewById(R.id.signup_button);

        button.setOnClickListener(new SignUpButtonOnClickListener());
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        MTracker.getInstance().GA(Screen.sign_up, Category.sign_up, Action.view);
    }

    class SignUpButtonOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            MTracker.getInstance().GA(Screen.sign_up, Category.sign_up, Action.submit);

            String firstName = mFirstName.getText().toString();
            String lastName = mLastName.getText().toString();
            String email = mPhoneOrEmailEdit.getText().toString();
            String password = mPasswordEdit.getText().toString();
            String gRecaptchaResponse = webAppInterface.getgRecaptchaResponse();

            Boolean isError = false;

            // focus on topmost error
            boolean focusRequested = false;
            if(StringUtils.isBlank(firstName)) {
                mFirstName.setError(MessageFormat.format(getResources().getString(R.string
                        .error_required_field), getResources().getString(R.string
                        .label_first_name)));
                isError = true;
                mFirstName.requestFocus();
                focusRequested = true;
                MTracker.getInstance().GA(Screen.sign_up, Category.sign_up, Action.error,
                        "empty_first_name");
            }

            if(StringUtils.isBlank(lastName)) {
                mLastName.setError(MessageFormat.format(getResources().getString(R.string
                        .error_required_field), getResources().getString(R.string
                        .label_last_name)));
                isError = true;
                if (! focusRequested) {
                    mLastName.requestFocus();
                    focusRequested = true;
                }
                MTracker.getInstance().GA(Screen.sign_up, Category.sign_up, Action.error,
                        "empty_last_name");
            }

            if(StringUtils.isBlank(email)) {
                mPhoneOrEmailEdit.setError(MessageFormat.format(getResources().getString(R.string
                        .error_required_field), getResources().getString(R.string.label_email)));
                isError = true;
                if (! focusRequested) {
                    mPhoneOrEmailEdit.requestFocus();
                    focusRequested = true;
                }
                MTracker.getInstance().GA(Screen.sign_up, Category.sign_up, Action.error,
                        "empty_email");
            }

            if(StringUtils.isBlank(password)) {
                mPasswordEdit.setError(MessageFormat.format(getResources().getString(R.string
                        .error_required_field), getResources().getString(R.string.label_password)));
                isError = true;
                if (! focusRequested) {
                    mPasswordEdit.requestFocus();
                }
                MTracker.getInstance().GA(Screen.sign_up, Category.sign_up, Action.error,
                        "empty_password");
            }

            if(StringUtils.isBlank(gRecaptchaResponse)) {
                //TODO need to show captcha message required.
                mRecaptchView.setFocusable(true);
                isError = true;
            }

            if(isError)
                return;

            SignUpRequestDTO requestDTO  = new SignUpRequestDTO();
            requestDTO.setFirstName(firstName);
            requestDTO.setLastName(lastName);
            requestDTO.setCountryCode(CountryCode.getCountryCode(mCountryCode.getSelectedItem().toString()).toString());
            requestDTO.setEmail(email);
            requestDTO.setPassword(password);
            requestDTO.setgRecaptchaResponse(gRecaptchaResponse);
            signUpService.signUp(new SignUpCallback(), requestDTO);
        }
    }

    public class SignUpCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            LoginResponseDTO response = (LoginResponseDTO) o;
            if (response.getErrorList().size() > 0) {
                // focus on topmost error
                boolean focusRequested = false;
                for (ErrorDTO error : response.getErrorList()) {
                    if ("firstName".equals(error.getFieldName())) {
                        mFirstName.setError(error.getMessage());
                        if (!focusRequested) {
                            mFirstName.requestFocus();
                            focusRequested = true;
                            MTracker.getInstance().GA(Screen.sign_up, Category.sign_up, Action.error,
                                    error.getFieldName());
                            break;
                        }
                    }
                }
                for (ErrorDTO error : response.getErrorList()) {
                    if("lastName".equals(error.getFieldName())) {
                        mLastName.setError(error.getMessage());
                        if (!focusRequested) {
                            mLastName.requestFocus();
                            focusRequested = true;
                            MTracker.getInstance().GA(Screen.sign_up, Category.sign_up, Action.error,
                                    error.getFieldName());
                            break;
                        }
                    }
                }
                for (ErrorDTO error : response.getErrorList()) {
                    if ("email".equals(error.getFieldName())) {
                        mPhoneOrEmailEdit.setError(error.getMessage());
                        if (!focusRequested) {
                            mPhoneOrEmailEdit.requestFocus();
                            focusRequested = true;
                            MTracker.getInstance().GA(Screen.sign_up, Category.sign_up, Action.error, error.getFieldName());

                            break;
                        }
                    }
                }
                for (ErrorDTO error : response.getErrorList()) {
                    if ("password".equals(error.getFieldName())) {
                        mPasswordEdit.setError(error.getMessage());
                        if (!focusRequested) {
                            mPasswordEdit.requestFocus();
                            MTracker.getInstance().GA(Screen.sign_up, Category.sign_up, Action.error,
                                    error.getFieldName());
                            break;
                        }
                    }
                }
            }
            else {
                sessionService.postLoginSessionUpdate(getActivity(), mPhoneOrEmailEdit.getText().toString(),
                        response);
                MTracker.getInstance().GA(Screen.sign_up, Category.sign_up, Action.success);

                Intent contactBalancesIntent = new Intent(getActivity(), ContactBalancesActivity.class);
                startActivity(contactBalancesIntent);
            }
        }
    }

}
