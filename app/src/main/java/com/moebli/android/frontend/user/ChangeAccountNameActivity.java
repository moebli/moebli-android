package com.moebli.android.frontend.user;


import android.support.v4.app.Fragment;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.SingleFragmentWithBackActivity;

public class ChangeAccountNameActivity extends SingleFragmentWithBackActivity {

    @Override
    protected Fragment createFragment() {
        return new ChangeAccountNameFragment();
    }

    @Override
    protected int getActivityTitle() {
        return R.string.action_update_account_info;
    }

}
