package com.moebli.android.frontend.bill;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.ReplacementSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.moebli.android.backend.billing.bill.BillDTO;
import com.moebli.android.backend.billing.billlist.ContactBillsService;
import com.moebli.android.backend.billing.billlist.GetContactBillsResponseDTO;
import com.moebli.android.backend.billing.billlist.GetItemBillsResponseDTO;
import com.moebli.android.backend.billing.billlist.GetMonthlyBillsResponseDTO;
import com.moebli.android.backend.billing.billlist.ItemBillsService;
import com.moebli.android.backend.billing.billlist.MonthlyBillsService;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.JsonUtil;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.util.RemoteCallError;
import com.moebli.android.frontend.util.SizeConverter;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

/*
 * https://developer.android.com/training/material/lists-cards.html
 * http://stackoverflow.com/questions/24471109/recyclerview-onclick
 */
public class BillListFragment extends Fragment {

    // class variables
    private String mContactUuid;
    private String mMonthYearCode;
    private String mItemUuid;

    // ui elements
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public BillListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.object_list_fragment, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.object_list_recycler);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof ContactActivity) {
            mContactUuid = ((ContactActivity) getActivity()).getContactUuid();
            ContactBillsService.getInstance().getContactBills(new GetContactBillsCallback(),
                    mContactUuid);
        }
        if (getActivity() instanceof MonthlyBillsActivity) {
            mMonthYearCode = ((MonthlyBillsActivity) getActivity()).getMonthYearCode();
            MonthlyBillsService.getInstance().getMonthlyBills(new GetMonthlyBillsCallback(),
                    mMonthYearCode);
        }
        if (getActivity() instanceof ItemBillsActivity) {
            mItemUuid = ((ItemBillsActivity) getActivity()).getItemUuid();
            ItemBillsService.getInstance().getItemBills(new GetItemBillsCallback(), mItemUuid);
        }
    }

    class GetContactBillsCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            List<BillDTO> billDTOs = ((GetContactBillsResponseDTO) o).getBillList();
            mAdapter = new BillListAdapter(billDTOs);
            mRecyclerView.setAdapter(mAdapter);

            MTracker.getInstance().GA(Screen.contact_bills, Category.contact_bills, Action.view,
                    0l + billDTOs.size());
        }
    }

    class GetMonthlyBillsCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            List<BillDTO> billDTOs = ((GetMonthlyBillsResponseDTO) o).getBillList();
            mAdapter = new BillListAdapter(billDTOs);
            mRecyclerView.setAdapter(mAdapter);

            MTracker.getInstance().GA(Screen.monthly_bills, Category.monthly_bills, Action.view,
                    0l + billDTOs.size());
        }
    }

    class GetItemBillsCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            List<BillDTO> billDTOs = ((GetItemBillsResponseDTO) o).getBillList();
            mAdapter = new BillListAdapter(billDTOs);
            mRecyclerView.setAdapter(mAdapter);

            MTracker.getInstance().GA(Screen.item_bills, Category.item_bills, Action.view,
                    0l + billDTOs.size());
        }
    }

    private List<BillDTO> mBillDTOList;

    public class BillListAdapter extends RecyclerView.Adapter<BillViewHolder> {

        // Provide a suitable constructor (depends on the kind of dataset)
        public BillListAdapter(List<BillDTO> billDTOList) {
            mBillDTOList = billDTOList;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public BillViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.bill_row, parent,
                    false);
            // set the view's size, margins, paddings and layout parameters

            BillViewHolder vh = new BillViewHolder(v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(BillViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            BillDTO billDTO = mBillDTOList.get(position);
            holder.mBillDateText.setText(billDTO.getDateFormatted());
            holder.mAmountText.setText(billDTO.getAmountFormatted());
            if (billDTO.getPayTo().equals("contact")) {
                holder.mAmountText.setTextColor(getResources().getColor(R.color.balance_negative));
            } else {
                holder.mAmountText.setTextColor(getResources().getColor(R.color.balance_positive));
            }
            // (billDTO.getRecordType().equals("payment"))
            //    holder.mPaymentText.setVisibility(View.VISIBLE);
            // if ("paypal".equals(billDTO.getProvider()))
            //     holder.mPaypalImage.setVisibility(View.VISIBLE);

            // http://stackoverflow.com/questions/5561981
            SpannableStringBuilder spannable = new SpannableStringBuilder();
            if (billDTO.getRecordType().equals("payment")) {
                spannable.append(getActivity().getResources().getString(R.string
                        .label_title_payment));
                spannable.setSpan(new PaddingBackgroundSpan(0xfff9f2f4, 0xffc7254e), 0, spannable
                        .length(), 0);
                spannable.setSpan(new RelativeSizeSpan(0.9f), 0, spannable.length(), 0);

                spannable.append("   ");
            }
            if ("paypal".equals(billDTO.getProvider())) {
                Drawable paypalImg = getActivity().getResources().getDrawable(R.drawable.paypal);
                paypalImg.setBounds(0, 0, (int) (holder.mBillNameText.getLineHeight()), holder
                        .mBillNameText.getLineHeight());
                spannable.setSpan(new ImageSpan(paypalImg), spannable.length() - 1, spannable
                        .length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            spannable.append(billDTO.getBillMessage());

            holder.mBillNameText.setText(spannable);

            if (StringUtils.isNotBlank(billDTO.getInternalNotes())) {
                holder.mMemoText.setVisibility(View.VISIBLE);
                holder.mMemoText.setText(billDTO.getInternalNotes());
            }
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mBillDTOList.size();
        }
    }

    public class PaddingBackgroundSpan extends ReplacementSpan {
        private int mBackgroundColor;
        private int mForegroundColor;

        public PaddingBackgroundSpan(int backgroundColor, int foregroundColor) {
            this.mBackgroundColor = backgroundColor;
            this.mForegroundColor = foregroundColor;
        }

        @Override
        public int getSize(Paint paint, CharSequence text, int start, int end, Paint.FontMetricsInt fm) {
            return Math.round(measureText(paint, text, start, end));
        }

        @Override
        public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top,
                         int y, int bottom, Paint paint) {
            int dp2 = SizeConverter.dpToPx(getActivity().getResources(), 2);
            RectF rect = new RectF(x, top + dp2
                    , x + measureText(paint, text, start, end) + SizeConverter.dpToPx(getActivity
                    ().getResources(), 8), bottom + dp2);
            paint.setColor(mBackgroundColor);
            canvas.drawRoundRect(rect, dp2, dp2, paint);
            paint.setColor(mForegroundColor);
            canvas.drawText(text, start, end, x + SizeConverter.dpToPx(getActivity().getResources
                    (), 4), y, paint);
        }

        private float measureText(Paint paint, CharSequence text, int start, int end) {
            return paint.measureText(text, start, end);
        }

    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class BillViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public LinearLayout mBillRow;
        public TextView mBillDateText;
        public TextView mBillNameText;
        public TextView mAmountText;
        public TextView mMemoText;
        // public TextView mPaymentText;
        // public ImageView mPaypalImage;

        public BillViewHolder(View view) {
            super(view);
            mBillRow = (LinearLayout) view.findViewById(R.id.bill_row_layout);
            mBillDateText = (TextView) view.findViewById(R.id.bill_date_text);
            mBillNameText = (TextView) view.findViewById(R.id.bill_name_text);
            mAmountText = (TextView) view.findViewById(R.id.amount_text);
            mMemoText = (TextView) view.findViewById(R.id.memo_text);
            // mPaymentText = (TextView) view.findViewById(R.id.payment_text);
            // mPaypalImage = (ImageView) view.findViewById(R.id.paypal_image);

            // taking a simple approach to make the entire row clickable
            // other solutions exist at:
            // http://stackoverflow.com/questions/24471109/
            // http://stackoverflow.com/questions/24885223/
            mBillRow.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int itemPosition = getPosition();
            BillDTO billDTO = mBillDTOList.get(itemPosition);

            Intent updateBillIntent = new Intent(getActivity(), UpdateBillActivity.class);
            updateBillIntent.putExtra(UpdateBillActivity.BILL_DTO, JsonUtil.getObjectToJSON
                    (billDTO));
            startActivity(updateBillIntent);
        }

    }

}
