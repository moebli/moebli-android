package com.moebli.android.frontend.generic;

import android.app.Application;

public class Payminder extends Application {
    private static Payminder mPayminder;

    public static Payminder getInstance() {
         return mPayminder;
    }

    public Payminder() {
        super();
        mPayminder = this;
    }

}