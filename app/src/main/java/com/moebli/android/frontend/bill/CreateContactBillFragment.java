package com.moebli.android.frontend.bill;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.moebli.android.backend.billing.balance.ContactBalanceDTO;
import com.moebli.android.backend.billing.balance.ContactBalanceService;
import com.moebli.android.backend.billing.bill.BillDTO;
import com.moebli.android.backend.billing.bill.CreateBillRequestDTO;
import com.moebli.android.backend.billing.bill.CreateBillResponseDTO;
import com.moebli.android.backend.billing.bill.CreateBillService;
import com.moebli.android.backend.enums.Currency;
import com.moebli.android.backend.item.ItemService;
import com.moebli.android.backend.session.SessionService;
import com.moebli.android.backend.util.ErrorDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.JsonUtil;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.util.RemoteCallError;

import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.List;

// https://github.com/danilao/fragments-viewpager-example
public class CreateContactBillFragment extends Fragment {

    // ui elements
    private ScrollView mFormLayout;
    private AutoCompleteTextView mItemEdit;
    private RadioGroup payToRadioGroup;
    private RadioButton payToMeRadio;
    private RadioButton mPayToContactRadio;
    private EditText mAmount0Edit;
    private Spinner mCurrencySpinner;
    private Button mCreateBillButton;
    // success section
    private LinearLayout mSuccessLayout;
    private TextView mSuccessMessage;
    private TextView mBlockedMessage;
    private Button mAddAnother;
    private View mBillRow;

    // class variables
    private String mContactUuid;
    private ContactBalanceDTO contactBalanceDTO;
    private CreateBillListener mCreateBillListener;
    private static CreateContactBillFragment mCreateContactBillFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {

        mContactUuid = ((ContactActivity) getActivity()).getContactUuid();
        contactBalanceDTO = ContactBalanceService.getInstance().getContactBalanceDTO(mContactUuid);
        View v = inflater.inflate(R.layout.create_contact_bill_fragment, container, false);

        // create bill form
        mFormLayout = (ScrollView) v.findViewById(R.id.create_contact_bill_form);
        mItemEdit = (AutoCompleteTextView) v.findViewById(R.id.item_edit);
        payToRadioGroup = (RadioGroup) v.findViewById(R.id.pay_to_radio_group);
        payToMeRadio = (RadioButton) v.findViewById(R.id.pay_to_me_radio);
        mPayToContactRadio = (RadioButton) v.findViewById(R.id.pay_to_conn_radio);
        payToMeRadio.setText(MessageFormat.format(getResources().getString(R.string
                .label_contact_owes_me), contactBalanceDTO.getAccountName()));
        mPayToContactRadio.setText(MessageFormat.format(getResources().getString(R.string
                .label_i_owe_contact), contactBalanceDTO.getAccountName()));
        mAmount0Edit = (EditText) v.findViewById(R.id.amount_1_edit);
        mCreateBillButton = (Button) v.findViewById(R.id.create_bill_button);
        mCreateBillButton.setOnClickListener(new CreateBillButtonClickListener());

        // currency
        mCurrencySpinner = (Spinner) v.findViewById(R.id.currency);
        List<Currency> currencies = Currency.getCurrencyCodeEnumList();
        ArrayAdapter<Currency> currencyArrayAdapter = new CreateBillFormFragment
                .CurrencySpinnerAdapter(getActivity(), R.layout.currency_row, currencies);
        mCurrencySpinner.setAdapter(currencyArrayAdapter);
        String sessionCurrency = SessionService.getInstance().getCurrencyCode(getActivity());
        for (int i = 0; i < currencies.size(); i++) {
            if (currencies.get(i).getCode().equals(sessionCurrency)) {
                mCurrencySpinner.setSelection(i);
                break;
            }
        }

        // success section
        mSuccessLayout = (LinearLayout) v.findViewById(R.id.create_contact_bill_success);
        mSuccessMessage = (TextView) v.findViewById(R.id.success_text);
        mBlockedMessage = (TextView) v.findViewById(R.id.blocked_message);
        mBillRow = v.findViewById(R.id.bill_row);
        mAddAnother = (Button) v.findViewById(R.id.button_add_another);
        mAddAnother.setOnClickListener(new AddAnotherClickListener());

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCreateBillListener = (CreateBillListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement " +
                    "CreateBillListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        showForm();
    }

    private void showForm() {
        mFormLayout.setVisibility(View.VISIBLE);
        mSuccessLayout.setVisibility(View.GONE);
        mItemEdit.setText("");
        mAmount0Edit.setText("");
        ItemService.getInstance().getItems(new GetItemsCallback());

        MTracker.getInstance().GA(Screen.create_contact_bill, Category.create_contact_bill, Action.view);
    }

    public class GetItemsCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            ArrayAdapter<String> itemAdapter = new ArrayAdapter(getActivity(), android.R.layout
                    .simple_dropdown_item_1line, ItemService.getInstance().getAccessibleItemStrings
                    (mContactUuid));
            mItemEdit.setAdapter(itemAdapter);
        }
    }

    private class CreateBillButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            MTracker.getInstance().GA(Screen.create_contact_bill, Category.create_contact_bill, Action
                    .submit);

            String itemName = mItemEdit.getText().toString();
            String amount = mAmount0Edit.getText().toString();

            if (StringUtils.isBlank(itemName)) {
                mItemEdit.setError(MessageFormat.format(getResources().getString(R.string.error_required_field), getResources().getString(R.string.label_event)));
                MTracker.getInstance().GA(Screen.create_contact_bill, Category.create_contact_bill, Action.error,
                        "empty_item");
            }
            if (StringUtils.isBlank(amount)) {
                mAmount0Edit.setError(MessageFormat.format(getResources().getString(R.string.error_required_field), getResources().getString(R.string.label_amount)));
                MTracker.getInstance().GA(Screen.create_contact_bill, Category
                        .create_contact_bill, Action.error, "empty_amount");
            }
            if (StringUtils.isBlank(itemName) || StringUtils.isBlank(amount)) {
                return;
            }

            CreateBillRequestDTO requestDTO = new CreateBillRequestDTO();
            requestDTO.setItemName(itemName);
            requestDTO.setItemUuid(ItemService.getInstance().getItemUuid(itemName));
            requestDTO.setRecordType(CreateBillService.RecordTypeEnum.bill.name());
            requestDTO.setContact0(contactBalanceDTO.getEmail());
            int selectedPayTo = payToRadioGroup.getCheckedRadioButtonId();
            String getMoney = selectedPayTo == mPayToContactRadio.getId() ? "-" : "";
            requestDTO.setAmount0(getMoney + amount);
            requestDTO.setCurrencyCode(mCurrencySpinner.getSelectedItem().toString());

            CreateBillService.getInstance().createBill(new CreateBillCallback(), requestDTO);
        }
    }

    public class CreateBillCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            CreateBillResponseDTO responseDTO = (CreateBillResponseDTO) o;
            if (responseDTO.getErrorList().size() > 0) {
                for (ErrorDTO error : responseDTO.getErrorList()) {
                    if ("itemName".equals(error.getFieldName()))
                        mItemEdit.setError(error.getMessage());
                    if ("amount0".equals(error.getFieldName()))
                        mAmount0Edit.setError(error.getMessage());
                    MTracker.getInstance().GA(Screen.create_contact_bill, Category.create_contact_bill,
                            Action.error, error.getFieldName());
                }
            } else {
                CreateBillFormFragment.updateCache(responseDTO);
                MTracker.getInstance().GA(Screen.create_contact_bill, Category
                        .create_contact_bill, Action.success, 0l + responseDTO.getBillList().size
                        ());

                // on success, notify parent activity
                mCreateBillListener.onCreateBillSuccess(responseDTO);

                mFormLayout.setVisibility(View.GONE);
                mSuccessLayout.setVisibility(View.VISIBLE);

                CreateBillSuccessFragment.showMessage(responseDTO.getBillList(), responseDTO
                        .getBlockedContactList(), responseDTO.getSuccess(), mSuccessMessage,
                        mBlockedMessage, mBillRow, getResources(), new BillClickListener
                                (responseDTO.getBillList()));
            }
        }
    }

    private class BillClickListener implements View.OnClickListener {
        private BillDTO mBillDTO;
        BillClickListener(List<BillDTO> billDTOs) {
            if (billDTOs.size() > 0)
                mBillDTO = billDTOs.get(0);
        }
        @Override
        public void onClick(View view) {
            Intent updateBillIntent = new Intent(getActivity(), UpdateBillActivity.class);
            updateBillIntent.putExtra(UpdateBillActivity.BILL_DTO, JsonUtil.getObjectToJSON
                    (mBillDTO));
            // send a flag indicating bill update from success page
            updateBillIntent.putExtra(UpdateBillActivity.IS_CREATE_SUCCESS, true);
            startActivity(updateBillIntent);
        }
    }

    private class AddAnotherClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            showForm();
        }
    }

    // Container Activity must implement this interface
    public interface CreateBillListener {
        void onCreateBillSuccess(CreateBillResponseDTO responseDTO);
    }

    public static CreateContactBillFragment getInstance() {
        if (mCreateContactBillFragment == null)
            mCreateContactBillFragment = new CreateContactBillFragment();
        return mCreateContactBillFragment;
    }

}
