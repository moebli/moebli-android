package com.moebli.android.frontend.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class TextUtil {
    public static void addTextErrorClearer(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            public void afterTextChanged(Editable edt){
                editText.setError(null);
            }
        });

    }
}
