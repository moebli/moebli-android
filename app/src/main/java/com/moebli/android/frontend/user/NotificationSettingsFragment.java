package com.moebli.android.frontend.user;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.moebli.android.backend.user.ChangeNotificationService;
import com.moebli.android.backend.user.GetCurrentUserService;
import com.moebli.android.backend.user.GetCurrentUserResponseDTO;
import com.moebli.android.backend.util.BaseResponseDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.util.RemoteCallError;

public class NotificationSettingsFragment extends Fragment {

    private static ChangeNotificationService changeNotificationService= new ChangeNotificationService();
    private static GetCurrentUserService getCurrentUserService = new GetCurrentUserService();

    private Switch mSwitch;

    public NotificationSettingsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_notif_setting, container, false);


        mSwitch = (Switch) v.findViewById(R.id.notifToggle);

        getCurrentUserService.getCurrentUser(new GetCurrentUserCallback());

        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                changeNotificationService.changeNotification(new ChangeNotificationCallback(),
                        isChecked);
                MTracker.getInstance().GA(Screen.email_notification, Category.email_notification, Action
                        .submit, "" + isChecked);
            }
        });

        MTracker.getInstance().GA(Screen.email_notification, Category.email_notification,
                Action.view);

        return v;
    }


    public class ChangeNotificationCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            BaseResponseDTO responseDTO = (BaseResponseDTO) o;
            Snackbar.make(getActivity().findViewById(android.R.id.content),responseDTO.getSuccess(),Snackbar.LENGTH_LONG).show();

            MTracker.getInstance().GA(Screen.email_notification, Category.email_notification,
                    Action.success);
        }
    }

    private class GetCurrentUserCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            GetCurrentUserResponseDTO responseDTO = (GetCurrentUserResponseDTO) o;
            mSwitch.setChecked(responseDTO.isBillEmailUnsubscribe());
        }
    }
}
