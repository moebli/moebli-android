package com.moebli.android.frontend.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.util.FontManager;

public class TextViewLink extends TextViewBase {

    public TextViewLink(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextViewLink(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewLink(Context context) {
        super(context);
        init();
    }

    public void init() {
        setTextColor(getResources().getColor(R.color.m_theme_accent));
    }
}
