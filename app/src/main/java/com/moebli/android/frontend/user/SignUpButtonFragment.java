package com.moebli.android.frontend.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.generic.AboutAppActivity;

public class SignUpButtonFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.u_signup_button_fragment, container, false);

        TextView aboutLink = (TextView) v.findViewById(R.id.about_app_link);
        Button signUpButton = (Button) v.findViewById(R.id.new_user_sign_up_button);
        signUpButton.setOnClickListener(new SignUpClickListener());

        aboutLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent aboutAppActivity = new Intent(getActivity(), AboutAppActivity.class);
                startActivity(aboutAppActivity);
            }
        });

        return v;
    }

    private class SignUpClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent signUpIntent = new Intent(getActivity(), SignUpActivity.class);
            startActivity(signUpIntent);
        }
    }
}
