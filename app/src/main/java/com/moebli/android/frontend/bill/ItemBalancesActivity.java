package com.moebli.android.frontend.bill;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.moebli.android.frontend.R;

public class ItemBalancesActivity extends AppCompatActivity {

    // ui elements
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.object_list_activity);

        // toolbar as actionbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.label_title_event_bal));

        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.create_bill_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent createBillIntent = new Intent(getApplicationContext(), CreateBillActivity
                        .class);
                startActivity(createBillIntent);
            }
        });
        */

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.object_list_container);

        if (fragment == null) {
            fragment = new ItemBalancesFragment();
            fm.beginTransaction().add(R.id.object_list_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // not using menu right now, but keep the code for possible future ise
        // getMenuInflater().inflate(R.menu.contact_balance_list_page_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // not using menu right now, but keep the code for possible future ise

        //if (mDrawerToggle != null && mDrawerToggle.onOptionsItemSelected(item)) {
        //    return true;
        //}

        /*
        int id = item.getItemId();

        if (id == R.id.action_new_tab) {
            Intent createBillIntent = new Intent(getApplicationContext(), CreateBillActivity.class);
            startActivity(createBillIntent);
            return true;
        }
        */

        return super.onOptionsItemSelected(item);
    }

}
