package com.moebli.android.frontend;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.webkit.JavascriptInterface;

import com.moebli.android.backend.billing.balance.ContactBalanceService;
import com.moebli.android.backend.item.ItemService;
import com.moebli.android.backend.session.LoginResponseDTO;
import com.moebli.android.backend.user.SignUpRequestDTO;
import com.moebli.android.backend.user.SignUpService;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.bill.ContactBalancesActivity;
import com.moebli.android.frontend.util.RemoteCallError;

/**
 * Created by sushant on 10/07/15.
 */
public class WebAppInterface {

    private static String C = WebAppInterface.class.getName();

    Context mContext;

    private String recaptchaChallengeField;

    private String recaptchaResponseField;

    private String gRecaptchaResponse;

    private SignUpRequestDTO requestDTO;

    private SignUpService signUpService = new SignUpService();

    /** Instantiate the interface and set the context */
    public WebAppInterface(Context c) {
        mContext = c;
    }

    /** capture and submit signup form */
    @JavascriptInterface
    public void submit(String gRecaptchaResponse) {
        Log.d("WebView", "recaptcha :" + gRecaptchaResponse);
        this.gRecaptchaResponse = gRecaptchaResponse;
      //  requestDTO.setgRecaptchaResponse(gRecaptchaResponse);
      //  signUpService.signUp(new SignUpCallback(), requestDTO);
    }

    public class SignUpCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            /* TODO pass activity to this class
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }
            */

            LoginResponseDTO response = (LoginResponseDTO) o;
            if (response.getErrorList().size() > 0) {

            }
            else{
                ContactBalanceService.getInstance().getContactBalances(new GetContactsCallback());
                ItemService.getInstance().getItems(new GetItemsCallback());
            }
        }
    }

    public class GetContactsCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            /* TODO pass activity to this class
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }
            */

            Intent billsByConnIntent = new Intent(mContext, ContactBalancesActivity.class);
            mContext.startActivity(billsByConnIntent);
        }
    }

    public class GetItemsCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            /* TODO pass activity to this class
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }
            */
        }
    }

    public String getRecaptchaChallengeField() {
        return recaptchaChallengeField;
    }

    public String getRecaptchaResponseField() {
        return recaptchaResponseField;
    }

    public SignUpRequestDTO getRequestDTO() {
        return requestDTO;
    }

    public void setRequestDTO(SignUpRequestDTO requestDTO) {
        this.requestDTO = requestDTO;
    }

    public String getgRecaptchaResponse() {
        return gRecaptchaResponse;
    }
}
