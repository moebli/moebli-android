package com.moebli.android.frontend.bill;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.moebli.android.backend.billing.balance.BalanceDTO;
import com.moebli.android.backend.billing.balance.ContactBalanceDTO;
import com.moebli.android.backend.billing.balance.ContactBalanceService;
import com.moebli.android.backend.billing.bill.CreateBillResponseDTO;
import com.moebli.android.backend.enums.SharedPrefs;
import com.moebli.android.backend.payment.RecordPaymentResponseDTO;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.gcm.GCMRegistrationService;
import com.moebli.android.frontend.payment.RecordPaymentFragment;

import java.util.List;

public class ContactActivity extends AppCompatActivity implements CreateContactBillFragment
        .CreateBillListener, RecordPaymentFragment.RecordPaymentListener {

    // constants
    public static final String CONTACT_ACCOUNT_UUID = "contact_account_uuid";

    // class variables
    private String mContactUuid;
    private Fragment mContactBillsFragment = new BillListFragment();
    private Fragment mCreateContactBillFragment = new CreateContactBillFragment();
    private Fragment mRecordPaymentFragment = new RecordPaymentFragment();
    private FragmentPagerAdapter mAdapterViewPager;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent connBills = getIntent();
        mContactUuid = connBills.getStringExtra(CONTACT_ACCOUNT_UUID);

        setContentView(R.layout.contact_activity);
        setHeader();
    }

    // activity becomes visible from hidden or from partially visible
    @Override
    protected void onResume() {
        super.onResume();
        setBalanceHeader();
    }

    private void setHeader() {
        // toolbar as actionbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // balance
        actionBar.setDisplayShowCustomEnabled(true);
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context
                .LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.toolbar_balance_badge, null);
        actionBar.setCustomView(v);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.contact_tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string
                .label_title_bills)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.
                label_title_new_bill)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string
                .label_title_payment)));

        // https://github.com/codepath/android_guides/wiki/ViewPager-with-FragmentPagerAdapter
        mViewPager = (ViewPager) findViewById(R.id.contact_view_pager);
        mAdapterViewPager = new ContactPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapterViewPager);
        // following is needed to reflect new or updated bill after bill is created or updated
        mViewPager.addOnPageChangeListener(new ContactPageChangeListener());
        tabLayout.setupWithViewPager(mViewPager);
    }

    private void setBalanceHeader() {
        ContactBalanceDTO contactBalanceDTO = ContactBalanceService.getInstance().getContactBalanceDTO
                (mContactUuid);
        if (contactBalanceDTO == null)
            return;

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(contactBalanceDTO.getAccountName());

        List<BalanceDTO> balanceDTOs = contactBalanceDTO.getBalanceList();
        if (balanceDTOs != null && balanceDTOs.size() > 0) {
            TextView totalBalanceView = (TextView) findViewById(R.id.action_bar_balance_text);
            totalBalanceView.setText(getBalanceHtml(balanceDTOs));
        }
        else {
            LinearLayout totalBalanceContainer = (LinearLayout) findViewById(R.id
                    .toolbar_balance_container);
            totalBalanceContainer.setVisibility(View.GONE);
        }
    }

    public static Spanned getBalanceHtml(List<BalanceDTO> balanceDTOs) {
        StringBuilder buf = new StringBuilder();

        // if multiple currencies and at least one non-zero balance, then skip zero balances
        boolean hasNonZero = false;
        for (BalanceDTO balanceDTO : balanceDTOs) {
            if (balanceDTO.getBalance() != 0) {
                hasNonZero = true;
                break;
            }
        }
        boolean skipZero = (balanceDTOs.size() > 1 && hasNonZero);

        boolean first = true;
        for (BalanceDTO balanceDTO : balanceDTOs) {
            if (skipZero && balanceDTO.getBalance() == 0)
                continue;
            if (!first)
                buf.append(", ");
            String color = balanceDTO.getBalance() < 0 ? "ff0000" : "006622";
            buf.append("<font color='#" + color + "'>").append(balanceDTO.getBalanceFormatted
                    ()).append("</font>");
            first = false;
        }
        return Html.fromHtml(buf.toString());
    }

    @Override
    public void onCreateBillSuccess(CreateBillResponseDTO responseDTO) {
        setBalanceHeader();
    }

    @Override
    public void onRecordPaymentSuccess(RecordPaymentResponseDTO responseDTO) {
        setBalanceHeader();
    }

    class ContactPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    // note: normally it is not a good idea to call lifecycle method
                    mContactBillsFragment.onResume();
                    break;
                case 1:
                    // throws exception when orientation changed because onCreate not called
                    // mCreateContactBillFragment.onResume();
                    // TODO basically event item array adapter should be refreshed here
                    // wait till UI is finalized; may add a floating button
                    break;
                case 2:
                    // throws exception when orientation changed because onCreate not called
                    // mRecordPaymentFragment.onResume();
                    // TODO basically event item array adapter should be refreshed here
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    }

    // https://github.com/codepath/android_guides/wiki/ViewPager-with-FragmentPagerAdapter
    public class ContactPagerAdapter extends FragmentPagerAdapter {
        private int NUM_ITEMS = 3;

        public ContactPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return mContactBillsFragment;
                case 1:
                    return mCreateContactBillFragment;
                case 2:
                    return mRecordPaymentFragment;
                default:
                    return null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.label_title_bills);
                case 1:
                    return getResources().getString(R.string.label_title_new_bill);
                case 2:
                    return getResources().getString(R.string.label_title_payment);
                default:
                    return null;
            }
        }

    }

    public String getContactUuid() {
        return mContactUuid;
    }

    public void setCurrentItem(int i) {
        mViewPager.setCurrentItem(i, false);
    }
}
