package com.moebli.android.frontend.payment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.moebli.android.backend.billing.balance.ContactBalanceDTO;
import com.moebli.android.backend.billing.balance.ContactBalanceService;
import com.moebli.android.backend.billing.balance.ItemBalanceService;
import com.moebli.android.backend.billing.balance.MonthlyBalanceService;
import com.moebli.android.backend.billing.bill.BillDTO;
import com.moebli.android.backend.billing.billlist.ContactBillsService;
import com.moebli.android.backend.billing.billlist.ItemBillsService;
import com.moebli.android.backend.billing.billlist.MonthlyBillsService;
import com.moebli.android.backend.enums.Currency;
import com.moebli.android.backend.item.ItemService;
import com.moebli.android.backend.payment.RecordPaymentRequestDTO;
import com.moebli.android.backend.payment.RecordPaymentResponseDTO;
import com.moebli.android.backend.payment.RecordPaymentService;
import com.moebli.android.backend.session.SessionService;
import com.moebli.android.backend.util.ErrorDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.backend.util.JsonUtil;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.bill.ContactActivity;
import com.moebli.android.frontend.bill.CreateBillFormFragment;
import com.moebli.android.frontend.bill.CreateBillSuccessFragment;
import com.moebli.android.frontend.bill.UpdateBillActivity;
import com.moebli.android.frontend.util.RemoteCallError;
import com.moebli.android.frontend.widget.EditTextAutoComplete;

import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.List;

// https://github.com/danilao/fragments-viewpager-example
public class RecordPaymentFragment extends Fragment {


    // ui elements
    private ScrollView mFormLayout;
    private Spinner mCurrencySpinner;
    private RadioGroup recordPaymentRadioGroup;
    private EditTextAutoComplete mItemEdit;
    private TextView mAmount0Edit;
    private RadioButton mIPaidContact;
    private RadioButton mContactPaidMe;
    private RadioGroup mXPaidYRadioGroup;
    // success section
    private LinearLayout mSuccessLayout;
    private TextView mSuccessMessage;
    private TextView mBlockedMessage;
    private Button mAddAnother;
    private View mBillRow;

    // class variables
    private String mContactUuid;
    private ContactBalanceDTO contactBalanceDTO;
    private RecordPaymentListener mRecordPaymentListener;
    private static RecordPaymentFragment mRecordPaymentFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        mContactUuid = ((ContactActivity) getActivity()).getContactUuid();
        contactBalanceDTO = ContactBalanceService.getInstance().getContactBalanceDTO(mContactUuid);
        View v = inflater.inflate(R.layout.record_payment_fragment, container, false);

        // create bill form
        mFormLayout = (ScrollView) v.findViewById(R.id.record_payment_form);
        mItemEdit = (EditTextAutoComplete) v.findViewById(R.id.item_edit);
        mXPaidYRadioGroup = (RadioGroup) v.findViewById(R.id.xpaidy_radio_group);
        mIPaidContact = (RadioButton) v.findViewById(R.id.paid_to_conn_radio);
        mIPaidContact.setText(MessageFormat.format(getResources().getString(R.string
                .label_i_paid_contact), contactBalanceDTO.getAccountName()));
        mContactPaidMe = (RadioButton) v.findViewById(R.id.paid_to_me_radio);
        mContactPaidMe.setText(MessageFormat.format(getResources().getString(R.string
                .label_contact_paid_me), contactBalanceDTO.getAccountName()));
        mAmount0Edit = (TextView) v.findViewById(R.id.amount0_edit);
        Button payButton = (Button) v.findViewById(R.id.record_payment_button);
        payButton.setOnClickListener(new RecordPaymentClickListener());

        // currency
        mCurrencySpinner = (Spinner) v.findViewById(R.id.currency);
        List<Currency> currencies = Currency.getCurrencyCodeEnumList();
        ArrayAdapter<Currency> currencyArrayAdapter = new CreateBillFormFragment
                .CurrencySpinnerAdapter(getActivity(), R.layout.currency_row, currencies);
        mCurrencySpinner.setAdapter(currencyArrayAdapter);
        String sessionCurrency = SessionService.getInstance().getCurrencyCode(getActivity());
        for (int i = 0; i < currencies.size(); i++) {
            if (currencies.get(i).getCode().equals(sessionCurrency)) {
                mCurrencySpinner.setSelection(i);
                break;
            }
        }

        // success section
        mSuccessLayout = (LinearLayout) v.findViewById(R.id.record_payment_success);
        mSuccessMessage = (TextView) v.findViewById(R.id.success_text);
        mBlockedMessage = (TextView) v.findViewById(R.id.blocked_message);
        mBillRow = v.findViewById(R.id.bill_row);
        mAddAnother = (Button) v.findViewById(R.id.button_add_another);
        mAddAnother.setOnClickListener(new AddAnotherClickListener());

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mRecordPaymentListener = (RecordPaymentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement " +
                    "RecordPaymentListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        showForm();
    }

    private void showForm() {
        mFormLayout.setVisibility(View.VISIBLE);
        mSuccessLayout.setVisibility(View.GONE);
        mItemEdit.setText("");
        mAmount0Edit.setText("");
        ItemService.getInstance().getItems(new GetItemsCallback());

        MTracker.getInstance().GA(Screen.create_cash_payment, Category.create_cash_payment, Action.view);
    }

    public class GetItemsCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            ArrayAdapter<String> itemAdapter = new ArrayAdapter(getActivity(), android.R.layout
                    .simple_dropdown_item_1line, ItemService.getInstance()
                    .getAccessibleItemStrings(mContactUuid));
            mItemEdit.setAdapter(itemAdapter);
        }
    }

    private class RecordPaymentClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            MTracker.getInstance().GA(Screen.create_cash_payment, Category.create_cash_payment, Action
                    .submit);

            String itemName = mItemEdit.getText().toString();
            String amount = mAmount0Edit.getText().toString();

            if (StringUtils.isBlank(itemName)) {
                mItemEdit.setError(MessageFormat.format(getResources().getString(R.string.error_required_field), getResources().getString(R.string.label_event)));
                MTracker.getInstance().GA(Screen.create_cash_payment, Category
                        .create_cash_payment, Action.error, "empty_item");
            }
            if (StringUtils.isBlank(amount)) {
                mAmount0Edit.setError(MessageFormat.format(getResources().getString(R.string.error_required_field), getResources().getString(R.string.label_amount)));
                MTracker.getInstance().GA(Screen.create_cash_payment, Category
                        .create_cash_payment, Action.error, "empty_amount");
            }
            if (StringUtils.isBlank(itemName) || StringUtils.isBlank(amount)) {
                return;
            }

            RecordPaymentRequestDTO requestDTO = new RecordPaymentRequestDTO();
            requestDTO.setItemName(itemName);
            requestDTO.setItemUuid(ItemService.getInstance().getItemUuid(itemName));
            requestDTO.setAmount0(mAmount0Edit.getText().toString());
            requestDTO.setContact0(contactBalanceDTO.getEmail());
            requestDTO.setCurrencyCode(mCurrencySpinner.getSelectedItem().toString());
            //TODO check how paymentMethod and xPaidY are used.

            int selectedPayTo = mXPaidYRadioGroup.getCheckedRadioButtonId();
            String getMoney = selectedPayTo == mContactPaidMe.getId() ? "-" : "";
            requestDTO.setAmount0(getMoney + amount);

            RecordPaymentService.getInstance().recordCashPayment(new RecordPaymentCallback(),
                    requestDTO);
        }
    }

    private class RecordPaymentCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            RecordPaymentResponseDTO responseDTO = (RecordPaymentResponseDTO) o;
            if (responseDTO.getErrorList().size() > 0) {
                for (ErrorDTO error : responseDTO.getErrorList()) {
                    if ("itemName".equals(error.getFieldName()))
                        mItemEdit.setError(error.getMessage());
                    if ("amount0".equals(error.getFieldName()))
                        mAmount0Edit.setError(error.getMessage());
                    MTracker.getInstance().GA(Screen.create_cash_payment, Category.create_cash_payment,
                            Action.error, error.getFieldName());
                }
            } else {
                updateCache(responseDTO);
                MTracker.getInstance().GA(Screen.create_cash_payment, Category
                        .create_cash_payment, Action.success, 0l + responseDTO.getBillList().size
                        ());

                // on success, notify parent activity
                mRecordPaymentListener.onRecordPaymentSuccess(responseDTO);

                mFormLayout.setVisibility(View.GONE);
                mSuccessLayout.setVisibility(View.VISIBLE);

                CreateBillSuccessFragment.showMessage(responseDTO.getBillList(), responseDTO
                        .getBlockedContactList(), responseDTO.getSuccess(), mSuccessMessage,
                        mBlockedMessage, mBillRow, getResources(), new BillClickListener
                                (responseDTO.getBillList()));
            }
        }
    }

    public static void updateCache(RecordPaymentResponseDTO responseDTO) {
        ContactBalanceService.getInstance().addNewContacts(responseDTO.getNewContactList());
        ItemService.getInstance().addOrUpdateItem(responseDTO.getNewItem());
        ContactBalanceService.getInstance().addOrUpdateContactBalances(responseDTO
                .getContactBalanceList());
        MonthlyBalanceService.getInstance().addOrUpdateMonthlyBalances(responseDTO
                .getMonthlyBalanceList());
        ItemBalanceService.getInstance().addOrUpdateItemBalances(responseDTO.getItemBalanceList());
        ContactBillsService.getInstance().addNewBills(responseDTO.getBillList());
        MonthlyBillsService.getInstance().addNewBills(responseDTO.getBillList());
        ItemBillsService.getInstance().addNewBills(responseDTO.getBillList());
    }

    private class BillClickListener implements View.OnClickListener {
        private BillDTO mBillDTO;

        BillClickListener(List<BillDTO> billDTOs) {
            if (billDTOs.size() > 0) mBillDTO = billDTOs.get(0);
        }

        @Override
        public void onClick(View view) {
            Intent updateBillIntent = new Intent(getActivity(), UpdateBillActivity.class);
            updateBillIntent.putExtra(UpdateBillActivity.BILL_DTO, JsonUtil.getObjectToJSON
                    (mBillDTO));
            // send a flag indicating bill update from success page
            updateBillIntent.putExtra(UpdateBillActivity.IS_CREATE_SUCCESS, true);
            startActivity(updateBillIntent);
        }
    }

    private class AddAnotherClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            showForm();
        }
    }

    public interface RecordPaymentListener {
        void onRecordPaymentSuccess(RecordPaymentResponseDTO responseDTO);
    }

    public static RecordPaymentFragment getInstance() {
        if (mRecordPaymentFragment == null) mRecordPaymentFragment = new RecordPaymentFragment();
        return mRecordPaymentFragment;
    }
}
