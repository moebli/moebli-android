package com.moebli.android.frontend.bill;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.moebli.android.backend.billing.balance.BalanceDTO;
import com.moebli.android.backend.billing.balance.ContactBalanceDTO;
import com.moebli.android.backend.billing.balance.ContactBalanceService;
import com.moebli.android.backend.billing.balance.GetContactBalancesResponseDTO;
import com.moebli.android.backend.item.ItemService;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.util.RemoteCallError;

import java.util.List;

/*
 * https://developer.android.com/training/material/lists-cards.html
 * http://stackoverflow.com/questions/24471109/recyclerview-onclick
 */
public class ContactBalancesFragment extends Fragment {

    // ui elements
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public ContactBalancesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.object_list_fragment, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.object_list_recycler);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        ContactBalanceService.getInstance().getContactBalances(new GetContactBalancesCallback());
        ItemService.getInstance().getItems(new GetItemsCallback());
    }

    public class GetContactBalancesCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            // specify an adapter
            GetContactBalancesResponseDTO responseDTO = (GetContactBalancesResponseDTO) o;
            List<ContactBalanceDTO> contactDTOList = ContactBalanceService
                    .getUnblockedContactBalances(responseDTO.getContactBalanceList());
            mAdapter = new ContactListAdapter(contactDTOList);
            mRecyclerView.setAdapter(mAdapter);

            MTracker.getInstance().GA(Screen.contact_balances, Category.contact_balances, Action.view,
                    0l + contactDTOList.size());
        }
    }

    class GetItemsCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }
        }
    }

    private List<ContactBalanceDTO> mContactBalanceDTOList;

    public class ContactListAdapter extends RecyclerView.Adapter<ContactViewHolder> {

        // Provide a suitable constructor (depends on the kind of dataset)
        public ContactListAdapter(List<ContactBalanceDTO> contactDTOList) {
            mContactBalanceDTOList = contactDTOList;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.balance_row,
                    parent, false);
            // set the view's size, margins, paddings and layout parameters

            ContactViewHolder vh = new ContactViewHolder(v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ContactViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            ContactBalanceDTO contactBalanceDTO = mContactBalanceDTOList.get(position);
            holder.mNameText.setText(contactBalanceDTO.getAccountName());

            List<BalanceDTO> balanceDTOs = contactBalanceDTO.getBalanceList();
            if (balanceDTOs.size() == 0)
                return;
            holder.mBalanceText.setText(ContactActivity.getBalanceHtml(balanceDTOs));
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mContactBalanceDTOList.size();
        }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ContactViewHolder extends RecyclerView.ViewHolder implements View
            .OnClickListener {
        public LinearLayout mBalanceRow;
        // each data item is just a string in this case
        public TextView mNameText;
        public TextView mBalanceText;

        public ContactViewHolder(View view) {
            super(view);
            mBalanceRow = (LinearLayout) view.findViewById(R.id.balance_row_layout);
            mNameText = (TextView) view.findViewById(R.id.name_text);
            mBalanceText = (TextView) view.findViewById(R.id.balance_text);

            // taking a simple approach to make the entire row clickable
            // other solutions exist at:
            // http://stackoverflow.com/questions/24471109/
            // http://stackoverflow.com/questions/24885223/
            mBalanceRow.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int itemPosition = getPosition();
            ContactBalanceDTO contactDTO = mContactBalanceDTOList.get(itemPosition);

            Intent contactIntent = new Intent(getActivity(), ContactActivity.class);
            contactIntent.putExtra(ContactActivity.CONTACT_ACCOUNT_UUID, contactDTO.getAccountUuid());
            startActivity(contactIntent);
        }

    }

}
