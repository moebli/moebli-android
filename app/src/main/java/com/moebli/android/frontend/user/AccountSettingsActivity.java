package com.moebli.android.frontend.user;

import android.support.v4.app.Fragment;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.SingleFragmentWithBackActivity;

public class AccountSettingsActivity extends SingleFragmentWithBackActivity {

    @Override
    protected Fragment createFragment() {
        return new AccountSettingsFragment();
    }

    @Override
    protected int getActivityTitle() {
        return R.string.label_title_acct_settings;
    }
}
