package com.moebli.android.frontend.bill;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.moebli.android.backend.billing.balance.BalanceDTO;
import com.moebli.android.backend.billing.balance.ItemBalanceDTO;
import com.moebli.android.backend.billing.balance.ItemBalanceService;
import com.moebli.android.frontend.R;

import java.util.List;

public class ItemBillsActivity extends AppCompatActivity {

    public static final String ITEM_UUID = "item_uuid";

    // data variables
    private String mItemUuid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent connBills = getIntent();
        mItemUuid = connBills.getStringExtra(ITEM_UUID);

        setContentView(R.layout.object_list_activity);

        // toolbar as actionbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // balance
        actionBar.setDisplayShowCustomEnabled(true);
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context
                .LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.toolbar_balance_badge, null);
        actionBar.setCustomView(v);

        setBalanceHeader();

        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.create_bill_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent createBillIntent = new Intent(getApplicationContext(), CreateBillActivity
                        .class);
                startActivity(createBillIntent);
            }
        });
        */

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.object_list_container);

        if (fragment == null) {
            fragment = new BillListFragment();
            fm.beginTransaction().add(R.id.object_list_container, fragment)
                    .commit();
        }
    }

    // activity becomes visible from hidden or from partially visible
    @Override
    protected void onResume() {
        super.onResume();
        setBalanceHeader();
    }

    private void setBalanceHeader() {
        ItemBalanceDTO itemBalanceDTO = ItemBalanceService.getInstance().getItemBalanceDTO
                (mItemUuid);
        if (itemBalanceDTO == null)
            return;

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(itemBalanceDTO.getItemName());

        List<BalanceDTO> balanceDTOs = itemBalanceDTO.getBalanceList();
        TextView totalBalanceView = (TextView) findViewById(R.id.action_bar_balance_text);
        totalBalanceView.setText(ContactActivity.getBalanceHtml(balanceDTOs));
    }


    public String getItemUuid() {
        return mItemUuid;
    }

}
