package com.moebli.android.frontend.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.util.SizeConverter;

public class RowLinearLayout extends LinearLayout {

    public RowLinearLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public RowLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RowLinearLayout(Context context) {
        super(context);
        init();
    }

    public void init() {
        // orientation, gravity
        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.TOP);

        // background resource needs to be set before padding
        setBackgroundResource(R.drawable.border_bottom);

        // padding
        int px = SizeConverter.dpToPx(getResources(), 15);
        setPadding(0, px, 0, px);
    }
}
