package com.moebli.android.frontend.user;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.moebli.android.backend.user.ChangePasswordService;
import com.moebli.android.backend.user.GetCurrentUserService;
import com.moebli.android.backend.user.GetCurrentUserResponseDTO;
import com.moebli.android.backend.util.BaseResponseDTO;
import com.moebli.android.backend.util.ErrorDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.util.RemoteCallError;

import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.List;

public class ChangePasswordFragment extends Fragment {

    private static ChangePasswordService changePasswordService = new ChangePasswordService();
    private static GetCurrentUserService getCurrentUserService = new GetCurrentUserService();

    private EditText mPasswordEdit;
    private EditText mNewPassword;
    private Button mButton;
    private TextView mFbUserText;

    public ChangePasswordFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_change_password, container, false);

        mPasswordEdit = (EditText) v.findViewById(R.id.password1);
        mNewPassword = (EditText) v.findViewById(R.id.password2);
        mButton = (Button) v.findViewById(R.id.changePassword);
        mFbUserText = (TextView) v.findViewById(R.id.fb_user_text);

        getCurrentUserService.getCurrentUser(new GetCurrentUserCallback());

        mButton.setOnClickListener(new ButtonClickListener());

        MTracker.getInstance().GA(Screen.update_password, Category.update_password, Action.view);
        return v;
    }

    private class ButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            MTracker.getInstance().GA(Screen.update_password, Category.update_password, Action.submit);

            String oldPasswordEdit = mPasswordEdit.getText().toString();
            String newPasswordEdit = mNewPassword.getText().toString();
            if (StringUtils.isBlank(oldPasswordEdit)||StringUtils.isBlank(newPasswordEdit)) {

                if(StringUtils.isBlank(oldPasswordEdit))
                {
                    String s = MessageFormat.format(getResources().getString(R.string
                            .error_required_field), getResources().getString(R.string
                            .label_old_password));
                    mPasswordEdit.setError(s);
                    MTracker.getInstance().GA(Screen.update_password, Category.update_password, Action
                            .error, "empty_old_password");
                }
                if(StringUtils.isBlank(newPasswordEdit))
                {
                    String s = MessageFormat.format(getResources().getString(R.string
                            .error_required_field), getResources().getString(R.string
                            .label_new_password));
                    mNewPassword.setError(s);
                    MTracker.getInstance().GA(Screen.update_password, Category.update_password,
                            Action.error, "empty_new_password");
                }
                return;
            }

            changePasswordService.changePassword(new ChangePasswordCallback(),oldPasswordEdit,newPasswordEdit);
        }
    }
    public class ChangePasswordCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            BaseResponseDTO responseDTO = (BaseResponseDTO) o;
            List<ErrorDTO> errors = responseDTO.getErrorList();
            if (errors.size() > 0) {
                for(ErrorDTO error: errors) {
                    if (error.getFieldName().equals("oldPassword")) {
                        mPasswordEdit.setError(error.getMessage());
                    }
                    if (error.getFieldName().equals("newPassword")) {
                        mNewPassword.setError(error.getMessage());
                    }
                    MTracker.getInstance().GA(Screen.update_password, Category.update_password, Action.error,
                            error.getFieldName());
                }
                return;
            }
            else {
                mPasswordEdit.setText("");
                mNewPassword.setText("");
                Snackbar.make(getActivity().findViewById(android.R.id.content), responseDTO.getSuccess(), Snackbar.LENGTH_LONG).show();
                MTracker.getInstance().GA(Screen.update_password, Category.update_password,
                        Action.success);
            }
        }
    }
    private class GetCurrentUserCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            GetCurrentUserResponseDTO responseDTO = (GetCurrentUserResponseDTO) o;

            if (StringUtils.isNotBlank(responseDTO.getIdentityProvider())) {
                mPasswordEdit.setVisibility(View.GONE);
                mNewPassword.setVisibility(View.GONE);
                mButton.setVisibility(View.GONE);
                String appName = getResources().getString(R.string.label_app_name);
                mFbUserText.setText(MessageFormat.format(getResources().getString(R.string
                        .info_fb_user_passwd), appName));
                mFbUserText.setVisibility(View.VISIBLE);
                MTracker.getInstance().GA(Screen.update_password, Category.update_password,
                        Action.error, responseDTO.getIdentityProvider());
            }
        }
    }

}
