package com.moebli.android.frontend.user;


import android.support.v4.app.Fragment;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.SingleFragmentWithBackActivity;

public class ChangeNameActivity extends SingleFragmentWithBackActivity {

    @Override
    protected Fragment createFragment() {
        return new ChangeNameFragment();
    }

    protected int getActivityTitle()
    {
        return R.string.action_update_person_info;
    }


}
