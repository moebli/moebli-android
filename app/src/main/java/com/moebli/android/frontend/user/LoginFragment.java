package com.moebli.android.frontend.user;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.moebli.android.backend.billing.balance.ContactBalanceService;
import com.moebli.android.backend.enums.Constants;
import com.moebli.android.backend.enums.SharedPrefs;
import com.moebli.android.backend.item.ItemService;
import com.moebli.android.backend.session.LoginResponseDTO;
import com.moebli.android.backend.session.LoginService;
import com.moebli.android.backend.session.SessionService;
import com.moebli.android.backend.util.ErrorDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.generic.SplashScreenActivity;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.bill.ContactBalancesActivity;
import com.moebli.android.frontend.util.RemoteCallError;
import com.moebli.android.frontend.widget.TextViewLink;

import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;

public class LoginFragment extends Fragment {

    // ui elements
    private static EditText mPhoneOrEmailEdit;
    private static EditText mPasswordEdit;

    // service classes
    private static LoginService loginService = new LoginService();

    public LoginFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.login_fragment, container, false);

        mPhoneOrEmailEdit = (EditText) v.findViewById(R.id.phone_or_email_edit_text);
        mPasswordEdit = (EditText) v.findViewById(R.id.password_edit_text);
        Button button = (Button) v.findViewById(R.id.button);
        button.setOnClickListener(new LoginButtonOnClickListener());

        SharedPreferences sharedPrefs = getActivity().getSharedPreferences(Constants.PREF_FILE,
                Context.MODE_PRIVATE);
        String loginId = sharedPrefs.getString(SharedPrefs.LOGIN_ID.name(), null);
        if (loginId != null)
            mPhoneOrEmailEdit.setText(loginId);

        TextViewLink forgotPasswordText = (TextViewLink) v.findViewById(R.id.btnForgot);
        forgotPasswordText.setOnClickListener(new ForgotPasswordClickListener());

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        MTracker.getInstance().GA(Screen.login, Category.login, Action.view);
    }

    class LoginButtonOnClickListener implements Button.OnClickListener {

        @Override
        public void onClick(View view) {
            MTracker.getInstance().GA(Screen.login, Category.login, Action.submit);
            String phoneOrEmail = mPhoneOrEmailEdit.getText().toString().trim();
            String password = mPasswordEdit.getText().toString();

            // this is a hack to clear shared preferences while testing
            if (StringUtils.isBlank(phoneOrEmail) && password.equals("fc")) {
                SharedPreferences sharedPrefs = getActivity().getSharedPreferences(Constants
                        .PREF_FILE, Context.MODE_PRIVATE);
                sharedPrefs.edit().clear().commit();

                Intent splashScreenIntent = new Intent(getActivity(), SplashScreenActivity.class);
                startActivity(splashScreenIntent);
                return;
            }

            // focus on topmost error
            boolean focusRequested = false;
            if (StringUtils.isBlank(phoneOrEmail)) {
                mPhoneOrEmailEdit.setError(MessageFormat.format(getResources().getString(R.string
                        .error_required_field), getResources().getString(R.string
                        .label_email_address)));
                mPhoneOrEmailEdit.requestFocus();
                focusRequested = true;
                MTracker.getInstance().GA(Screen.login, Category.login, Action.error,
                        "empty_id");
            }
            if (StringUtils.isBlank(password)) {
                mPasswordEdit.setError(MessageFormat.format(getResources().getString(R.string
                        .error_required_field), getResources().getString(R.string.label_password)));
                if (! focusRequested)
                    mPasswordEdit.requestFocus();
                MTracker.getInstance().GA(Screen.login, Category.login, Action.error,
                        "empty_password");
            }
            if (StringUtils.isBlank(phoneOrEmail) || StringUtils.isBlank(password)) {
                return;
            }

            loginService.login(new LoginCallback(), phoneOrEmail, password);
        }
    }

    public class LoginCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            LoginResponseDTO response = (LoginResponseDTO) o;
            if (response.getErrorList().size() > 0) {
                // focus on topmost error
                boolean focusRequested = false;
                for (ErrorDTO error : response.getErrorList()) {
                    if (StringUtils.isBlank(error.getFieldName())) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder
                                (getActivity(), R.style.AlertDialog);
                        alertDialogBuilder.setMessage(error.getMessage());
                        alertDialogBuilder.setPositiveButton(getActivity().getResources().getString(R.string
                                .action_ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                    if ("phoneOrEmail".equals(error.getFieldName())) {
                        mPhoneOrEmailEdit.setError(error.getMessage());
                        mPhoneOrEmailEdit.requestFocus();
                        focusRequested = true;
                    }
                    if ("password".equals(error.getFieldName())) {
                        mPasswordEdit.setError(error.getMessage());
                        if (! focusRequested)
                            mPasswordEdit.requestFocus();
                    }
                    MTracker.getInstance().GA(Screen.login, Category.login, Action.error,
                            error.getFieldName());
                }
            } else {
                SessionService.getInstance().postLoginSessionUpdate(getActivity(),
                        mPhoneOrEmailEdit.getText().toString(), response);
                MTracker.getInstance().GA(Screen.login, Category.login, Action.success);

                Intent contactBalancesIntent = new Intent(getActivity(), ContactBalancesActivity.class);
                startActivity(contactBalancesIntent);
            }
        }
    }

    private class ForgotPasswordClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent forgotPasswordIntent = new Intent(getActivity(), InitPasswordResetActivity.class);
            startActivity(forgotPasswordIntent);
        }
    }
}
