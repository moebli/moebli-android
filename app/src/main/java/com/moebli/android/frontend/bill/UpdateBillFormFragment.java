package com.moebli.android.frontend.bill;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.moebli.android.backend.billing.balance.ContactBalanceDTO;
import com.moebli.android.backend.billing.balance.ContactBalanceService;
import com.moebli.android.backend.billing.balance.ItemBalanceService;
import com.moebli.android.backend.billing.balance.MonthlyBalanceService;
import com.moebli.android.backend.billing.bill.BillDTO;
import com.moebli.android.backend.billing.bill.DeleteBillRequestDTO;
import com.moebli.android.backend.billing.bill.DeleteBillResponseDTO;
import com.moebli.android.backend.billing.bill.DeleteBillService;
import com.moebli.android.backend.billing.bill.UpdateBillRequestDTO;
import com.moebli.android.backend.billing.bill.UpdateBillResponseDTO;
import com.moebli.android.backend.billing.bill.UpdateBillService;
import com.moebli.android.backend.billing.bill.UpdateMemoRequestDTO;
import com.moebli.android.backend.billing.bill.UpdateMemoResponseDTO;
import com.moebli.android.backend.billing.bill.UpdateMemoService;
import com.moebli.android.backend.billing.billlist.ContactBillsService;
import com.moebli.android.backend.billing.billlist.ItemBillsService;
import com.moebli.android.backend.billing.billlist.MonthlyBillsService;
import com.moebli.android.backend.item.ItemService;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.backend.session.SessionService;
import com.moebli.android.backend.util.ErrorDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.util.RemoteCallError;
import com.moebli.android.frontend.widget.EditTextBase;
import com.moebli.android.frontend.widget.TextViewBase;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UpdateBillFormFragment extends Fragment {

    // class variables
    private BillDTO mBillDTO;
    private boolean mIsCreateSuccess;

    // ui elements
    private AutoCompleteTextView mItemEdit;
    private RadioGroup mPayToRadioGroup;
    private RadioButton mPayToMeRadio;
    private RadioButton mPayToContactRadio;
    private EditText mAmountEdit;
    private EditText mBillDateEdit;
    private EditText mMemoEdit;
    private TextView mCurrency;

    public UpdateBillFormFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.update_bill_form_fragment, container, false);

        mBillDTO = ((UpdateBillActivity) getActivity()).getBillDTO();
        mIsCreateSuccess = ((UpdateBillActivity) getActivity()).isCreateSuccess();
        ContactBalanceDTO contactBalanceDTO = ContactBalanceService.getInstance()
                .getContactBalanceDTO(mBillDTO.getContactAccountUuid());

        // item
        mItemEdit = (AutoCompleteTextView) v.findViewById(R.id.item_edit);
        ItemService.getInstance().getItems(new GetItemsCallback());
        mItemEdit.setText(mBillDTO.getItemName());

        // pay to
        mPayToRadioGroup = (RadioGroup) v.findViewById(R.id.pay_to_radio_group);
        mPayToMeRadio = (RadioButton) v.findViewById(R.id.pay_to_me_radio);
        mPayToContactRadio = (RadioButton) v.findViewById(R.id.pay_to_contact_radio);
        if (mBillDTO.getRecordType().equals("bill")) {
            mPayToMeRadio.setText(MessageFormat.format(getResources().getString(R.string
                    .label_contact_owes_me), contactBalanceDTO.getAccountName()));
            mPayToContactRadio.setText(MessageFormat.format(getResources().getString(R.string
                    .label_i_owe_contact), contactBalanceDTO.getAccountName()));
        } else {
            mPayToMeRadio.setText(MessageFormat.format(getResources().getString(R.string
                    .label_i_paid_contact), contactBalanceDTO.getAccountName()));
            mPayToContactRadio.setText(MessageFormat.format(getResources().getString(R.string
                    .label_contact_paid_me), contactBalanceDTO.getAccountName()));
        }
        if (mBillDTO.getPayTo().equals("contact")) {
            mPayToContactRadio.setChecked(true);
        } else {
            mPayToMeRadio.setChecked(true);
        }

        // amount
        mAmountEdit = (EditTextBase) v.findViewById(R.id.amount_edit);
        mAmountEdit.setText(mBillDTO.getAmount());
        mCurrency = (TextViewBase) v.findViewById(R.id.currency);
        mCurrency.setText(mBillDTO.getCurrencySymbol());

        // bill date
        mBillDateEdit = (EditTextBase) v.findViewById(R.id.bill_date_edit);
        mBillDateEdit.setText(mBillDTO.getDate());
        mBillDateEdit.setFocusable(false);
        if (mBillDTO.isOwner() && mBillDTO.getProvider() == null) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat(SessionService.getInstance()
                    .getJavaDataFormat(getActivity()));
            try {
                Date d = format.parse(mBillDTO.getDate());
                cal.setTime(d);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new
                    OnBillDateSetListener(), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal
                    .get(Calendar.DAY_OF_MONTH));
            mBillDateEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    datePickerDialog.show();
                }
            });
        }

        // memo
        mMemoEdit = (EditTextBase) v.findViewById(R.id.memo_edit);
        mMemoEdit.setText(mBillDTO.getInternalNotes());

        Button updateBillButton = (Button) v.findViewById(R.id.update_bill_button);
        updateBillButton.setOnClickListener(new UpdateBillButtonClickListener());

        Button deleteBillButton = (Button) v.findViewById(R.id.delete_bill_button);
        deleteBillButton.setOnClickListener(new DeleteBillButtonClickListener());

        if (!mBillDTO.isOwner()) {
            mItemEdit.setFocusable(false);
            mPayToMeRadio.setEnabled(false);
            mPayToContactRadio.setEnabled(false);
            mAmountEdit.setFocusable(false);
            deleteBillButton.setVisibility(View.GONE);
            ViewGroup.MarginLayoutParams layout = (ViewGroup.MarginLayoutParams) updateBillButton
                    .getLayoutParams();
            layout.setMargins(0, 0, 0, 0);
            updateBillButton.setText(getResources().getString(R.string.action_update_memo));
        }

        if (mBillDTO.getProvider() != null) {
            mPayToMeRadio.setEnabled(false);
            mPayToContactRadio.setEnabled(false);
            mAmountEdit.setFocusable(false);
            deleteBillButton.setVisibility(View.GONE);
            ViewGroup.MarginLayoutParams layout = (ViewGroup.MarginLayoutParams) updateBillButton
                    .getLayoutParams();
            layout.setMargins(0, 0, 0, 0);
        }

        trackUpdate(mBillDTO, Action.view, null);

        return v;
    }

    private void trackUpdate(BillDTO mBillDTO, Action action, String fieldName) {
        if (! mBillDTO.isOwner()) {
            MTracker.getInstance().GA(Screen.update_memo, Category.update_memo, action, fieldName);
            return;
        }
        if (mBillDTO.getRecordType().equals("bill")) {
            MTracker.getInstance().GA(Screen.update_bill, Category.update_bill, action, fieldName);
            return;
        }
        if (mBillDTO.getProvider() == null) {
            MTracker.getInstance().GA(Screen.update_cash_payment, Category.update_cash_payment,
                    action, fieldName);
            return;
        }
        MTracker.getInstance().GA(Screen.update_provider_payment, Category
                .update_provider_payment, action, mBillDTO.getProvider() + " " + fieldName);
    }

    private void trackDelete(BillDTO mBillDTO, Action action, String fieldName) {
        if (mBillDTO.getRecordType().equals("bill")) {
            MTracker.getInstance().GA(Screen.delete_bill, Category.delete_bill, action, fieldName);
            return;
        }
        MTracker.getInstance().GA(Screen.delete_cash_payment, Category.delete_cash_payment,
                action, fieldName);
    }

    public class GetItemsCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            ArrayAdapter<String> itemAdapter = new ArrayAdapter(getActivity(), android.R.layout
                    .simple_dropdown_item_1line, ItemService.getInstance()
                    .getAccessibleItemStrings(mBillDTO.getContactAccountUuid()));
            mItemEdit.setAdapter(itemAdapter);
        }
    }

    public class OnBillDateSetListener implements DatePickerDialog.OnDateSetListener {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
            Calendar cal = Calendar.getInstance();
            cal.set(year, monthOfYear, dayOfMonth);
            SimpleDateFormat format = new SimpleDateFormat(SessionService.getInstance()
                    .getJavaDataFormat(getActivity()));
            String dateStr = format.format(cal.getTime());
            mBillDateEdit.setText(dateStr);
        }
    }

    private class DeleteBillButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), R
                    .style.AlertDialog);

            alertDialogBuilder.setTitle(getResources().getString(R.string
                    .info_title_confirm_delete));
            alertDialogBuilder.setMessage(getResources().getString(R.string.info_delete_bill_q));

            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.action_delete)
                    , new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    DeleteBillRequestDTO requestDTO = new DeleteBillRequestDTO();
                    requestDTO.setBillUuid(mBillDTO.getUuid());
                    DeleteBillService.getInstance().deleteBill(new DeleteBillCallback(),
                            requestDTO);
                    trackDelete(mBillDTO, Action.submit, null);
                }
            });

            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.action_cancel)
                    , new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                    trackDelete(mBillDTO, Action.cancel, null);
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            trackDelete(mBillDTO, Action.confirm, null);
        }
    }

    private class UpdateBillButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (mBillDTO.isOwner()) {
                UpdateBillRequestDTO requestDTO = new UpdateBillRequestDTO();
                requestDTO.setBillUuid(mBillDTO.getUuid());
                requestDTO.setItemName(mItemEdit.getText().toString());
                requestDTO.setAmount(mAmountEdit.getText().toString());
                requestDTO.setBillDate(mBillDateEdit.getText().toString());
                requestDTO.setInternalNotes(mMemoEdit.getText().toString());

                if (mPayToRadioGroup.getCheckedRadioButtonId() == mPayToContactRadio.getId())
                    requestDTO.setPayTo("contact");
                else requestDTO.setPayTo("me");

                UpdateBillService.getInstance().updateBill(new UpdateBillCallback(), requestDTO);
            } else {
                UpdateMemoRequestDTO requestDTO = new UpdateMemoRequestDTO();
                requestDTO.setBillUuid(mBillDTO.getUuid());
                requestDTO.setInternalNotes(mMemoEdit.getText().toString());
                UpdateMemoService.getInstance().updateMemo(new UpdateMemoCallback(), requestDTO);
            }

            trackUpdate(mBillDTO, Action.submit, null);
        }
    }

    public class UpdateBillCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            UpdateBillResponseDTO responseDTO = (UpdateBillResponseDTO) o;
            if (responseDTO.getErrorList().size() > 0) {
                for (ErrorDTO error : responseDTO.getErrorList()) {
                    if ("itemName".equals(error.getFieldName()))
                        mItemEdit.setError(error.getMessage());
                    if ("payTo".equals(error.getFieldName()))
                        Snackbar.make(getActivity().findViewById(android.R.id.content), error
                                .getMessage(), Snackbar.LENGTH_LONG).show();
                    if ("amount".equals(error.getFieldName()))
                        mAmountEdit.setError(error.getMessage());
                    if ("billDate".equals(error.getFieldName()))
                        mBillDateEdit.setError(error.getMessage());
                    if ("internalNotes".equals(error.getFieldName()))
                        mMemoEdit.setError(error.getMessage());
                    trackUpdate(mBillDTO, Action.error, error.getFieldName());
                }
            } else {
                updateCache(responseDTO);
                trackUpdate(mBillDTO, Action.success, null);

                if (mIsCreateSuccess) {
                    // if initiated from success page, go to contact bills page
                    Intent contactIntent = new Intent(getActivity(), ContactActivity.class);
                    contactIntent.putExtra(ContactActivity.CONTACT_ACCOUNT_UUID, responseDTO
                            .getBill().getContactAccountUuid());
                    startActivity(contactIntent);
                } else getActivity().finish(); // goes back to previous view
            }
        }
    }

    private void updateCache(UpdateBillResponseDTO responseDTO) {
        ContactBalanceService.getInstance().addOrUpdateContactBalances(responseDTO
                .getContactBalanceList());
        MonthlyBalanceService.getInstance().addOrUpdateMonthlyBalances(responseDTO
                .getMonthlyBalanceList());
        ItemBalanceService.getInstance().addOrUpdateItemBalances(responseDTO.getItemBalanceList());

        ContactBillsService.getInstance().updateBill(responseDTO.getBill());
        MonthlyBillsService.getInstance().updateBill(responseDTO.getBill());
        ItemBillsService.getInstance().updateBill(responseDTO.getBill());

        ItemService.getInstance().addOrUpdateItem(responseDTO.getItem());

        if (responseDTO.getDeletedItemUuid() != null) {
            ItemService.getInstance().deleteItem(responseDTO.getDeletedItemUuid());
            ItemBalanceService.getInstance().deleteItemBalance(responseDTO.getDeletedItemUuid());
        }

        if (responseDTO.getDeletedMonthYearCode() != null) {
            MonthlyBalanceService.getInstance().deleteMonthlyBalance(responseDTO
                    .getDeletedMonthYearCode());
        }
    }

    class UpdateMemoCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            UpdateMemoResponseDTO responseDTO = (UpdateMemoResponseDTO) o;
            if (responseDTO.getErrorList().size() > 0) {
                for (ErrorDTO error : responseDTO.getErrorList()) {
                    mMemoEdit.setError(error.getMessage());
                    trackUpdate(mBillDTO, Action.error, error.getFieldName());
                }
            } else {
                updateCache(responseDTO);
                trackUpdate(mBillDTO, Action.success, null);

                if (mIsCreateSuccess) {
                    // if initiated from success page, go to contact bills page
                    Intent contactIntent = new Intent(getActivity(), ContactActivity.class);
                    contactIntent.putExtra(ContactActivity.CONTACT_ACCOUNT_UUID, responseDTO
                            .getBill().getContactAccountUuid());
                    startActivity(contactIntent);
                } else getActivity().finish();
            }
        }
    }

    private void updateCache(UpdateMemoResponseDTO responseDTO) {
        ContactBillsService.getInstance().updateBill(responseDTO.getBill());
        MonthlyBillsService.getInstance().updateBill(responseDTO.getBill());
        ItemBillsService.getInstance().updateBill(responseDTO.getBill());
    }

    class DeleteBillCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            DeleteBillResponseDTO responseDTO = (DeleteBillResponseDTO) o;
            if (responseDTO.getErrorList().size() > 0) {
                for (ErrorDTO error : responseDTO.getErrorList()) {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), error
                            .getMessage(), Snackbar.LENGTH_LONG).show();
                    trackDelete(mBillDTO, Action.error, error.getFieldName());
                }
            } else {
                updateCache(responseDTO);
                trackDelete(mBillDTO, Action.success, null);

                if (mIsCreateSuccess) {
                    // if initiated from success page, go to contact bills page
                    Intent contactIntent = new Intent(getActivity(), ContactActivity.class);
                    contactIntent.putExtra(ContactActivity.CONTACT_ACCOUNT_UUID, mBillDTO
                            .getContactAccountUuid());
                    startActivity(contactIntent);
                } else getActivity().finish();
            }
        }
    }

    private void updateCache(DeleteBillResponseDTO responseDTO) {
        ContactBalanceService.getInstance().addOrUpdateContactBalances(responseDTO
                .getContactBalanceList());
        MonthlyBalanceService.getInstance().addOrUpdateMonthlyBalances(responseDTO
                .getMonthlyBalanceList());
        ItemBalanceService.getInstance().addOrUpdateItemBalances(responseDTO.getItemBalanceList());

        ContactBillsService.getInstance().deleteBill(responseDTO.getDeletedBillUuid());
        MonthlyBillsService.getInstance().deleteBill(responseDTO.getDeletedBillUuid());
        ItemBillsService.getInstance().deleteBill(responseDTO.getDeletedBillUuid());

        if (responseDTO.getDeletedItemUuid() != null) {
            ItemService.getInstance().deleteItem(responseDTO.getDeletedItemUuid());
            ItemBalanceService.getInstance().deleteItemBalance(responseDTO.getDeletedItemUuid());
        }

        if (responseDTO.getDeletedMonthYearCode() != null) {
            MonthlyBalanceService.getInstance().deleteMonthlyBalance(responseDTO
                    .getDeletedMonthYearCode());
        }
    }

}
