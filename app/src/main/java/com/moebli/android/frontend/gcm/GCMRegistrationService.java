package com.moebli.android.frontend.gcm;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.moebli.android.backend.enums.Constants;
import com.moebli.android.backend.enums.SharedPrefs;

public class GCMRegistrationService {
    private static String C = GCMRegistrationService.class.getName();
    private static GCMRegistrationService mGCMRegistrationService;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public BroadcastReceiver registerForGCM(final Activity activity) {
        // check if registered on previous login
        SharedPreferences sharedPrefs = activity.getSharedPreferences(Constants.PREF_FILE,
                Context.MODE_PRIVATE);
        boolean sentToken = sharedPrefs.getBoolean(SharedPrefs.SENT_TOKEN_TO_SERVER
                .name(), false);
        if (sentToken)
            return null;

        BroadcastReceiver registrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPrefs = activity.getSharedPreferences(Constants.PREF_FILE,
                        Context.MODE_PRIVATE);
                boolean sentToken = sharedPrefs.getBoolean(SharedPrefs.SENT_TOKEN_TO_SERVER
                        .name(), false);
                if (sentToken) {
                    Toast.makeText(activity, "Token retrieved and sent to server!", Toast
                            .LENGTH_LONG).show();
                } else {
                    Toast.makeText(activity, "An error occurred while either fetching the " +
                            "InstanceID token, sending the fetched token to the server or " +
                            "subscribing to the PubSub topic.!", Toast.LENGTH_LONG).show();
                }
            }
        };

        if (checkPlayServices(activity)) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(activity, RegistrationIntentService.class);
            activity.startService(intent);
        }

        return registrationBroadcastReceiver;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices(Activity activity) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(activity, resultCode,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(C, "This device is not supported.");
                activity.finish();
            }
            return false;
        }
        return true;
    }

    public static GCMRegistrationService getInstance() {
        if (mGCMRegistrationService == null) mGCMRegistrationService = new GCMRegistrationService();
        return mGCMRegistrationService;
    }

    private GCMRegistrationService() {
    }
}
