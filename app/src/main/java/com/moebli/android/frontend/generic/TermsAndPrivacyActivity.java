package com.moebli.android.frontend.generic;

import android.support.v4.app.Fragment;

import com.moebli.android.frontend.R;
import com.moebli.android.frontend.SingleFragmentWithBackActivity;

public class TermsAndPrivacyActivity extends SingleFragmentWithBackActivity {

    @Override
    protected Fragment createFragment() {
        return new TermsAndPrivacyFragment();
    }

    @Override
    protected int getActivityTitle() {
        return R.string.action_link_terms_n_pvc;
    }
}
