package com.moebli.android.frontend.user;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.moebli.android.backend.session.SessionService;
import com.moebli.android.backend.user.SignUpRequestDTO;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.WebAppInterface;

import java.util.HashMap;
import java.util.Map;

import static com.moebli.android.backend.enums.Constants.BASE_URL;

/**
 * Created by sushant on 11/07/15.
 */
public class RecaptchaActivity extends Activity {

    private static SessionService sessionService = SessionService.getInstance();

    private static WebView mRecaptchView;

    private WebAppInterface webAppInterface = new WebAppInterface(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SignUpRequestDTO requestDTO = (SignUpRequestDTO) getIntent().getSerializableExtra("signUpRequestDTO");
        webAppInterface.setRequestDTO(requestDTO);
        setContentView(R.layout.web_view);
        mRecaptchView = (WebView) findViewById(R.id.recaptchaView);
        mRecaptchView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
        WebSettings settings = mRecaptchView.getSettings();
        settings.setJavaScriptEnabled(true);
        mRecaptchView.addJavascriptInterface(webAppInterface, "mRecaptcha");
        Map<String, String> param = new HashMap<String, String>();
        param.put("SessionId", sessionService.getSessionId(this));
        mRecaptchView.loadUrl(BASE_URL + "/recaptcha", param);
    }
}
