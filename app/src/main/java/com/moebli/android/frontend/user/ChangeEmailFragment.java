package com.moebli.android.frontend.user;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.moebli.android.backend.enums.Constants;
import com.moebli.android.backend.enums.SharedPrefs;
import com.moebli.android.backend.user.ChangeEmailService;
import com.moebli.android.backend.user.GetCurrentUserService;
import com.moebli.android.backend.user.GetCurrentUserResponseDTO;
import com.moebli.android.backend.util.BaseResponseDTO;
import com.moebli.android.backend.util.ErrorDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.util.RemoteCallError;

import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.List;

public class ChangeEmailFragment extends Fragment {

    private static ChangeEmailService changeEmailService = new ChangeEmailService();
    private static GetCurrentUserService getCurrentUserService = new GetCurrentUserService();

    private EditText mEmailEdit;
    private Button mButton;

    public ChangeEmailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_change_email, container, false);

        mEmailEdit = (EditText) v.findViewById(R.id.email);
        mButton = (Button) v.findViewById(R.id.button);

        getCurrentUserService.getCurrentUser(new GetCurrentUserCallback());

        mButton.setOnClickListener(new ButtonClickListener());

        MTracker.getInstance().GA(Screen.update_email, Category.update_email, Action.view);
        return v;
    }

    private class ButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            MTracker.getInstance().GA(Screen.update_email, Category.update_email,
                    Action.submit);

            String emailEdit = mEmailEdit.getText().toString();
            if (StringUtils.isBlank(emailEdit)) {
                String s = MessageFormat.format(getResources().getString(R
                        .string.error_required_field), getResources()
                        .getString(R.string.label_email));
                mEmailEdit.setError(s);
                MTracker.getInstance().GA(Screen.update_email, Category.update_email, Action
                        .error, "empty_email");
                return;
            }

            changeEmailService.changeEmail(new ChangeEmailCallback(), emailEdit);
        }
    }

    public class ChangeEmailCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            BaseResponseDTO responseDTO = (BaseResponseDTO) o;
            List<ErrorDTO> errors = responseDTO.getErrorList();
            if (errors.size() > 0) {
                for (ErrorDTO error : errors) {
                    mEmailEdit.setError(error.getMessage());
                    MTracker.getInstance().GA(Screen.update_email, Category.update_email,
                            Action.error, error.getFieldName());
                }
                return;
            }
            else {
                SharedPreferences sharedPrefs = getActivity().getSharedPreferences(Constants
                        .PREF_FILE, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPrefs.edit();
                String loginId = sharedPrefs.getString(SharedPrefs.LOGIN_ID.name(), null);
                if (loginId != null && loginId.contains("@")) {
                    editor.putString(SharedPrefs.LOGIN_ID.name(), mEmailEdit.getText().toString()
                            .trim()).commit();
                }

                Snackbar.make(getActivity().findViewById(android.R.id.content), responseDTO
                        .getSuccess(), Snackbar.LENGTH_LONG).show();
                MTracker.getInstance().GA(Screen.update_email, Category.update_email,
                        Action.success);
            }
        }
    }

    private class GetCurrentUserCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            GetCurrentUserResponseDTO responseDTO = (GetCurrentUserResponseDTO) o;
            mEmailEdit.setText(responseDTO.getEmail());
        }
    }
}
