package com.moebli.android.frontend.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.moebli.android.backend.session.LoginResponseDTO;
import com.moebli.android.backend.session.SessionService;
import com.moebli.android.backend.user.DoPasswordResetService;
import com.moebli.android.backend.util.ErrorDTO;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.bill.ContactBalancesActivity;
import com.moebli.android.frontend.util.RemoteCallError;

import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.List;

public class DoPasswordResetFragment extends Fragment {

    public static final String PHONE_OR_EMAIL = "phone_or_email";

    private EditText mPhoneOrEmailEdit;
    private EditText mResetCodeEdit;
    private EditText mNewPasswordEdit;

    public DoPasswordResetFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.u_do_password_reset_fragment, container, false);
        mPhoneOrEmailEdit = (EditText) v.findViewById(R.id.email_edit);
        mResetCodeEdit = (EditText) v.findViewById(R.id.reset_code_edit);
        mNewPasswordEdit = (EditText) v.findViewById(R.id.new_password_edit);

        Button resetPasswordButton = (Button) v.findViewById(R.id.do_password_reset_button);
        resetPasswordButton.setOnClickListener(new DoPasswordResetOnClickListener());
        mResetCodeEdit.requestFocus();

        Intent doPasswordResetIntent = getActivity().getIntent();
        String phoneOrEmail = doPasswordResetIntent.getStringExtra(PHONE_OR_EMAIL);
        if (StringUtils.isNotBlank(phoneOrEmail))
            mPhoneOrEmailEdit.setText(phoneOrEmail);

        TextView restartLink = (TextView) v.findViewById(R.id.restart_reset_password_link);
        restartLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent initPasswordResetIntent = new Intent(getActivity(), InitPasswordResetActivity
                        .class);
                startActivity(initPasswordResetIntent);
            }
        });

        MTracker.getInstance().GA(Screen.do_password_reset, Category.do_password_reset, Action.view);
        return v;
    }

    private class DoPasswordResetOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            MTracker.getInstance().GA(Screen.do_password_reset, Category.do_password_reset,
                    Action.submit);

            String resetCode = mResetCodeEdit.getText().toString();
            String email = mPhoneOrEmailEdit.getText().toString();
            String newPassword = mNewPasswordEdit.getText().toString();

            // focus on topmost error
            boolean focusRequested = false;
            if (StringUtils.isBlank(resetCode)) {
                String s = MessageFormat.format(getResources().getString(R.string
                        .error_required_field), getResources().getString(R.string.label_password_reset_code));
                mResetCodeEdit.setError(s);
                mResetCodeEdit.requestFocus();
                focusRequested = true;
                MTracker.getInstance().GA(Screen.do_password_reset, Category.do_password_reset, Action
                        .error, "empty_reset_code");
            }
            if (StringUtils.isBlank(email)) {
                String s = MessageFormat.format(getResources().getString(R.string
                        .error_required_field), getResources().getString(R.string.label_email_address));
                mPhoneOrEmailEdit.setError(s);
                if (! focusRequested) {
                    mPhoneOrEmailEdit.requestFocus();
                    focusRequested = true;
                }
                MTracker.getInstance().GA(Screen.do_password_reset, Category.do_password_reset, Action
                        .error, "empty_email");
            }
            if (StringUtils.isBlank(newPassword)) {
                String s = MessageFormat.format(getResources().getString(R.string
                        .error_required_field), getResources().getString(R.string.label_new_password));
                mNewPasswordEdit.setError(s);
                if (! focusRequested)
                    mNewPasswordEdit.requestFocus();
                MTracker.getInstance().GA(Screen.do_password_reset, Category.do_password_reset, Action
                        .error, "empty_new_password");
            }
            if (StringUtils.isBlank(resetCode) || StringUtils.isBlank(email) || StringUtils.isBlank(newPassword)) {
                return;
            }

            DoPasswordResetService.getInstance().doPasswordReset(new DoPasswordResetCallback(),
                    email, resetCode, newPassword);
        }
    }

    private class DoPasswordResetCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            LoginResponseDTO responseDTO = (LoginResponseDTO) o;
            List<ErrorDTO> errors = responseDTO.getErrorList();

            // focus on topmost error
            boolean focusRequested = false;
            if (errors.size() > 0) {
                for (ErrorDTO error : errors) {
                    if ("resetCode".equals(error.getFieldName())) {
                        mResetCodeEdit.setError(error.getMessage());
                        if (! focusRequested) {
                            mResetCodeEdit.requestFocus();
                            focusRequested = true;
                        }
                        MTracker.getInstance().GA(Screen.do_password_reset, Category
                                .do_password_reset, Action.error, error.getFieldName());
                        break;
                    }
                }
                for (ErrorDTO error : errors) {
                    if ("phoneOrEmail".equals(error.getFieldName())) {
                        mPhoneOrEmailEdit.setError(error.getMessage());
                        if (! focusRequested) {
                            mPhoneOrEmailEdit.requestFocus();
                            focusRequested = true;
                        }
                        MTracker.getInstance().GA(Screen.do_password_reset, Category
                                .do_password_reset, Action.error, error.getFieldName());
                        break;
                    }
                }
                for (ErrorDTO error : errors) {
                    if ("newPassword".equals(error.getFieldName())) {
                        mNewPasswordEdit.setError(error.getMessage());
                        if (! focusRequested)
                            mNewPasswordEdit.requestFocus();
                        MTracker.getInstance().GA(Screen.do_password_reset, Category
                                .do_password_reset, Action.error, error.getFieldName());
                        break;
                    }
                }
                return;
            }
            else {
                SessionService.getInstance().postLoginSessionUpdate(getActivity(),
                        mPhoneOrEmailEdit.getText().toString(), responseDTO);
                MTracker.getInstance().GA(Screen.do_password_reset, Category.do_password_reset,
                        Action.success);

                Intent contactBalancesIntent = new Intent(getActivity(), ContactBalancesActivity.class);
                startActivity(contactBalancesIntent);
            }
        }
    }
}
