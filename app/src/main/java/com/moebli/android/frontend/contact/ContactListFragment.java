package com.moebli.android.frontend.contact;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.moebli.android.backend.billing.balance.ContactBalanceDTO;
import com.moebli.android.backend.billing.balance.ContactBalanceService;
import com.moebli.android.backend.billing.balance.GetContactBalancesResponseDTO;
import com.moebli.android.backend.contact.ContactEmailAcceptanceService;
import com.moebli.android.backend.contact.ContactStatusService;
import com.moebli.android.backend.util.HttpClientCallback;
import com.moebli.android.frontend.R;
import com.moebli.android.frontend.analytics.Action;
import com.moebli.android.frontend.analytics.Category;
import com.moebli.android.frontend.analytics.MTracker;
import com.moebli.android.frontend.analytics.Screen;
import com.moebli.android.frontend.util.RemoteCallError;

import java.util.List;

/*
 * https://developer.android.com/training/material/lists-cards.html
 * http://stackoverflow.com/questions/24471109/recyclerview-onclick
 */
public class ContactListFragment extends Fragment {

    // ui elements
    private CheckBox mShowBlockedCheckBox;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public ContactListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.contact_list_fragment, container, false);

        mShowBlockedCheckBox = (CheckBox) v.findViewById(R.id.show_blocked_checkbox);
        mShowBlockedCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactBalanceService.getInstance().getContactBalances(new
                        GetContactBalancesCallback());
            }
        });

        mRecyclerView = (RecyclerView) v.findViewById(R.id.object_list_recycler);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        ContactBalanceService.getInstance().getContactBalances(new GetContactBalancesCallback());

        return v;
    }

    class GetContactBalancesCallback implements HttpClientCallback {
        @Override
        public void handleResponse(Object o) {
            if (o instanceof Throwable) {
                RemoteCallError.show(getActivity(), (Throwable) o);
                return;
            }

            List<ContactBalanceDTO> contactList;
            if (mShowBlockedCheckBox.isChecked())
                contactList = ((GetContactBalancesResponseDTO) o).getContactBalanceList();
            else
                contactList = ContactBalanceService.getInstance().getUnblockedContactBalances();
            mAdapter = new ContactListAdapter(contactList);
            mRecyclerView.setAdapter(mAdapter);

            MTracker.getInstance().GA(Screen.contact_settings, Category.contact_settings, Action
                    .view, 0l + contactList.size());
        }
    }

    private List<ContactBalanceDTO> mContactList;

    public class ContactListAdapter extends RecyclerView.Adapter<ContactViewHolder> {

        // Provide a suitable constructor (depends on the kind of dataset)
        public ContactListAdapter(List<ContactBalanceDTO> contactList) {
            mContactList = contactList;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_row, parent,
                    false);
            // set the view's size, margins, paddings and layout parameters

            ContactViewHolder vh = new ContactViewHolder(v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ContactViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            ContactBalanceDTO contact = mContactList.get(position);
            holder.mContactNameText.setText(contact.getAccountName());
            if ("invited_by".equals(contact.getStatus()))
                holder.mInvitationImage.setVisibility(View.VISIBLE);
            if ("blocked".equals(contact.getStatus()))
                holder.mBlockedImage.setVisibility(View.VISIBLE);
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mContactList.size();
        }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ContactViewHolder extends RecyclerView.ViewHolder implements View
            .OnClickListener, PopupMenu.OnMenuItemClickListener {
        // each data item is just a string in this case
        public TextView mContactNameText;
        public ImageView mInvitationImage;
        public ImageView mBlockedImage;
        private ImageView mOverflowIcon;
        private ContactBalanceDTO mCurrentContact;

        public ContactViewHolder(View view) {
            super(view);
            mContactNameText = (TextView) view.findViewById(R.id.contact_name_text);
            mInvitationImage = (ImageView) view.findViewById(R.id.invitation_image);
            mBlockedImage = (ImageView) view.findViewById(R.id.blocked_image);
            mContactNameText.setOnClickListener(this);
            mOverflowIcon = (ImageView) view.findViewById(R.id.contact_context_menu);
            mOverflowIcon.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int itemPosition = getPosition();
            mCurrentContact = mContactList.get(itemPosition);
            if (view == mOverflowIcon) {
                PopupMenu popup = new PopupMenu(view.getContext(), view);
                popup.inflate(R.menu.contact_settings_context_menu);
                popup.setOnMenuItemClickListener(this);
                Menu m = popup.getMenu();
                switch (mCurrentContact.getStatus()) {
                    case "connected":
                    case "invited":
                        m.removeItem(R.id.action_unblock);
                        m.removeItem(R.id.action_accept_invitation);
                        m.removeItem(R.id.action_decline_invitation);
                        break;
                    case "blocked":
                        m.removeItem(R.id.action_block);
                        m.removeItem(R.id.action_accept_invitation);
                        m.removeItem(R.id.action_decline_invitation);
                        m.removeItem(R.id.action_block_email);
                        m.removeItem(R.id.action_unblock_email);
                        break;
                    case "invited_by":
                        m.removeItem(R.id.action_block);
                        m.removeItem(R.id.action_unblock);
                        m.removeItem(R.id.action_block_email);
                        m.removeItem(R.id.action_unblock_email);
                        break;
                }
                switch (mCurrentContact.getEmailAcceptance()) {
                    case "accepted":
                        m.removeItem(R.id.action_unblock_email);
                        break;
                    case "blocked":
                        m.removeItem(R.id.action_block_email);
                }
                popup.show();
            }
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_block:
                    ContactStatusService.getInstance().changeContactStatus(new
                            ChangeContactStatusCallback(), mCurrentContact.getAccountUuid(), "blocked");
                    MTracker.getInstance().GA(Screen.contact_settings, Category.contact_settings, Action
                            .submit, "blocked");
                    break;
                case R.id.action_unblock:
                    ContactStatusService.getInstance().changeContactStatus(new
                            ChangeContactStatusCallback(), mCurrentContact.getAccountUuid(), "connected");
                    MTracker.getInstance().GA(Screen.contact_settings, Category.contact_settings,
                            Action.submit, "unblock");
                    break;
                case R.id.action_block_email:
                    ContactEmailAcceptanceService.getInstance().changeContactEmailAcceptance(new
                            ChangeContactEmailAcceptanceCallback(), mCurrentContact
                            .getAccountUuid(), "blocked");
                    MTracker.getInstance().GA(Screen.contact_settings, Category.contact_settings,
                            Action.submit, "block_email");
                    break;
                case R.id.action_unblock_email:
                    ContactEmailAcceptanceService.getInstance().changeContactEmailAcceptance(new
                            ChangeContactEmailAcceptanceCallback(), mCurrentContact
                            .getAccountUuid(), "accepted");
                    MTracker.getInstance().GA(Screen.contact_settings, Category.contact_settings,
                            Action.submit, "unblock_email");
                    break;
                case R.id.action_accept_invitation:
                    ContactStatusService.getInstance().changeContactStatus(new
                                    ChangeContactStatusCallback(), mCurrentContact.getAccountUuid(),
                            "connected");
                    MTracker.getInstance().GA(Screen.contact_settings, Category.contact_settings,
                            Action.submit, "accept_invitation");
                    break;
                case R.id.action_decline_invitation:
                    ContactStatusService.getInstance().changeContactStatus(new
                            ChangeContactStatusCallback(), mCurrentContact.getAccountUuid(), "blocked");
                    MTracker.getInstance().GA(Screen.contact_settings, Category.contact_settings,
                            Action.submit, "decline_invitation");
            }
            return true;
        }

        class ChangeContactStatusCallback implements HttpClientCallback {
            @Override
            public void handleResponse(Object o) {
                if (o instanceof Throwable) {
                    RemoteCallError.show(getActivity(), (Throwable) o);
                    return;
                }

                ContactBalanceService.getInstance().getContactBalances(new GetContactBalancesCallback());
            }
        }

        class ChangeContactEmailAcceptanceCallback implements HttpClientCallback {
            @Override
            public void handleResponse(Object o) {
                if (o instanceof Throwable) {
                    RemoteCallError.show(getActivity(), (Throwable) o);
                    return;
                }

                ContactBalanceService.getInstance().getContactBalances(new GetContactBalancesCallback());
            }
        }
    }
}
